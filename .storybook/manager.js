// .storybook/manager.js

import { addons } from '@storybook/addons';
import albiTheme from './albiTheme';

addons.setConfig({
  theme: albiTheme,
});