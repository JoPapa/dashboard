import { create } from '@storybook/theming';
import logo from './logo-dark-lg.png'

export default create({
    base: 'light',
    brandTitle: 'My custom storybook',
    brandUrl: 'https://albicchiere.com',
    brandImage: logo,
});