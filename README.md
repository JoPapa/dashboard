This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Albicchiere Dashboard

## Table of contents
[Description](#description)
[How to start](#how_to_start)
[File structure](#file_structure)
[Routes](#routes)
[Authentication](#authentication)
[Container Components](#container_components)
[Components](#components)
[API](#api)
[Dependencies](#dependencies)

## <a id="description"></a>Description
The albicchiere dashbord allows companies to get to know their own wine consumption, add wine data to the Albi database such as descriptions and wine labels, and manage their company's albicchiere devices and their interactions with the database.

## <a id="how_to_start"></a>How to start

To install the dependencies, go in the project directory and run:

```
npm install
```
In the project directory you can then run:

#### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

#### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

#### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

#### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## <a id="file_structure"></a>File structure

```
src
│   README.md
│   Routes.js
│   index.js
│   App files..    
│   Global Styles..    
│   ...
└───components
│   │   ...
└───containers
    │   ...
└───fonts
    │   ...
└───images
    │   ...
└───utils
    │   ...
└───Containers
    │   ...
```

## <a id="routes"></a>Routes
Routes are managed using [react-router]()
They are all used in  **/src/Routes.js**
There are four types of routes:
| Type | Description | Component |
| --- | --- | --- |
| **UnauthenticatedRoute** | With access to all users | [UnauthenticatedRoute]() |
| **AuthenticatedOnlyRoute** | Grants access only if user is registered but not associated with a company | [AuthenticatedOnlydRoute]() |
| **AuthenticatedRoute** | Gives access to all registered users with an associated company account | [AuthenticatedRoute]() |
| **AuthorizedRoute** | Specific access for the type of associated company |  [AuthorizedRoute]() |

#### <a id="generic_routes"></a>Generic Routes
| Route             | Description                                   | Component        |
| ----------------: | --------------------------------------------- | ---------------- |
| [`/login`]()      | User login page                               | [Login.js]()     |
| [`/register`]()     | User registration page                        | [Register.js]()  |
| [`/reset`]()      | Allows users to reset their passwords         | [Register.js]()  |
| [`/home`]()       | Associeate users with their first company     | [Home.js]()      |
| [`/shop`]()       | Shop section for companies                    | [Home.js]()      |
| [`/admin`]()      | Company administration section                | [Home.js]()      |
| [`/settings`]()   | User settings page                            | [Home.js]()      |

#### <a id="winery_routes"></a>Winery Specific Routes
**Description**
Routes only accesseible from companies of type "winery"
| Route                 | Description                                   | Component        |
| --------------------: | --------------------------------------------- | ---------------- |
| [`/analytics`]()      | Analytical wine consumption data for wineries | [Analytics.js](#analytics) |
| [`/my-wines`]()       | List of the winery's wines                    | [MyWines.js]()   |
| [`/wines/:id`]()      | Wine description page                         | [MyWines.js]()   |
| [`/my-wines/edit`]()  | Wine edit page                                | [MyWines.js]()   |
| [`/blockchain`]()     | Company's blockchain log                      | [Blockchain.js]()|
| [`/blockchain/bag`]() | Specific bag blockchain log                   | [Blockchain.js]()|
| [`/blockchain/box`]() | Blockchain logs for containers                | [Blockchain.js]()|

#### <a id="professional_routes"></a>Professional Specific Routes
**Description**
Routes only accesseible from companies of type "professional"

| Route             | Description                   | Component        |
| ----------------: | ----------------------------- | ---------------- |
| [`/stock`]()      | Wines in the company stock    | [MyWines.js]()   |
| [`/dashboard`]()  | Financial dashboard           | [Analytics.js]() |
| [`/dispensers`]() | Company's dispenser locations | [MyWines.js]()   |
| [`/room`]()       | Companiy's room page          | [MyWines.js]()   |

## <a id="authentication"></a>Authentcation

The authentication process is done through the [aws-amplify]() library

## <a id="container_components"></a>Container Components

### <a id="analytics"></a>Login
Login page - Uses AWS Cognito to login users
### <a id="analytics"></a>Register
register page - Uses AWS Cognito to register users
### <a id="analytics"></a>Settings
User settings
### <a id="analytics"></a>ResetPassword
Resets users password
### <a id="analytics"></a>Home
This page allows new registered users or accounts with no company association to associate with or create a company profile.
### <a id="analytics"></a>Shop
The shop section allows users to buy Albicchiere items through a company name.
### <a id="analytics"></a>Admin
Users with admin permissions can view and edit the company administration panel
### <a id="analytics"></a>Analytics
The analytics comopent shows all
### <a id="analytics"></a>MyWines
List of the wines added by the company
### <a id="analytics"></a>WinePage
### <a id="analytics"></a>Blockchain
### <a id="analytics"></a>Dashboard
### <a id="analytics"></a>Stock
### <a id="analytics"></a>Dispensers
### <a id="analytics"></a>Room

## <a id="components"></a>Components

All components are listed here:



## <a id="dependencies"></a>Dependencies

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).