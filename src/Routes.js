import React, { Suspense, lazy } from "react";
import { useTranslation } from "react-i18next";
import { Route, Switch, Redirect } from "react-router-dom";
import Card from "../src/components/Card"
import NoWines from "../src/images/no-wines.png"
const AuthenticatedOnlyRoute = lazy(() => import('./components/AuthenticatedOnlyRoute'));
const AuthenticatedRoute = lazy(() => import('./components/AuthenticatedRoute'));
const AuthorizedRoute = lazy(() => import('./components/AuthorizedRoute'));
const UnauthenticatedRoute = lazy(() => import('./components/UnauthenticatedRoute'));
const Home = lazy(() => import('./containers/Home'));
const Analytics = lazy(() => import('./containers/Analytics'));
const Login = lazy(() => import('./containers/Login'));
const Register = lazy(() => import('./containers/Register'));
const ResetPassword = lazy(() => import('./containers/ResetPassword'));
const Settings = lazy(() => import('./containers/Settings'));
const DashboardPro = lazy(() => import('./containers/DashboardPro'));
const ChiSono = lazy(() => import('./containers/ChiSono'));
const DatiGeo = lazy(() => import('./containers/DatiGeo'));
const DatiQuando = lazy(() => import('./containers/DatiQuando'));
const MyWines = lazy(() => import('./containers/MyWines'));
const WinePage = lazy(() => import('./containers/WinePage'));
const NewWine = lazy(() => import('./containers/NewWine'));
const BlockchainHome = lazy(() => import('./containers/BlockchainHome'));
const BlockchainWine = lazy(() => import('./containers/BlockchainWine'));
const BlockchainBox = lazy(() => import('./containers/BlockchainBox'));
const Stock = lazy(() => import('./containers/Stock'));
const Room = lazy(() => import('./containers/Room'));
const Admin = lazy(() => import('./containers/Admin'));
const Shop = lazy(() => import("./containers/Shop"));
const Dispensers = lazy(() => import("./containers/Dispensers"));
const BlockchainTags = lazy(() => import("./containers/BlockchainTags"));

export default function Routes(props) {
    const {t} = useTranslation()
    const appProps = props.appProps
    return (
        <Suspense fallback={<h3>Loading...</h3>}>
            <Switch>
                {/* <UnauthenticatedRoute exact path="/login" component={Login}
                    appProps={appProps}
                    authorize={props.authorize}
                    authenticate={props.authenticate}
                />
                <UnauthenticatedRoute exact path="/register" component={Register}
                    appProps={appProps}
                    authorize={props.authorize}
                    authenticate={props.authenticate}
                />
                <UnauthenticatedRoute exact path="/reset" component={ResetPassword}
                    appProps={appProps}
                    authorize={props.authorize}
                    authenticate={props.authenticate}
                /> */}
                {/* If user is authenticated only and not authorized */}
                {/* <AuthenticatedOnlyRoute exact path="/home" component={Home} appProps={appProps} /> */}
                {/* If user is both authenticated and authorized */}
                {/* <AuthenticatedRoute exact path="/shop" component={Shop} appProps={appProps} />
                <AuthenticatedRoute exact path="/settings" component={Settings} appProps={appProps} />
                <AuthenticatedRoute exact path="/room" component={Room} appProps={appProps} /> */}
                {/* If user is authorized */}
                {/* <AuthorizedRoute exact path="/analytics" component={Analytics} appProps={appProps} authorizedRole={"winery"} />
                <AuthorizedRoute exact path="/chi-sono" component={ChiSono} appProps={appProps} authorizedRole={"winery"} />
                <AuthorizedRoute exact path="/dove-sono" component={DatiGeo} appProps={appProps} authorizedRole={"winery"} />
                <AuthorizedRoute exact path="/quando" component={DatiQuando} appProps={appProps} authorizedRole={"winery"} /> */}
                <AuthorizedRoute exact path="/wines" component={MyWines} appProps={appProps} authorizedRole={"winery"} />
                <AuthorizedRoute exact path="/wine" component={WinePage} appProps={appProps} authorizedRole={"winery"} />
                <AuthorizedRoute exact path="/wines/new-wine" component={NewWine} appProps={appProps} authorizedRole={"winery"} />
                {/* <AuthorizedRoute exact path="/blockchain" component={BlockchainHome} appProps={appProps} authorizedRole={"winery"} />
                <AuthorizedRoute exact path="/blockchain/tags" component={BlockchainTags} appProps={appProps} authorizedRole={"winery"} />
                <AuthorizedRoute exact path="/blockchain/wine" component={BlockchainWine} appProps={appProps} authorizedRole={"winery"} />
                <AuthorizedRoute exact path="/blockchain/box" component={BlockchainBox} appProps={appProps} authorizedRole={"winery"} />
                <AuthorizedRoute exact path="/homepro" component={DashboardPro} appProps={appProps} authorizedRole={"pro"} />
                <AuthorizedRoute exact path="/dispensers" component={Dispensers} appProps={appProps} authorizedRole={"pro"} />
                <AuthorizedRoute exact path="/magazzino" component={Stock} appProps={appProps} authorizedRole={"pro"} />
                <AuthorizedRoute exact path="/room" component={Room} appProps={appProps} authorizedRole={"pro"} />
                <AuthorizedRoute exact path="/admin" component={Admin} appProps={appProps} authorizedRole={"winery"} /> */}
                { /* Catch all unmatched routes */}
                {/* <Route exact path="/" render={() => <Redirect to="/login" />} /> */}
                
                <Route exact path="/" render={() => <Redirect to="/wines" />} />
                
                <Route render={() =>
                    <>
                        <div className="inline-flex flex-col space-y-1 items-start justify-start mb-3">
                            {/* Page Title */}
                            <p className="w-full h-5 text-base font-bold">{t("404 - Page not found")}</p>
                            <p className="w-full h-3 text-xs font-light text-gray-600">{t("")}</p>
                        </div>
                        <Card
                            title={t('Page not found')}
                            description={""}
                        >
                            <div className="flex flex-col md:flex-row w-full h-full space-y-3 md:space-x-6 lg:space-x-12 items-center justify-center">
                                <img className="max-w-96 max-h-96 rounded-tl-full rounded-tr-3xl rounded-bl-full" src={NoWines} alt="Illustration of hands cheering for wine" />
                                <div className="inline-flex flex-col space-y-2 lg:space-y-6 items-start justify-start">
                                    <p className="text-2xl lg:text-4xl font-bold">Ops... questa pagina non esiste.</p>
                                    <p className="text-base max-w-xs">Sembra che questa pagina non esista o è stata eliminata. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at porttitor diam. Aliquam in orci sed elit faucibus dapibus!</p>
                                </div>
                            </div>
                        </Card>
                    </>
                } />
            </Switch>
        </Suspense>
    );
}