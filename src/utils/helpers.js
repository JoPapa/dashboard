import { useState, useEffect } from "react";
import { Storage } from "aws-amplify";

export async function s3Upload(file) {
  //TODO: This might not be the best way to create a unique filename
  const filename = `${Date.now()}-${file.name}`;
  //TODO: only use Storage.put() when you are ready
  const stored = await Storage.vault.put(filename, file, {
    contentType: file.type,
  });

  return stored.key;
}

export function getChartArrayFromObject(object) {
  return Object.entries(object).map(([key, value]) => {
    return {
      name: key,
      value: value
    }
  })
}

export function getDateFromTimestamp(timestamp) {
  const date = new Date(timestamp * 1000)
  return date
}

export function useFormFields(initialState) {
  const [fields, setValues] = useState(initialState);

  return [
    fields,
    function (event) {
      setValues({
        ...fields,
        [event.target.id]: event.target.value
      });
    }
  ];
}

export function useFormNameFields(initialState) {
  const [fields, setValues] = useState(initialState);

  return [
    fields,
    function (event) {
      setValues({
        ...fields,
        [event.target.name]: event.target.value
      });
    }
  ];
}

// Limit age between 18/125yrs old, used below in input type="date"
export function setLimitDate(limit) {
  const now = new Date();
  now.setFullYear(now.getFullYear() - limit, now.getMonth(), now.getDate())
  const month = now.getMonth() < 11 ? `0${now.getMonth() + 1}` : now.getMonth + 1;
  const date = now.getDate() < 10 ? `0${now.getDate()}` : now.getDate();
  return `${now.getFullYear()}-${month}-${date}`
}

// using Ref to handle the outside click
export const useOutsideClick = (ref, callback) => {
  const handleClick = e => {
    if (ref.current && !ref.current.contains(e.target)) {
      callback();
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleClick);

    return () => {
      document.removeEventListener("click", handleClick);
    };
  });
};