import * as wines from "./data/wines.json";
import * as wines2 from "./data/wines2.json";
import * as dispensers from "./data/dispensers.json";
import * as stock from "./data/stock.json";
import * as room from "./data/room.json";
import * as home from "./data/home.json";
import * as box from "./data/blockchainBox.json";
import * as bag from "./data/blockchainWine.json";
import * as blockchain from "./data/blockchain.json";
import * as blockchainTags from "./data/blockchainTags.json";
import * as dashboard from "./data/finance.json";
import * as company from "./data/company.json";
import * as user from "./data/user.json";
import { API } from 'aws-amplify'
// /analytics
// /wines
// /wines/:id
// /blockchain
// /blockchain/:id
// /shop
// /dashboard
// /stock
// /stock/:id
// /dispensers
// /dispensers?location={location}
// /dispensers/room/:id
// /company
// /company/logs
// /company/orders
// /user

// HOME
// /analytics
export function getHome(companyId) {
    console.log("CompanyId", companyId)
    return new Promise((res, rej) => {
        setTimeout(() => res(home.default[companyId]), 200)
    })
}

// WINES
// /wines
export function getWines(companyId) {
    // return API.get('wines', '/wines').then(res => console.log('resld..', res))
    return new Promise((res, rej) => {
        setTimeout(() => res(wines2.default[companyId]), 1000)
    })
}

// /wines/:id
export function getWineInfo(id, vintage = '') {
    const wine = wines.data.find(wine => wine.id === id)
    const result = !vintage
        ? wine
        : {
            wine: wine,
            vintage: wine.wines.find(wine => wine.year === vintage)
        }
    return new Promise((res, rej) => {
        setTimeout(() => res(result), 1000)
    })
}

// POST /wines/
export async function createNewWine(newWine) {
    console.log("Creating new wine at /wines")
    API
        .put("albi", "/wines", {
            headers: {
                'company': '--04940'
            },
            response: true,
            body: newWine
        })
        .then(res => {
            console.log("Wine successfully created: ", res)
            return res
        })
        .catch(error => {
            console.log("Wine successfully created: ", error)
            return error
        })
}

// POST /wines/
export function createNewVintage(wineId, vintage) {
    API
        .put("albi", "/wines", {
            body: {
                wine: wineId,
                vintage: vintage
            }
        })
        .then(res => {
            console.log("Wine successfully created: ", res)
            return res
        })
        .catch(error => {
            console.log("Wine successfully created: ", error)
            return error
        })
}

// BLOCKCHAIN
// /blockchain
export function getBlockchainData() {
    return new Promise((res, rej) => {
        setTimeout(() => res(blockchain.default), 1000)
    })
}

export function listBlockchainBoxes() {
    return new Promise((res, rej) => {
        setTimeout(() => res(blockchainTags.default), 1000)
    })
}

export function getBoxInfo() {
    return new Promise((res, rej) => {
        setTimeout(() => res(box.default), 1000)
    })
}

export function getBagInfo({ companyId, bagId }) {
    return new Promise((res, rej) => {
        setTimeout(() => res(bag.default), 1000)
    })
}

// FINANCE DASHBOARD
export function getFinance(companyId) {
    return new Promise((res, rej) => {
        setTimeout(() => res(dashboard.default[companyId]), 0)
    })
}

// DISPENSERS
// /dispensers
// /dispensers?location={location}
export function getDispensers(companyId) {
    return new Promise((res, rej) => {
        setTimeout(() => res(dispensers.default[companyId]), 1000)
    })
}

// room by id
// /dispensers/room/:id
export function getRoom({ companyId, roomId }) {
    return new Promise((res, rej) => {
        setTimeout(() => res(room.default), 1000)
    })
}

// STOCK
// /stock
export function getStock(companyId) {
    return new Promise((res, rej) => {
        setTimeout(() => res(stock.stock), 1000)
    })
}

// COMPANY

// Create company
export async function createCompany(newCompany) {
    let res;
    await API.put("albi", "/companies", {
        body: newCompany
    }).then(res =>
        console.log("Company created => ", res)
    )
    return res
}

// GET Company
export async function getCompanyInfo(companyId) {
    return new Promise((res, rej) => {
        setTimeout(() => res(company["companies"].filter(company =>
            company.id === companyId
        )[0]), 0)
    })
}
// Company log
export function getCompanyLogs(companyId) {
    return new Promise((res, rej) => {
        setTimeout(() => res(company["companyLogs"].filter(company =>
            company.companyId === companyId
        )[0]), 0)
    })
}
// Company orders log
export function getCompanyOrders(companyId) {
    return new Promise((res, rej) => {
        setTimeout(() => res(company["companyOrders"].filter(company =>
            company.companyId === companyId
        )[0]), 0)
    })
}

// USER
export function getUserInfo(userId) {
    return new Promise((res, rej) => {
        setTimeout(() => res(user.default.filter(user =>
            user.id === userId
        )), 1000)
    })
}