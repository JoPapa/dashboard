import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import Label from '../images/albi-wine-label.jpg'

const StockTable = ({ stock }) => {
    const { t } = useTranslation()
    const [collapse, setCollapse] = useState("");
    const collapseItem = (item) => {
        collapse === item
            ? setCollapse('')
            : setCollapse(item)
    }
    return (
        <div className="card table-responsive">
            <table className="table table-hover table-lg text-center mb-0">
                {/* Table Header */}
                <thead>
                    <tr>
                        {["Vino", "Prezzo di acquisto medio", "Rimasto", "Aperto", "In scadenza", "Scaduto"].map(header => (
                            <th key={header} className="border-top-0">{header}</th>
                        ))}
                    </tr>
                </thead>
                {/* Table Body */}
                <tbody style={{ "width": "100%" }}>
                    {stock.data.map(wine => (
                        <React.Fragment key={wine.id}>
                            <tr
                                className="text-center font-medium-3"
                                style={{ "cursor": "pointer" }}
                                onClick={() => collapseItem(wine.id)}
                            >
                                {["wine", "purchasePrice", "available", "opened", "expiring", "expired"].map((header, index) => (
                                    header === "wine"
                                        ? <td key={wine.id + index} className="">
                                            <div className="d-flex align-items-center">
                                                <i className={`las la-chevron-${collapse === wine.id ? "down" : "right"}`}></i>
                                                <div className="d-flex justify-content-center align-items-center">
                                                    <div className="pl-1">
                                                        <img className="img-fluid rounded" src={Label} width={"35px"} height={"55px"} alt={`${wine.wine}'s label`} />
                                                    </div>
                                                    <div className="font-weight-bold px-1">{wine.vintage}</div>
                                                    <div className="text-left px-1">
                                                        <div className="font-weight-bold">{wine[header]}</div>
                                                        <div>{wine.winery}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        : <td key={wine.id + index} className="align-middle">
                                            <div>{wine[header]}{header === "purchasePrice" && "$"}</div>
                                        </td>
                                ))}
                            </tr>
                            {collapse === wine.id &&
                                <tr>
                                    <td colSpan={7} className="p-0">
                                        <table className="table table-hover table-lg text-center mb-0">
                                            {/* Table Header */}
                                            <thead>
                                                <tr>
                                                    {stock.bagHeaders.map(header => (
                                                        <th key={header.field} className="border-top-0">{header.title}</th>
                                                    ))}
                                                    <th className="border-top-0"></th>
                                                    {/* <th className="border-top-0"></th> */}
                                                </tr>
                                            </thead>
                                            {/* Table Body */}
                                            <tbody style={{ "width": "100%" }}>
                                                {wine.bags.map(bag => (
                                                    <tr key={bag.bagCode} className="text-center">
                                                        {stock.bagHeaders.map(header => (
                                                            <td key={header.field} className="align-middle">
                                                                {header.field === 'uso' &&
                                                                    <div>{}</div>
                                                                }
                                                                <div>{bag[header.field]}</div>
                                                            </td>
                                                        ))}
                                                        <td className="text-center align-middle">
                                                            <button type="button" className="btn btn-outline-danger" onClick={() => null}>
                                                                {t('stock.archive')}
                                                            </button>
                                                        </td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            }
                        </React.Fragment>
                    ))}
                </tbody>
            </table>
        </div>

    )
}

export default StockTable