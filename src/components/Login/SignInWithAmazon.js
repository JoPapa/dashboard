import React, { Component } from 'react';
import { Auth } from 'aws-amplify';
import config from '../../config';

// To federated sign in from Facebook
export default class SignInWithAmazon extends Component {
    componentDidMount() {
        // create amazon root element
        var div = document.createElement('div');
        div.id = 'amazon-root';
        document.body.appendChild(div);

        window.onAmazonLoginReady = function () {
            window.amazon.Login.setClientId(config.social.amazon);
        };
        if (!window.amazon) this.createScript();
    }

    signIn() {
        const amazon = window.amazon;
        const options = {}
        options.scope = 'profile';
        options.pkce = true;
        options.redirect_uri = 'http://localhost:3000/login';
        options.response_type = 'token'
        options.scope_data = {
            'profile': { 'essential': false }
        };
        amazon.Login.authorize(options, function (response) {
            if (response.error) {
                alert('Login error ' + response.error);
                return;
            }
            console.log("Auth Response", response)

            amazon.Login.retrieveToken(response.code, function (response) {
                const { access_token, expires_in } = response
                if (response.error) {
                    alert('Login error ' + response.error);
                    return;
                }
                console.log("Token", access_token)
                console.log("Token", response)

                amazon.Login.retrieveProfile(access_token, async function (response) {
                    const user = {
                        name: response.profile.name,
                        email: response.profile.email,
                        givenName: response.profile.name
                    };
                    console.log("Expires in ", expires_in)
                    console.log("Token", access_token)

                    Auth.federatedSignIn('amazon', { token: access_token, expires_at: expires_in }, user)
                        .then(credentials => {
                            console.log("Credentials: ", credentials);
                            this.props.setCurrentUser({...this.props.setCurrentUser, firstName: user.givenName})
                            this.props.authenticate(true)
                        });

                    if (window.console && window.console.log)
                        window.console.log(response);
                });
            });
            return false;
        });
    }

    createScript() {
        // load the sdk
        var a = document.createElement('script'); a.type = 'text/javascript';
        a.async = true; a.id = 'amazon-login-sdk';
        a.src = 'https://assets.loginwithamazon.com/sdk/na/login1.js';
        a.onload = this.initAmz;
        document.getElementById('amazon-root').appendChild(a);
    }

    initAmz() {
        console.log('Amazon SDK initialized');
        return window.amazon;
    }

    render() {
        return (
            <button onClick={this.signIn} className="btn btn-social-icon mr-1 mb-1 btn-outline-facebook">
                <span className="la la-amazon"></span>
            </button>
        );
    }
}