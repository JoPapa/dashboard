import React, { Component } from 'react';
import { Auth } from 'aws-amplify';
import config from '../../config';
// To federated sign in from Google
export default class SignInWithGoogle extends Component {
    constructor(props) {
        super(props);
        this.signIn = this.signIn.bind(this);
    }

    componentDidMount() {
        const ga = window.gapi && window.gapi.auth2 ?
            window.gapi.auth2.getAuthInstance() :
            null;
        if (!ga) this.createScript();
    }

    signIn() {
        const ga = window.gapi.auth2.getAuthInstance();
        ga.signIn().then(googleUser => {
            this.getAWSCredentials(googleUser);
        }, error => {
            console.log(error);
        }
        );
    }

    async getAWSCredentials(googleUser) {
        const { id_token, expires_at } = googleUser.getAuthResponse();
        const profile = googleUser.getBasicProfile();
        console.log("user google profile", profile)
        let user = {
            email: profile.getEmail(),
            name: profile.getName(),
            givenName: profile.getGivenName(),
            familyName: profile.getFamilyName(),
            profile: profile.getImageUrl()
        };

        const credentials = await Auth.federatedSignIn(
            'google',
            { token: id_token, expires_at },
            user
        );
        console.log('Credentials: ', credentials);
        Auth.currentAuthenticatedUser().then(user => {
            console.log(user)
            this.props.setCurrentUser({
                ...this.props.setCurrentUser,
                firstName: user.givenName,
                lastName: user.familyName,
                picture: user.profile,
                email: user.email,
            })
            this.props.authenticate(true)
        });
        Auth.currentCredentials().then(creds => console.log(creds));
        const logged = await (await Auth.currentSession()).getIdToken();
        console.log("Logged: ", logged);

    }

    createScript() {
        // load the Google SDK
        const script = document.createElement('script');
        script.src = 'https://apis.google.com/js/platform.js';
        script.async = true;
        script.onload = this.initGapi;
        document.body.appendChild(script);
    }

    initGapi() {
        // init the Google SDK client
        const g = window.gapi;
        g.load('auth2', function () {
            g.auth2.init({
                client_id: config.social.google,
                // authorized scopes
                scope: 'profile email openid'
            });
        });
    }

    render() {
        return (
            <button onClick={this.signIn} className="btn btn-social-icon mr-1 mb-1 btn-outline-google">
                {/* <span className="la la-google"></span> */}
                <GoogleIcon />
            </button>
        );
    }
}

const GoogleIcon = () => {
    return (
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="18px" height="18px" viewBox="0 0 48 48" className="abcRioButtonSvg"><g><path fill="#EA4335" d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"></path><path fill="#4285F4" d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"></path><path fill="#FBBC05" d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"></path><path fill="#34A853" d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"></path><path fill="none" d="M0 0h48v48H0z"></path></g></svg>
    )
}