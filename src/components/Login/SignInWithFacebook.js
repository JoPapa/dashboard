import React, { Component } from 'react';
import { Auth } from 'aws-amplify';
import config from '../../config';
// To federated sign in from Facebook
export default class SignInWithFacebook extends Component {
    constructor(props) {
        super(props);
        this.signIn = this.signIn.bind(this);
    }

    componentDidMount() {
        if (!window.FB) this.createScript();
    }

    signIn() {
        const fb = window.FB;
        fb.getLoginStatus(response => {
            if (response.status === 'connected') {
                this.getAWSCredentials(response.authResponse);
            } else {
                fb.login(
                    response => {
                        if (!response || !response.authResponse) {
                            return;
                        }
                        this.getAWSCredentials(response.authResponse);
                    },
                    {
                        // the authorized scopes
                        scope: 'public_profile,email'
                    }
                );
            }
        });
    }

    getAWSCredentials(response) {
        console.log("response FB: ", response)
        const { accessToken, expiresIn } = response;
        const date = new Date();
        const expires_at = expiresIn * 1000 + date.getTime();
        if (!accessToken) {
            return;
        }

        const fb = window.FB;
        fb.api('/me', { fields: 'name,email,first_name' }, response => {
            console.log("Given User: ", response)
            const user = {
                name: response.name,
                email: response.email,
                givenName: response.first_name
            };

            Auth.federatedSignIn('facebook', { token: accessToken, expires_at }, user)
                .then(credentials => {
                    console.log("Credentials: ", credentials);
                    this.props.setCurrentUser({ ...this.props.setCurrentUser, firstName: user.givenName })
                    this.props.authenticate(true)
                });

        });
    }

    createScript() {
        // load the sdk
        window.fbAsyncInit = this.fbAsyncInit;
        const script = document.createElement('script');
        script.src = 'https://connect.facebook.net/en_US/sdk.js';
        script.async = true;
        script.onload = this.initFB;
        document.body.appendChild(script);
    }

    initFB() {
        console.log('FB SDK initialized');
        return window.FB;
    }

    fbAsyncInit() {
        // init the fb sdk client
        const fb = window.FB;
        fb.init({
            appId: config.social.facebook,
            cookie: true,
            xfbml: true,
            version: 'v2.11'
        });
    }

    render() {
        return (
            <button onClick={this.signIn} className="btn btn-social-icon mr-1 mb-1 btn-outline-facebook">
                <span className="la la-facebook"></span>
            </button>
        );
    }
}