import React from "react";
import { Route, Redirect } from "react-router-dom";

export default function AuthorizedRoute({ component: C, appProps, authorizedRole, ...rest }) {
  console.log("got into authorized route")
  return (
    <Route
      {...rest}
      render={props =>
        appProps.isAuthenticating
        ? <div>Authenticating...</div>
        : !appProps.isAuthenticated
          ? <Redirect
            to={`/login?redirect=${props.location.pathname}${props.location
              .search}`}
          />
          : !appProps.authorized
            ? <Redirect exact to={`/home`} />
            : (appProps.role === 'all' || appProps.role === authorizedRole)
              ? <C {...props} {...appProps} />
              : <Redirect to={appProps.role === 'pro' ? '/homepro' : '/home'} />}
    />
  );
}