import React from 'react'

const ReviewCard = (props) => {
    return (
        <div className="card">
            {/* Card Header */}
            <div className="card-header text-center">
                <div className="card-title">Recensioni</div>
            </div>
            {/* Card Body */}
            <div className="card-body text-center">
                {/* Rating */}
                <div className="rating">
                    <h5 className="font-large-3">
                        95.6
                    </h5>
                    <p className="">Punteggio medio</p>
                </div>
                {/* Stars section */}
                <div className="stars">
                    <i className="la la-star warning mx-1"></i>
                    <i className="la la-star warning mx-1"></i>
                    <i className="la la-star warning mx-1"></i>
                    <i className="la la-star warning mx-1"></i>
                    <i className="la la-star-half-o warning mx-1"></i>
                </div>
                {/* Percentage Bars */}
                <div className="mb-2 mr-1">
                    <div className="progress mt-1 mb-0">
                        <div className="px-2 bg-white font-small-5">5 stars</div> 
                        <div className="progress-bar bg-warning" role="progressbar" style={{"width": `34%`}}></div>
                    </div>
                    <div className="progress mt-1 mb-0">
                        <div className="px-2 bg-white font-small-5">4 stars</div> 
                        <div className="progress-bar bg-warning" role="progressbar" style={{"width": `46%`}}></div>
                    </div>
                    <div className="progress mt-1 mb-0">
                        <div className="px-2 bg-white font-small-5">3 stars</div> 
                        <div className="progress-bar bg-warning" role="progressbar" style={{"width": `8%`}}></div>
                    </div>
                    <div className="progress mt-1 mb-0">
                        <div className="px-2 bg-white font-small-5">2 stars</div> 
                        <div className="progress-bar bg-warning" role="progressbar" style={{"width": `4%`}}></div>
                    </div>
                    <div className="progress mt-1 mb-0">
                        <div className="px-2 bg-white font-small-5">1 star &nbsp;</div> 
                        <div className="progress-bar bg-warning" role="progressbar" style={{"width": `8%`}}></div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ReviewCard