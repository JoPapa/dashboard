import React, { useState } from 'react'
import PropTypes from 'prop-types'

const MultiLanguageDescription = (props) => {
    const [currentLanguage, setCurrentLanguage] = useState('en')

    return (
        <>
            <div className="font-weight-bold font-medium-1 my-1">
                <select className="form-control" value={currentLanguage} onChange={(e) => setCurrentLanguage(e.target.value)}>
                    {Object.keys(props.languages).map((item, i) =>
                        <option value={item} key={item}>{item.toUpperCase()}</option>
                    )}
                </select>
            </div>
            <textarea
                rows={14}
                className="form-control"
                placeholder={props.placeholder}
                value={props.languages[currentLanguage]}
                onChange={(e) => props.onChange({ currentLanguage, value: e.target.value})}
                style={{ "width": "100%", "height": "auto" }}
                disabled={props.disabled}
            />
        </>
    )
}

MultiLanguageDescription.propTypes = {
    label: PropTypes.string,
    placeholder: PropTypes.string,
    languages: PropTypes.objectOf(PropTypes.string)
}

export default MultiLanguageDescription