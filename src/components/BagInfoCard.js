import React from 'react'
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import loader from '../images/loader_200.gif'
import WineAttribute from './WineAttribute'
import WineLevel from './WineLevel'

const BagInfoCard = (props) => {
    const { t } = useTranslation();
    return (
        <div className="card">
            <div className="row no-gutters">
                {/* Image column */}
                <div className={"col-12 col-lg-4 px-2 mt-2 d-flex"}>
                    {props.isLoading
                        ? <img src={loader} width="75%" height="auto" alt="wine loader.." />
                        : <img className="img-fluid" max-height="90%" width="auto" src={process.env.PUBLIC_URL + `/images/wines/000001.jpg`} alt="wine label" />
                    }
                </div>
                {/* Attribute column */}
                <div className="col-12 col-lg-8 px-2 my-3">
                    <h1 className="text-uppercase">{props.wine.wine_name}</h1>
                    <h4 className="text-uppercase">{props.wine.winery}</h4>
                    <div className="row">
                        {/* Wine attributes */}
                        <div className="col-12 col-lg-4 px-1">
                            <WineAttribute
                                attribute={t('wine.tag code')}
                                value={"00490785"}
                            />
                            <WineAttribute
                                attribute={t('blockchain.packaging date')}
                                value={props.wine.imbottigliata}
                            />
                            <WineAttribute
                                attribute={t('wine.wine left')}
                                value={
                                    <WineLevel
                                        totalGlasses={props.wine.bagCapacity}
                                        glassesLeft={props.wine.quantityLeft}
                                    />}
                            />
                        </div>
                        <div className="col-12 col-lg-4 px-1">
                            <WineAttribute
                                attribute={t('wine.first opening')}
                                value={props.wine.prima_apertura}
                            />
                            <WineAttribute
                                attribute={t('wine.last service')}
                                value={props.wine.ultimo_utilizzo}
                            />
                            <WineAttribute
                                attribute={t('wine.avarage serving temperature')}
                                value={props.wine.temperatura_servizio_media}
                            />
                        </div>
                        <div className="col-12 col-lg-4 px-1">
                            <div className="text-uppercase info mt-1">{"Tag associati"}</div>
                            {props.wine.pairedTags.length > 0
                                ? props.wine.pairedTags.map(tag =>
                                    <div className="font-medium-3 d-flex align-items-center">
                                        <i className={`font-large-1 la la-${tag.type === 'box' && "box"} mr-1`}></i>
                                        <Link to={`/blockchain/box`}>{tag.tagId}</Link>
                                    </div>
                                )
                                : <div className="font-medium-3">{t('blockchain.none')}</div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

BagInfoCard.defaultProps = {
    wine: {
        wine_name: 'Sangiovese Tenuta Montecosi',
        annata: "2016",
        vitigno: "Sangiovese",
        type: "Rosso",
        grado_alcolico: "14",
        temperatura_consigliata: "16",
        aroma: "Lampone, Agrumi",
        mood: "Con Amici",
        imbottigliata: "12/01/2020",
        prima_apertura: "23/05/2020",
        ultimo_utilizzo: "24/05/2020",
        temperatura_servizio_media: "18°C",
    }
}

BagInfoCard.propTypes = {
    isLoading: PropTypes.bool,
    wine: PropTypes.object.isRequired,
    wineYear: PropTypes.string
}

export default BagInfoCard