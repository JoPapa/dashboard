import React from 'react'
import { useTranslation } from 'react-i18next';
// import PropTypes from 'prop-types'

const BlockchainScoreCard = (props) => {
    const { t } = useTranslation();
    const blockchainScore = props.score
    console.log(blockchainScore)
    let score = (blockchainScore >= 90)
        ? {
            icon: "grin-stars",
            description: t('blockchain.scores.excellent')
        }
        : blockchainScore < 90 && blockchainScore >= 80
            ? {
                icon: "grin-wink",
                description: t('blockchain.scores.great')
            }
            : blockchainScore < 80 && blockchainScore >= 70
                ? {
                    icon: "laugh",
                    description: t('blockchain.scores.good')
                }
                : {
                    icon: "smile",
                    description: t('blockchain.scores.acceptable')
                }

    return (
        <div className="card">
            <div className="card-header">
                <div className="card-title text-center font-medium-4">{t('blockchain.scores.quality score')}</div>
            </div>
            <div className="card-body">
                <div className="h4 text-center pb-2"><i className={`las la-${score.icon} la-5x`}></i><div>{score.description}</div></div>

                <button className="btn btn-primary btn-lg btn-block">{t('blockchain.scores.download certificate')}</button>
                <button className="btn btn-secondary btn-lg btn-block">{t('blockchain.scores.go to blockchain')}</button>

            </div>
        </div>
    )
}

BlockchainScoreCard.defaultProps = {
    score: 90
}

BlockchainScoreCard.propTypes = {
    // score: PropTypes.string || PropTypes.number
}

export default BlockchainScoreCard