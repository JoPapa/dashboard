import React from 'react'
import './Chip.css'

const Chip = (props) => {
    return (
        <div className="chip" onClick={props.onClick}>
            <span>{props.name}</span>
            <i role="button" className="las la-times-circle close-btn"></i>
        </div>
    )
}

export default Chip