import React from 'react';
import { useTranslation } from 'react-i18next'
import Modal from 'react-modal'

const modalStyle = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        border: '0',
        borderRadius: '15px',
        boxShadow: '0px 0px 20px 0px #c5c5c5',
        marginRight: '-50%',
        padding: '50px',
        maxWidth: '600px',
        transform: 'translate(-50%, -50%)'
    }
}

const ArchiveModal = ({ isOpen, onRequestClose, onSave }) => {
    const { t } = useTranslation()

    const onClose = () => onRequestClose()

    const onConfirm = () => {
        onSave()
        onRequestClose()
    }

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onClose}
            style={modalStyle}
            contentLabel="Archive consumptions"
        >
            <div className="text-center">
                <button
                    type="button"
                    onClick={onClose}
                    className="close"
                    aria-label="Close"
                    style={{
                        position: 'absolute',
                        top: '20px',
                        right: '25px',
                    }}>
                    <i className="la la-times"></i>
                </button>
                <h3 className="text-uppercase">{'Archivia consumazioni'}</h3>
                <fieldset className="form-group">
                    <label htmlFor="selectOpt" className="filled">Causale</label>
                    <select className="form-control" id="selectOpt">
                        <option>Select Option</option>
                        <option>Errore umano</option>
                    </select>
                </fieldset>
                <fieldset className="form-group mb-2">
                    <label htmlFor="notes" className="filled">Nota</label>
                    <input id="notes" type="textarea" className="form-control" style={{"minHeight": "100px"}}/>
                </fieldset>

                <div className="d-flex justify-content-around">
                    <button type="button" className="btn btn-outline-info round" onClick={() => onClose()}>
                        <span className="text-uppercase mx-1">{t('settings.cancel')}</span>
                        <i className="las la-times"></i>
                    </button>
                    <button type="button" className="btn btn-outline-danger round ml-1" onClick={() => onConfirm()}>
                        <span className="text-uppercase mx-1">{t('settings.confirm')}</span>
                        <i className="las la-check"></i>
                    </button>
                </div>
            </div>
        </Modal >
    )
}

export default ArchiveModal