import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import Chip from './Chip'
import './Autocomplete.css'

class Autocomplete extends Component {
    static propTypes = {
        tags: PropTypes.instanceOf(Array),
        suggestions: PropTypes.instanceOf(Array).isRequired
    };

    state = {
        suggestions: this.props.suggestions,
        // The active selection's index
        activeSuggestion: 0,
        // The suggestions that match the user's input
        filteredSuggestions: this.props.suggestions,
        // Whether or not the suggestion list is shown
        showSuggestions: false,
        userInput: "",
        tags: this.props.tags,
    };

    onChange = (e) => {
        const { suggestions } = this.state;
        const userInput = e.currentTarget.value;
        let filteredSuggestions;

        // Filter our suggestions that don't contain the user's input
        filteredSuggestions = suggestions.filter(
            suggestion =>
                suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
        );

        // Update the user input and filtered suggestions, reset the active
        // suggestion and make sure the suggestions are shown
        this.setState({
            activeSuggestion: 0,
            filteredSuggestions,
            showSuggestions: true,
            userInput: e.currentTarget.value
        });
    };

    // Event fired when the user clicks on a suggestion
    onClick = (e) => {
        let suggestions = [...this.state.suggestions];
        let index = suggestions.indexOf(e.currentTarget.innerText)
        if (index !== -1) {
            suggestions.splice(index, 1);
        }
        // Update the tags and reset the rest of the state
        this.setState({
            activeSuggestion: 0,
            suggestions,
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: '',
            tags: [...this.state.tags, e.currentTarget.innerText]
        });
        console.log(this.state.filteredSuggestions)
    };

    onTagDelete = (tag) => {
        console.log('TagDlete', tag)
        let tags = [...this.state.tags];
        let index = tags.indexOf(tag)
        if (index !== -1) {
            tags.splice(index, 1);
        }
        this.setState({
            tags,
            filteredSuggestions: [],
            suggestions: [...this.state.suggestions, tag]
        })
        console.log('filetred uggestions', this.state.filteredSuggestions)
    }

    // Event fired when the user presses a key down
    onKeyDown = (e) => {
        const { activeSuggestion, filteredSuggestions } = this.state;

        // User pressed the enter key, update the input and close the
        // suggestions
        if (e.keyCode === 13) {
            let suggestions = [...this.state.suggestions];
            let index = suggestions.indexOf(filteredSuggestions[activeSuggestion])
            if (index !== -1) {
                suggestions.splice(index, 1);
            }
            this.setState({
                activeSuggestion: 0,
                showSuggestions: false,
                userInput: '',
                suggestions,
                tags: [...this.state.tags, filteredSuggestions[activeSuggestion]]
            });
        }
        // User pressed the up arrow, decrement the index
        else if (e.keyCode === 38) {
            if (activeSuggestion === 0) {
                return;
            }

            this.setState({ activeSuggestion: activeSuggestion - 1 });
        }
        // User pressed the down arrow, increment the index
        else if (e.keyCode === 40) {
            if (activeSuggestion - 1 === filteredSuggestions.length) {
                return;
            }

            this.setState({ activeSuggestion: activeSuggestion + 1 });
        }
    };

    render() {
        const {
            onChange,
            onClick,
            onKeyDown,
            props: {
                placeholder,
                disabled
            },
            state: {
                activeSuggestion,
                filteredSuggestions,
                showSuggestions,
                userInput
            }
        } = this;

        let suggestionsListComponent;

        if (showSuggestions) {
            if (filteredSuggestions.length) {
                suggestionsListComponent = (
                    <ul className="suggestions">
                        {filteredSuggestions.map((suggestion, index) => {
                            let className;

                            // Flag the active suggestion with a class
                            if (index === activeSuggestion) {
                                className = "suggestion-active";
                            }

                            return (
                                <li
                                    className={className}
                                    key={suggestion}
                                    onClick={onClick}
                                >
                                    {suggestion}
                                </li>
                            );
                        })}
                    </ul>
                );
            } else {
                suggestionsListComponent = (
                    <div className="no-suggestions">
                        <em>No suggestions, search for something else</em>
                    </div>
                );
            }
        }

        return (
            <Fragment>
                <div className="autocomplete-container">
                    {/* Chip */}
                    {this.state.tags.map((tag) =>
                        <Chip
                            key={tag}
                            name={tag}
                            onClick={() => this.onTagDelete(tag)}
                        />
                    )}
                    <input
                        type="text"
                        onChange={onChange}
                        onKeyDown={onKeyDown}
                        value={userInput}
                        placeholder={placeholder ? placeholder : "Add Tag"}
                        disabled={disabled}
                    />
                    {suggestionsListComponent}
                </div>
            </Fragment>
        );
    }
}

Autocomplete.defaultProps = {
    suggestions: [],
    tags: []
}

Autocomplete.propTypes = {
    suggestions: PropTypes.arrayOf(PropTypes.string),
    tags: PropTypes.arrayOf(PropTypes.string)
}

export default Autocomplete;