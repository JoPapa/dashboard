import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next';
import SmartBag from '../images/smartbag-icon.png'

const WineElement = ({ headers, wine, filters, yearRange, isCollapsed }) => {
    const { t } = useTranslation();
    const [collapseTable, setCollapseTable] = useState(isCollapsed)
    // Check whether filters are being used.
    // With this variable we can show wines with no vintages, before filters are applied.
    // For example when there is a newly created wine with no vintages added directly.
    const isNotFiltering = filters.includes('bag') && filters.includes('bottle') && filters.includes('pending') && filters.includes('confirmed') && filters.includes('onSale') && filters.includes('notOnSale')
    // Filter vintages
    const vintages = wine.vintages.filter((vintage) => {
        // Create filters
        const sizeFilter = (filters.includes('bag') && vintage.sizes.bag && vintage.sizes.bag.length > 0) ||
            (filters.includes('bottle') && vintage.sizes.bottle && vintage.sizes.bottle.length > 0)
        const status = (filters.includes('pending') && vintage.confirmed === false) || (filters.includes('confirmed') && vintage.confirmed === true)
        const onSale = (filters.includes('onSale') && vintage.onSale === true) || (filters.includes('notOnSale') && vintage.onSale === false)
        const year = parseInt(vintage.wineVintage)
        const vintageRange = year <= yearRange.max && year >= yearRange.min
        // Use all filters
        return sizeFilter && status && onSale && vintageRange
    })

    const deleteVintage = () => {
        // TODO: Add logic to delete wines
        alert("Sei sicuro di voler eliminare questo vino?")
    }

    // TODO: Find a better solution than the one below
    const getMaximumPrice = (sizes) => {
        // Get maximum price between bags and bottles
        const bagMax = !sizes.bag.length > 0 ? 0 : sizes.bag.reduce((max, current) => (current.price > max ? current.price : max), sizes.bag[0].price)
        const bottleMax = !sizes.bottle.length > 0 ? 0 : sizes.bottle.reduce((max, current) => (current.price > max ? current.price : max), sizes.bottle[0].price)
        const res = bagMax > bottleMax ? bagMax : bottleMax
        return res
    }

    // TODO: Find a better solution than the one below
    const getMinimumPrice = (sizes) => {
        // Get minimum price between bags and bottles
        const bagMin = !sizes.bag.length > 0 ? 10000000 : sizes.bag.reduce((min, current) => (current.price < min ? current.price : min), sizes.bag[0].price)
        const bottleMin = !sizes.bottle.length > 0 ? 10000000 : sizes.bottle.reduce((min, current) => (current.price < min ? current.price : min), sizes.bottle[0].price)
        const res = bagMin < bottleMin ? bagMin : bottleMin
        return res
    }

    const element = () => {
        return (
            <React.Fragment>
                <div className="wine-title-header d-flex justify-content-between align-items-center">
                    <div className="wine-title text-left" onClick={() => setCollapseTable(!collapseTable)}>
                        <i className={`la la-chevron-${collapseTable ? "down" : "right"}`}></i>
                        <strong className="h4 ml-1">{`${wine.wineName} (${vintages.length})`}</strong>
                        {/* <strong className="h4 ml-1">{`${wine.wineName} (${wine.vintages.length})`}</strong> */}
                    </div>
                    <div>
                        <Link to={`new-wine?id=${wine.id}&editing=vintage`} className="btn btn-outline-success btn-round mr-1">
                            <i className="las la-plus"></i> {t('wine.add vintage')}
                        </Link>
                        <Link to={`new-wine?id=${wine.id}&editing=wine`} className="btn btn-outline-info btn-round mr-1">
                            <i className="la la-pen-nib"></i>
                        </Link>
                        <button onClick={() => alert("Sicuro di voler eliminare questo vino e tutti le sue annate")} className="btn btn-outline-danger btn-round">
                            <i className="la la-trash"></i>
                        </button>
                        {/* <Link to="new-wine">
                    <i className="la la-plus-circle danger"></i>
                </Link> */}
                    </div>
                </div>
                {collapseTable &&
                    <div className="table-responsive">
                        {/* Vintages Table */}
                        <table className="table table-hover table-lg text-center mb-0">
                            {/* Vintages Table Header */}
                            <thead>
                                <tr>
                                    {headers.map(header => <th key={header.field} className="border-top-0">{header.title}</th>)}
                                    <th className="border-top-0"></th>
                                </tr>
                            </thead>
                            {/* Vintages Table Rows */}
                            <tbody style={{ "width": "100%" }}>
                                {vintages.map(year => (
                                    <tr
                                        key={wine.id + year.wineVintage}
                                        className="text-center"
                                        style={{
                                            "borderLeft": `25px solid ${wine.type === "red" ? "#c23531" : wine.type === "white" ? "#dde20c" : "pink"}`
                                        }}>
                                        {headers.map(header => (
                                            <td key={header.field} className="align-middle">
                                                {header.field === 'img' &&
                                                    <div className="d-flex justify-content-center">
                                                        <img width="95px" height="95px" style={{ "display": "block" }} src={process.env.PUBLIC_URL + `/images/wines/${wine.id}.jpg`} alt={wine.name} />
                                                        <div className="ml-1">
                                                            <img width="45px" height="45px" style={{ "marginBottom": "5px", "display": "block" }} src={process.env.PUBLIC_URL + `/images/wines/${wine.id}.jpg`} alt={wine.name} />
                                                            <img width="45px" height="45px" style={{ "display": "block" }} src={process.env.PUBLIC_URL + `/images/wines/${wine.id}.jpg`} alt={wine.name} />
                                                        </div>
                                                    </div>
                                                }
                                                {header.field === 'year' &&
                                                    <div>
                                                        <div style={{
                                                            "height": "100%",
                                                            "width": "20px",
                                                            "background": "yellow"
                                                        }}>
                                                        </div>
                                                        {/* <Link to={`/wine?id=${wine.id}&year=${year.wineVintage}`} className="font-medium-4 text-primary"> */}
                                                            <strong className="font-medium-4 text-primary">{year.wineVintage}</strong>
                                                        {/* </Link> */}
                                                    </div>
                                                }
                                                {header.field === 'price' &&
                                                    <div className="font-medium-3 mt-1">
                                                        {(year.sizes.bag.length + year.sizes.bottle.length) > 1
                                                            ? <span>{t('settings.from')} <strong> {getMinimumPrice(year.sizes)}€</strong> {t('settings.to')} <strong>{getMaximumPrice(year.sizes)}€</strong></span>
                                                            : <strong>{year.sizes.bag.length > 0 ? year.sizes.bag[0].price : year.sizes.bottle[0].price}€</strong>
                                                        }
                                                    </div>
                                                }
                                                {header.field === 'uso' && year.salesPercentage &&
                                                    <>
                                                        <p className="h4">
                                                            {year.salesPercentage}
                                                        </p>
                                                        <div className="progress progress-sm mb-0">
                                                            <div className="progress-bar bg-danger" role="progressbar" style={{ "width": year.salesPercentage }} aria-valuenow={year.salesPercentage} aria-valuemin="0" aria-valuemax="100"></div>
                                                        </div>
                                                    </>
                                                }
                                                {header.field === 'review' &&
                                                    <div className="stars">
                                                        <p className="h4">4.5</p>
                                                        <i className="la la-star warning"></i>
                                                        <i className="la la-star warning"></i>
                                                        <i className="la la-star warning"></i>
                                                        <i className="la la-star warning"></i>
                                                        <i className="la la-star-half-alt warning"></i>
                                                    </div>
                                                }
                                                {header.field === 'format' &&
                                                    <div className="d-flex justify-content-around font-weight-bold">
                                                        {year.sizes.bag && year.sizes.bag.length > 0 &&
                                                            <div>
                                                                <img src={SmartBag} width="29px" height="29px" style={{ "objectFit": "contain" }} alt="smart bag icon" />
                                                                <p>{t('wine.bag')}</p>
                                                            </div>
                                                        }
                                                        {year.sizes.bottle.length > 0 &&
                                                            <div>
                                                                <i className={`las la-${year.format === 'bag' ? 'wine-glass-alt' : 'wine-bottle'} la-2x`}></i>
                                                                <p>{t('wine.bottle')}</p>
                                                            </div>
                                                        }
                                                    </div>
                                                }
                                                {header.field === 'on_sale' &&
                                                    <div>
                                                        <i className={`las la-${year.onSale ? 'check success' : 'times danger'} la-2x mt-1`}></i>
                                                        {!year.onSale &&
                                                            <button className="btn danger block" onClick={() => alert("You are not allowed to use this function yet.")}>
                                                                <span className="font-small-3">{t('wine.put on sale')} </span>
                                                                <i className="las la-arrow-right"></i>
                                                            </button>
                                                        }
                                                    </div>


                                                }
                                                {header.field === 'state' &&
                                                    <div className={`badge badge-pill badge-${year.confirmed === true ? "success" : "secondary"} font-weight-bold font-small-5 text-uppercase text-monospace mt-1`}>
                                                        {year.confirmed === true ? t('wine.confirmed') : t('wine.pending')}
                                                    </div>
                                                }
                                            </td>
                                        ))}
                                        <td className="pt-4 text-center">
                                            <div className="d-flex justify-content-around">
                                                <Link to={`new-wine?id=${wine.id}&vintage=${year.wineVintage}&editing=vintage`} className="text-dark p-1"><i className="las la-pen-nib la-2x"></i><p>{t('settings.edit')}</p></Link>
                                                <button className="text-dark p-1" onClick={() => deleteVintage()}><i className="las la-trash la-2x"></i><p>{t('settings.remove')}</p></button>
                                                <Link to={`/wine?id=${wine.id}&year=${year.wineVintage}`} className="text-center text-dark p-1"><i className="las la-arrow-right la-2x"></i><p>{t('settings.details')}</p></Link>
                                            </div>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                }
            </React.Fragment >
        )
    }

    return (
        isNotFiltering
            ? element()
            : vintages.length > 0 && element()
    )
}

export default WineElement