import React from 'react'

const WineAttribute = ({ attribute, value }) => {
    return (
        <div className="mt-1">
            {/* <i className="las la-calendar la-2x mr-1"></i> */}
            <div className="text-uppercase info">{attribute}</div>
            <div className="font-medium-5">{value}</div>
        </div>
    )
}

export default WineAttribute