import React from 'react'
import FullGlass from '../images/full-glass.png'
import EmptyGlass from '../images/empty-glass.png'

const WineLevel = ({ totalGlasses, glassesLeft }) => {
    return (
        <div className="d-flex">
            {[...Array(+totalGlasses).keys()].map(n =>
                n < glassesLeft
                    ? <img key={n} style={{ "margin": `${n === 0 ? "10px 2px 10px 0" : "10px 2px"}` }} src={FullGlass} width={'25px'} height={'18px'} alt="Full glass" />
                    : <img key={n} style={{ "margin": "10px 2px" }} src={EmptyGlass} width={'25px'} height={'18px'} alt="Empty glass" />
            )}
        </div>
    )
}

export default WineLevel