import React, { Component } from 'react'
import PropTypes from 'prop-types'

class FilterButton extends Component {
    state = {
        showFilter: false
    }

    toggleDropdown = () => {
        this.setState({ showFilter: !this.state.showFilter})
    }

    render () {
        return (
            <div className="btn-group mr-1">
                <button type="button" className="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="false" aria-expanded="false" onClick={this.toggleDropdown}>
                    Select Wine
                </button>
                
                    <div className={this.state.showFilter ? "dropdown-menu show" : "dropdown-menu"} x-placement="bottom-start" style={{position: "absolute", willChange: "transform", top: "0px", left: "0px", transform: "translate3d(0px, 40px, 0px)"}}>
                        <button className="dropdown-item active" type="button">All Wines</button>
                        <div className="dropdown-divider"></div>
                        <button className="dropdown-item" type="button">Sangiovese</button>
                        <button className="dropdown-item" type="button">Grechetto</button>
                    </div>

            </div>
        )
    }
}

FilterButton.propTypes = {
    data: PropTypes.array
}

export default FilterButton