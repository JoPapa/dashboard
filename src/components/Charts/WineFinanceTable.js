import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import Filter from '../Filter'
import Label from '../../images/albi-wine-label.jpg'
import MiniChart from 'react-mini-chart'

const WineFinanceTable = ({ headers, title, filter, data, onDelete, onDeleteAll, details }) => {
    const { t } = useTranslation()
    const [filteredData, setFilteredData] = useState(data)

    useEffect(() => {
        setFilteredData(data);
    }, [data])

    const filterSelection = [t('settings.all users')].concat(data.map(item => item.user))
        .filter((elem, index, self) => index === self.indexOf(elem))

    // You should be able to pass titles and rows for this table
    const filterRows = (value) => {
        console.log("Selected USer: ", value)
        value.toLowerCase() !== t('settings.all users').toLowerCase()
            ? setFilteredData(data.filter(item => item.user === value))
            : setFilteredData(data)
    }
    return (
        <div className="card">
            {/* Card Header */}
            <div className="card-header">
                <h4 className="card-title text-uppercase">{title}</h4>
                {/* Heading Elements */}
                <div className="heading-elements d-flex">
                    {filter &&
                        <Filter
                            onSelect={(user) => filterRows(user)}
                            selection={filterSelection}
                        />}
                </div>
            </div>
            <div className="card-body">
                <div className="table-responsive" style={{ "minHeight": "250px", "maxHeight": "100%" }}>
                    <table className="table table-hover table-md mb-0">
                        <thead>
                            <tr>
                                {headers.map(header => (
                                    <th key={header.field} className="border-top-0 text-center text-uppercase">{header.title}</th>
                                ))}
                                {onDelete && <th className="border-top-0"></th>}
                                {details && <th className="border-top-0"></th>}
                            </tr>
                        </thead>
                        <tbody>
                            {filteredData.length > 0
                                ? filteredData.map(row => (
                                    <tr key={row.wineId}>
                                        {headers.map(header => (
                                            <td key={header.field} className="text-center align-middle">
                                                {header.field === 'tag'
                                                    ? <div className="d-flex align-middle">
                                                        <i className={`la la-${row["tagType"] === "box" ? "box" : "wine-bottle"} pr-1`}></i>
                                                        <Link to={`/blockchain/box?id=${row["tag"]}`}>{row[header.field]}</Link>
                                                    </div>
                                                    : header.field === 'wine'
                                                        ? <div className="d-flex justify-content-left">
                                                            <div>
                                                                <img className="img-fluid rounded" src={Label} width={"55px"} height={"75px"} alt={`${row["wineName"]}'s label`} />
                                                            </div>
                                                            <div className="font-medium-2 text-left px-1">
                                                                <div className="font-weight-bold">{row.wineName}</div>
                                                                <div>{row.winery}</div>
                                                            </div>
                                                        </div>
                                                        : header.field === 'weekPerformance'
                                                            ? <MiniChart
                                                                strokeColor="#c23531"
                                                                activePointColor="#c23531"
                                                                activePointRadius={8}
                                                                strokeWidth={5}
                                                                labelFontSize={20}
                                                                width={200}
                                                                height={80}
                                                                dataSet={Object.values(row[header.field])}
                                                            />

                                                            : header.field === 'sold'
                                                                ? <>
                                                                    <i className={"las la-wine-glass-alt la-3x"}></i>
                                                                    <div className="font-medium-5">{row[header.field]}</div>
                                                                </>

                                                                : header.field === 'revenue'
                                                                    ? <>
                                                                        <i className={"las la-dollar-sign la-3x"}></i>
                                                                        <div className="font-medium-5">{row[header.field]}</div>
                                                                    </>
                                                                    : header.field === 'roi'
                                                                        ? <>
                                                                            <i className={"las la-percent la-3x"}></i>
                                                                            <div className="font-medium-5">{row[header.field]}</div>
                                                                        </>
                                                                        : header.field === 'type'
                                                                            // Add the different types of logs to this
                                                                            ? <span><i className={`las la-${row[header.field] === "Check" ? "check-circle success" : "user-plus info"} mr-1`}></i>{row[header.field]}</span>
                                                                            : row[header.field]
                                                }
                                            </td>
                                        ))}
                                        {onDelete &&
                                            <td>
                                                <button className="btn btn-danger p-0" onClick={() => onDelete(row.id)}>
                                                    <i className="la la-close"></i>
                                                </button>
                                            </td>
                                        }
                                        {details &&
                                            <td>
                                                <button className="text-center text-dark align-items-center" onClick={details}>
                                                    <span>{t('settings.details')}</span>
                                                    <i className="las la-arrow-right la-1x"></i>
                                                </button>
                                            </td>
                                        }
                                    </tr>
                                ))
                                : <div className="text-center">
                                    There is no data
                                        </div>
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

WineFinanceTable.defaultProps = {
    headers: [],
    data: []
}

WineFinanceTable.propTypes = {
    title: PropTypes.string,
    headers: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        field: PropTypes.string.isRequired
    })),
    data: PropTypes.array.isRequired
}

export default WineFinanceTable


