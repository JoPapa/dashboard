import React from 'react'
// TODO: Add proptypes
const TempMedia = (props) => {
    const MaxMin = (max, min) => {
        return (
            <div className="col-md-6 col-12 my-1 py-2 font-medium-1 align-middle">
                <div className="d-flex justify-content-around">
                    <span className="font-weight-bold mb-1">MAX</span>
                    <span>{max}°C</span>
                </div>
                <div className="d-flex justify-content-around">
                    <span className="font-weight-bold">MIN</span>
                    <span>{min}°C</span>
                </div>
            </div>
        )
    }
    const { red, white, rose } = props.temperatures
    return (
        <div className="card">
            <div className="card-body pt-0">
                <div className="card-header">
                    <h4 className="card-title text-center">{props.title}</h4>
                </div>
                <div className="row my-1">
                    <div className={`col-md-6 col-12 text-center d-inline-block ${props.large && "py-1"}`}>
                        <i className="font-large-3 danger la la-wine-glass-alt"></i>
                        <h4 className="font-large-1">{red.average}°C</h4>
                        <p className="blue-grey lighten-2 mb-0"></p>
                    </div>
                    {props.large && MaxMin(red.maximum, red.minimum)}
                    <hr />
                    <div className={`col-md-6 col-12 text-center d-inline-block ${props.large && "py-1"}`}>
                        <i className="font-large-3 yellow darken-3 la la-wine-glass-alt"></i>
                        <h4 className="font-large-1">{white.average}°C</h4>
                        <p className="blue-grey lighten-2 mb-0"></p>
                    </div>
                    {props.large && MaxMin(white.maximum, white.minimum)}
                    <div className={`col-md-6 col-12 text-center d-inline-block ${props.large && "py-1"}`}>
                        <i className="font-large-3 pink lighten-3 la la-wine-glass-alt"></i>
                        <h4 className="font-large-1">{rose.average}°C</h4>
                        <p className="blue-grey lighten-2 mb-0"></p>
                    </div>
                    {props.large && MaxMin(rose.maximum, rose.minimum)}
                </div>
            </div>
        </div>
    )
}

export default TempMedia


// OLD VERSION

// const MaxMin = () => {
//     return (
//         <div className="d-flex justify-content-center my-1 font-medium-1">
//             <div className="px-1">
//                 <div className="font-weight-bold">MAX</div>
//             18°C
//             </div>
//             <div className="px-1">
//                 <div className="font-weight-bold">MIN</div>
//             6°C
//             </div>
//         </div>
//     )
// }
// return (
//     <div className="card">
//         <div className="card-body pt-0">
//             <div className="card-header">
//                 <h4 className="card-title text-center">
//                     Temperatura media
//                 </h4>
//             </div>
//             <div className="row">
//                 <div className={`col-12 text-center d-inline-block ${props.large && "py-1"}`}>
//                     <h5 className="font-large-1">
//                         <i className="font-large-1 danger la la-wine-glass-alt"></i>
//                         18°C
//                     </h5>
//                     {props.large && MaxMin()}
//                 </div>
//                 <div className="col-12 text-center d-inline-block py-1">
//                     <h5 className="font-large-1">
//                         <i className="font-large-1 yellow darken-3 la la-wine-glass-alt"></i>
//                         12°C
//                     </h5>
//                     {props.large && MaxMin()}
//                 </div>
//                 <div className={`col-12 text-center d-inline-block ${props.large && "py-1"}`}>
//                     <h5 className="font-large-1">
//                         <i className="font-large-1 pink lighten-3 la la-wine-glass-alt"></i>
//                         4°C
//                     </h5>
//                     {props.large && MaxMin()}
//                 </div>
//             </div>
//         </div>
//     </div>
// )