import React from 'react'

const SingleDataCard = (props) => {
    return (
        <div className="card">
            <div className="card-content">
                <div className="card-body">
                    <h4 className="text-muted pb-2">{props.title}</h4>
                    <i className={`la la-${props.iconName} font-large-3 float-left`}></i>
                    <div className="font-large-2 pb-2 text-center">{props.value}</div>
                </div>
            </div>
        </div>
    )
}

export default SingleDataCard