import React from 'react'
import ReactEcharts from 'echarts-for-react'
import PropTypes from 'prop-types'

const SmoothLineChart = (props) => {
    let series = props.data
    const myOption = {
        tooltip: {
            trigger: 'show',
            formatter: "{a} <br/>{b} : {c} {d} {c} ({d}%)"
        },
        // legend: props.legend ? {} : '',
        // calculable: true,
        xAxis: [
            {
                type: 'category',
                data: props.data.map(el => el.name)
            }
        ],
        yAxis: [
            {
                type: "value",
                name: "",
            }
        ],
        series: [{
            data: series,
            type: 'line',
            lineStyle: {
                width: 3,
                shadowColor: 'rgba(0,0,0,0.4)',
                shadowBlur: 10,
                shadowOffsetY: 10
            },
            smooth: true
        }]
    };


    return (
        <ReactEcharts option={myOption} />
    )
}

SmoothLineChart.defaultProps = {
    data: [
        { name: 'Lun', value: 10 },
        { name: 'Mar', value: 20 },
        { name: 'Mer', value: 30 },
        { name: 'Gio', value: 25 },
        { name: 'Ven', value: 12 },
        { name: 'Sab', value: 3 },
        { name: 'Dom', value: 6 }
    ],
    data2: null,
    legend: null
}

SmoothLineChart.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        value: PropTypes.number.isRequired
    })),
    legend: PropTypes.string
}

export default SmoothLineChart