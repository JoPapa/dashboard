import React from 'react'
import ReactEcharts from 'echarts-for-react'
import PropTypes from 'prop-types'

const DonutChart = (props) => {
    // props.. 
    // legend_ opzionale,
    // data_ da inserire con parametri value, name
    // radius_
    // center_

    const myOption = {
        // animation: false,
        showSymbol: true,
        tooltip: {
            trigger: 'item',
            formatter: '{b} {d}%'
        },
        legend: {
            orient: 'horizontal',
            selectedMode: false,
            bottom: 0,
            data: ['Cina', 'Italia', 'Spagna', 'Francia']
        },
        series: [
            {
                name: '',
                type: 'pie',
                radius: ['40%', '60%'],
                avoidLabelOverlap: false,
                label: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    label: {
                        show: true,
                        fontSize: '20',
                        fontWeight: 'bold'
                    }
                },
                labelLine: {
                    show: true
                },
                data: [
                    {
                        value: 335, name: 'Cina', countryCode: 'CN',
                        // selected: props.selectedRegion === 'CN' ? true : false,
                        label: {
                            show: props.selectedRegion === 'CN' ? true : false,
                            fontSize: '20',
                            fontWeight: 'bold',
                            formatter: `{b}\n\n {d}%`
                        }
                    },
                    {
                        value: 310, name: 'Italia', countryCode: 'IT',
                        // selected: props.selectedRegion === 'IT' ? true : false,
                        label: {
                            show: props.selectedRegion === 'IT' ? true : false,
                            fontSize: '20',
                            fontWeight: 'bold',
                            formatter: `{b}\n\n {d}%`
                        }
                    },
                    {
                        value: 234, name: 'Spagna', countryCode: 'ES',
                        // selected: props.selectedRegion === 'ES' ? true : false,
                        label: {
                            show: props.selectedRegion === 'ES' ? true : false,
                            fontSize: '20',
                            fontWeight: 'bold',
                            formatter: `{b}\n\n {d}%`
                        }
                    },
                    {
                        value: 135, name: 'Francia', countryCode: 'FR',
                        // selected: props.selectedRegion === 'FR' ? true : false,
                        label: {
                            show: props.selectedRegion === 'FR' ? true : false,
                            fontSize: '20',
                            fontWeight: 'bold',
                            formatter: `{b}\n\n {d}%`
                        }
                    }
                ]
            }
        ]
    };

    const onClick = (params) => {
        console.log("Params", params)
        console.log("Params Code: ", params.data.countryCode)
        props.onClick && props.onClick(params.data.countryCode)
    }

    let onEvents = {
        'click': onClick,
        'legendselected': onClick,
        'legendselectedchange': onClick
    }

    return <ReactEcharts
        onClick={(e) => console.log(e)}
        option={myOption}
        style={{ height: '350px' }}
        onEvents={onEvents}
    />
}

DonutChart.defaultProps = {
    radius: '50%',
    center: ['50%', '50%'],
    data: [
        { value: 335, name: '18-30' },
        { value: 310, name: '31-45' },
        { value: 234, name: '45-54' },
        { value: 135, name: '55-64' },
        { value: 1548, name: 'over 65' }
    ]
};

DonutChart.propTypes = {
    radius: PropTypes.string,
    center: PropTypes.array,
    data: PropTypes.array
};

export default DonutChart