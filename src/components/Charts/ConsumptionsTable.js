import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'

const ConsumptionsTable = ({ title, headers, data, onDelete }) => {
    return (
        <div className="row">
            <div className="col-12">
                <div className="card">
                    {/* Card Header */}
                    <div className="card-header">
                        <h4 className="card-title">{title}</h4>
                        {/* Heading Elemnts */}
                        <div className="heading-elements d-flex">
                            {/* <ul className="list-inline mb-0">
                                <li><a className="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="invoice-summary.html" target="_blank">Invoice Summary</a></li>
                            </ul> */}

                            {/* <button className="btn blue" onClick={() => {
                                window.confirm("Non è possibile aggiornare i dati in questo momento.")
                            }}>
                                <i className="la la-redo-alt" />
                            </button> */}
                        </div>
                    </div>
                    <div className="card-content">
                        <div className="table-responsive" style={{ "minHeight": "250px", "maxHeight": "350px" }}>
                            <table className="table table-hover table-md mb-0">
                                <thead>
                                    <tr>
                                        {headers.map(header => <th key={header.field} className="border-top-0">{header.title}</th>)}
                                        {onDelete && <th className="border-top-0"></th>}
                                    </tr>
                                </thead>
                                <tbody>
                                    {data.map((row, index) => (
                                        <tr key={index}>
                                            {headers.map(header => (
                                                <td key={header.field} className="align-middle">
                                                    {header.field === 'tagId' ?
                                                        <Link to='/blockchain/box'>{row[header.field]}</Link> :
                                                        header.field === 'wine'
                                                            ? <div>{row[header.field]}<div className="font-weight-light font-weight-bold text-muted">{row['winery']}</div></div>
                                                            : header.field === 'alertLevel'
                                                                ? <i className={"la la-bullseye " + (row[header.field] === 'High' ? 'danger' : row[header.field] === 'Medium' ? 'warning' : 'success')}></i>
                                                                : row[header.field]
                                                    }
                                                </td>
                                            ))}
                                            {onDelete && (
                                                <td>
                                                    <button className="btn btn-danger p-0" onClick={() => window.confirm("Questo alert verrà eliminato definitivamente, sei sicuro?")}>
                                                        <i className="la la-close"></i>
                                                    </button></td>
                                            )}
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

ConsumptionsTable.propTypes = {
    title: PropTypes.string,
    headers: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        field: PropTypes.string.isRequired
    })),
    data: PropTypes.array.isRequired
}

export default ConsumptionsTable