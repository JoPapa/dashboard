import React from 'react'
import { Link } from 'react-router-dom'

const ResponsiveAlertTable = (props) => {
    // You should be able to pass titles and rows for this table
    return (
        <div className="card">
            {/* Card Header */}
            <div className="card-header">
                <h4 className="card-title">{props.title}</h4>
                <a className="heading-elements-toggle" href="##">
                    <i className="la la-ellipsis-v font-medium-3"></i>
                </a>
                {/* Heading Elemnts */}
                <div className="heading-elements">
                    {/* <ul className="list-inline mb-0">
                        <li><a className="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="invoice-summary.html" target="_blank">Invoice Summary</a></li>
                    </ul> */}
                </div>
            </div>            
            <div className="card-content">                
                <div className="table-responsive">
                    <table id="recent-orders" className="table table-hover table-xl mb-0">
                        <thead>
                            <tr>
                                <th className="border-top-0">Status</th>                                
                                <th className="border-top-0">TAG#</th>
                                <th className="border-top-0">Wine</th>
                                <th className="border-top-0">Ultima Localizzazione</th>
                                <th className="border-top-0">Data</th>
                                <th className="border-top-0">Ora</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className="text-truncate"><i className="la la-dot-circle-o success font-medium-1 mr-1"></i> Paid</td>
                                <td className="text-truncate"><Link to={props.link}>INV-001001</Link></td>
                                <td className="text-truncate">
                                    <span>None</span>
                                </td>
                                <td className="text-truncate p-1">
                                    Roma, Itlay
                                </td>
                                <td>
                                    Mon, 24/10/2019
                                </td>
                                <td>
                                    10:46 AM
                                </td>
                            </tr>
                            <tr>
                                <td className="text-truncate"><i className="la la-dot-circle-o danger font-medium-1 mr-1"></i> Declined</td>
                                <td className="text-truncate"><Link to={props.link}>INV-001002</Link></td>
                                <td className="text-truncate">
                                    <span>Sangiovese Rosso</span>
                                </td>
                                <td className="text-truncate p-1">
                                    Bologna, Italy
                                </td>
                                <td>
                                    Tue, 03/05/2019
                                </td>
                                <td>
                                    12:54 PM
                                </td>
                            </tr>
                            <tr>
                                <td className="text-truncate"><i className="la la-dot-circle-o warning font-medium-1 mr-1"></i> Pending</td>
                                <td className="text-truncate"><Link to={props.link}>INV-001003</Link></td>
                                <td className="text-truncate">
                                    <span>Passero S.</span>
                                </td>
                                <td className="text-truncate p-1">
                                    Ancona, Italy
                                </td>
                                <td>
                                    Wen, 17/05/2019
                                </td>
                                <td>
                                    11:43 AM
                                </td>
                            </tr>
                            <tr>
                                <td className="text-truncate"><i className="la la-dot-circle-o success font-medium-1 mr-1"></i> Paid</td>
                                <td className="text-truncate"><Link to={props.link}>INV-001004</Link></td>
                                <td className="text-truncate">
                                    <span>Umbro Rosso IGT</span>
                                </td>
                                <td className="text-truncate p-1">
                                    Milan, Italy
                                </td>
                                <td>
                                    Mon, 21/05/2019
                                </td>
                                <td>
                                    04:34 PM
                                </td>
                            </tr>
                            <tr>
                                <td className="text-truncate"><i className="la la-dot-circle-o success font-medium-1 mr-1"></i> Paid</td>
                                <td className="text-truncate"><Link to={props.link}>INV-001005</Link></td>
                                <td className="text-truncate">
                                    <span>Grechetto DOC</span>
                                </td>
                                <td className="text-truncate p-1">
                                    Florence, MI
                                </td>
                                <td>
                                    Sun, 09/07/2019
                                </td>
                                <td>
                                    09:09 PM 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

// ResponsiveAlertTable.proptypes = {
// }

export default ResponsiveAlertTable