import React from 'react'
import ReactEcharts from 'echarts-for-react'
import PropTypes from 'prop-types'

const CircleChart = (props) => { 
    let dataStyle = {
        normal: {
            label: {show:false},
            labelLine: {show:false}
        }
    };
    let placeHolderStyle = {
        normal : {
            color: 'rgba(0,0,0,0)',
            label: {show:false},
            labelLine: {show:false}
        },
        emphasis : {
            color: 'rgba(0,0,0,0)'
        }
    };
    const theOption = {
        tooltip : {
            show: true,
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient : 'horizontal',
            x : 'center',
            y : 'top',
            itemGap: 12,
            data:['Rosso','Bianco','Rosato']
        },
        series : [
            {
                name:'Rosso',
                type:'pie',
                clockWise:false,
                radius : [80, 100],
                itemStyle : dataStyle,
                data:[
                    {
                        value:42,
                        name:'Rosso'
                    },
                    {
                        value:58,
                        name:'invisible',
                        itemStyle : placeHolderStyle
                    }
                ]
            },
            {
                name:'Bianco',
                type:'pie',
                clockWise:false,
                radius : [60, 80],
                itemStyle : dataStyle,
                data:[
                    {
                        value:38, 
                        name:'Bianco'
                    },
                    {
                        value:62,
                        name:'invisible',
                        itemStyle : placeHolderStyle
                    }
                ]
            },
            {
                name:'Rosato',
                type:'pie',
                clockWise:false,
                radius : [40, 60],
                itemStyle : dataStyle,
                data:[
                    {
                        value:20, 
                        name:'Rosato'
                    },
                    {
                        value:80,
                        name:'invisible',
                        itemStyle : placeHolderStyle
                    }
                ]
            }
        ]
    };
                        
                                       
    return (
        <ReactEcharts option={theOption} />
    )
}

CircleChart.propTypes = {
    data: PropTypes.array
}

export default CircleChart