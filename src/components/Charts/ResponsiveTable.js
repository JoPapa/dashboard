import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import Filter from '../Filter'

const ResponsiveTable = ({ headers, title, filter, data, onDelete, onDeleteAll, details }) => {
    const { t } = useTranslation()
    const [filteredData, setFilteredData] = useState(data)

    useEffect(() => {
        setFilteredData(data);
    }, [data])

    const filterSelection = [t('settings.all users')].concat(data.map(item => item.user))
        .filter((elem, index, self) => index === self.indexOf(elem))

    // You should be able to pass titles and rows for this table
    const filterRows = (value) => {
        console.log("Selected USer: ", value)
        value.toLowerCase() !== t('settings.all users').toLowerCase()
            ? setFilteredData(data.filter(item => item.user === value))
            : setFilteredData(data)
    }
    return (
        <div className="row">
            <div className="col-12">
                <div className="card">
                    {/* Card Header */}
                    <div className="card-header">
                        <h4 className="card-title">{title}</h4>
                        {/* Heading Elemnts */}
                        <div className="heading-elements d-flex">
                            {/* <ul className="list-inline mb-0">
                                <li><a className="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="invoice-summary.html" target="_blank">Invoice Summary</a></li>
                            </ul> */}
                            {filter &&
                                <Filter
                                    onSelect={(user) => filterRows(user)}
                                    selection={filterSelection}
                                />}

                            {/* <button className="btn blue" onClick={() => {
                                window.confirm("Non è possibile aggiornare i dati in questo momento.")
                            }}>
                                <i className="la la-redo-alt" />
                            </button> */}
                            {onDeleteAll &&
                                <button className="btn blue" onClick={onDeleteAll}>
                                    {t('settings.delete all')}
                                </button>
                            }
                        </div>
                    </div>
                    <div className="card-content">
                        <div className="table-responsive" style={{ "minHeight": "250px", "maxHeight": "350px" }}>
                            <table className="table table-hover table-md mb-0">
                                <thead>
                                    <tr>
                                        {headers.map(header => (
                                            <th key={header.field} className="border-top-0">{header.title}</th>
                                        ))}
                                        {onDelete && <th className="border-top-0"></th>}
                                        {details && <th className="border-top-0"></th>}
                                    </tr>
                                </thead>
                                <tbody>
                                    {filteredData.length > 0
                                        ? filteredData.map(row => (
                                            <tr key={row.id}>
                                                {headers.map(header => (
                                                    <td key={header.field}>
                                                        {header.field === 'tag'
                                                            ? <div className="d-flex align-middle">
                                                                <i className={`la la-${row["tagType"] === "box" ? "box" : "wine-bottle"} pr-1`}></i>
                                                                <Link to={`/blockchain/box?id=${row["tag"]}`}>{row[header.field]}</Link>
                                                            </div>
                                                            : header.field === 'wine' ?
                                                                <div>{row[header.field]}<div className="font-weight-light font-italic text-muted">{row['winery']}</div></div>
                                                                :
                                                                header.field === 'alertLevel' ?
                                                                    <i className={"la la-bullseye " + (row[header.field] === 'High' ? 'danger' : row[header.field] === 'Medium' ? 'warning' : 'success')}></i>
                                                                    : header.field === 'type'
                                                                        // Add the different types of logs to this
                                                                        ? <span><i className={`las la-${row[header.field] === "Check" ? "check-circle success" : "user-plus info"} mr-1`}></i>{row[header.field]}</span>
                                                                        : row[header.field]
                                                        }
                                                    </td>
                                                ))}
                                                {onDelete &&
                                                    <td>
                                                        <button className="btn btn-danger p-0" onClick={() => onDelete(row.id)}>
                                                            <i className="la la-close"></i>
                                                        </button>
                                                    </td>
                                                }
                                                {details &&
                                                    <td>
                                                        <button className="text-center text-dark align-items-center" onClick={details}>
                                                            <span>{t('settings.details')}</span>
                                                            <i className="las la-arrow-right la-1x"></i>
                                                        </button>
                                                    </td>
                                                }
                                            </tr>
                                        ))
                                        : <div className="text-center">
                                            There is no data
                                        </div>
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )
}

ResponsiveTable.defaultProps = {
    headers: [],
    data: []
}

ResponsiveTable.propTypes = {
    title: PropTypes.string,
    headers: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        field: PropTypes.string.isRequired
    })),
    data: PropTypes.array.isRequired
}

export default ResponsiveTable