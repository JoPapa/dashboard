import React from 'react'
import ReactEcharts from 'echarts-for-react'
import PropTypes from 'prop-types'

const Histogram = (props) => {
    let series = props.data2 ? (
        [
            {
                name: 'Emisfero Nord',
                type: 'bar',
                data: props.data.map(el => parseInt(el.value))
            },
            {
                name: 'Emisfero Sud',
                type: 'bar',
                data: props.data2.map(el => parseInt(el.value))
            }
        ]
    ) : (
            [
                {
                    name: 'Consumo',
                    type: 'bar',
                    data: props.data.map(el => parseInt(el.value))
                }
            ]
        );
    // Calculate maximum value
    const total = props.data2 ? (props.data2.reduce((acc, curr) => acc + parseInt(curr.value), 0)) : (props.data.reduce((acc, curr) => acc + parseInt(curr.value), 0))

    const myOption = {
        tooltip: {
            trigger: 'show',
            formatter: "{a} <br/>{b} : {c} {d} {c} ({d}%)"
        },
        // legend: props.legend ? {} : '',
        // calculable: true,
        xAxis: [
            {
                type: 'category',
                data: props.data.map(el => el.name)
            }
        ],
        yAxis: [
            {
                type: "value",
                name: "",
                // max: total,
                // min: 0,
                axisLabel: {
                    formatter: function (value) {
                        return ((value * 100) / total).toFixed(0) + "%"
                    }
                }
            }
        ],
        series: series
    };


    return (
        <ReactEcharts option={myOption} />
    )
}

Histogram.defaultProps = {
    data: [
        { name: 'Lun', value: 10 },
        { name: 'Mar', value: 20 },
        { name: 'Mer', value: 30 },
        { name: 'Gio', value: 25 },
        { name: 'Ven', value: 12 },
        { name: 'Sab', value: 3 },
        { name: 'Dom', value: 6 }
    ],
    data2: null,
    legend: null
}

Histogram.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        value: PropTypes.number.isRequired
    })),
    legend: PropTypes.string
}

export default Histogram