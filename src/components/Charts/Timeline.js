import React, { useState } from 'react'
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types'
import './Timeline.css'
import Scrollable from '../Scrollable'

const EventLine = ({ event, onSelect }) => {
    // const { t } = useTranslation();
    return (
        <li className="">
            {/* <div className="timeline-date header">{t('blockchain.timeline.day')}</div> */}
            <div className="timeline-date flex-fill">{event.timestamp}</div>
            <div className="timeline-date location">{event.location}</div>
            <button onClick={() => onSelect(event.description)} className={`timeline-point`}></button>
            <div className="timeline-message">{event.message}</div>
        </li>
    )
}

const Timeline = (props) => {
    const { t } = useTranslation();
    // TODO: Aspetta le icone di flavio prima di completare la TimeLine
    const [description, setDescription] = useState('')
    return (
        <div className="card">
            <div className="card-header">
                <h4 className="card-title">{t('blockchain.timeline.title')}</h4>
            </div>
            <div className="card-content">
                <div className="card-body m-1">
                    <Scrollable>
                        <section className="timeline">
                            <ol className="d-inline-flex align-items-center">
                                {props.data.map((event) =>
                                    <EventLine key={event.timestamp} event={event} onSelect={(selected) => setDescription(selected)} />
                                )}
                            </ol>
                            <div className="font-medium-5 text-center mb-2">{description}</div>
                        </section>
                    </Scrollable>
                </div>
            </div>
        </div>
    )
}

Timeline.defaultProps = {
    data: []
}

Timeline.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default Timeline