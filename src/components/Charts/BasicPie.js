import React from 'react'
import ReactEcharts from 'echarts-for-react'
import PropTypes from 'prop-types'

const BasicPie = (props) => {
    // props.. 
    // legend_ opzionale,
    // data_ da inserire con parametri value, name
    // radius_
    // center_

    const myOption = {
        tooltip : {
            trigger: 'item',
            formatter: "{d}%"
        },
        legend: props.legend ? {} : '',
        calculable : true,
        series : [
            {
                name:'Day',
                type:'pie',
                radius : props.radius,
                center: props.center,
                data: props.data
            }
        ]
    };              
                        
        return (
            <ReactEcharts option={myOption} />
            )
}

BasicPie.defaultProps = {
    radius: '50%',
    center: ['50%', '50%'],
    data: [
        {value:335, name:'18-30'},
        {value:310, name:'31-45'},
        {value:234, name:'45-54'},
        {value:135, name:'55-64'},
        {value:1548, name:'over 65'}
    ]
};

BasicPie.propTypes = {
    radius: PropTypes.string,
    center: PropTypes.array,
    data: PropTypes.array
};

export default BasicPie