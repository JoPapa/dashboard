import React from 'react'
import { VectorMap } from 'react-jvectormap'
import PropTypes from 'prop-types'

const Map = (props) => {

    return (
        <VectorMap
            map={"world_mill"}
            backgroundColor="transparent" //change it to blue !!!
            zoomOnScroll={false}
            containerStyle={{
                width: props.width,
                height: props.height,
            }} //gets the country code
            containerClassName="map"
            regionStyle={{
                initial: {
                    fill: "#e4e4e4",
                    "fill-opacity": 0.9,
                    stroke: "none",
                    "stroke-width": 0,
                    "stroke-opacity": 0
                },
                hover: {
                    "fill-opacity": 0.8,
                    cursor: 'pointer'
                },
                selected: {
                    fill: '#2f4554'  //what colour clicked country will be
                },
                selectedHover: {
                }
            }}
            regionsSelectable={false}
            selectedRegions={props.selectedRegion}
            series={{
                regions: [
                    {
                        values: props.data,  //this is your data
                        scale: ["#FFB0B2", "#d53a35"],  //your color game's here
                        normalizeFunction: "polynomial"
                    }
                ]
            }}
            markerStyle={{
                initial: {
                    fill: '#f83d3b',
                    stroke: '#cccccc'
                }
            }}
            markers={props.markers}
            // onRegionTipShow={(e, el, code) =>{
            //     const number = props.data[code] ? props.data[code] : null;
            //     el.html(`${el.html()}${number ? ' - ' + number : ''}`);
            //   }}
        />
    )
}

Map.defaultProps = {
    width: "100%",
    height: "100%",
    data: {},
    markers: []
}

Map.propTypes = {
    data: PropTypes.object,
    width: PropTypes.string,
    height: PropTypes.string
}

export default Map