import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import avatar from '../../images/user-avatar.png'
import AddUserModal from '../../components/AddUserModal'

const permissions = [
    {
        role: "role_1",
        page: "Analytics",
        description: "Può consultare i dati analytics su tutti i vini"
    },
    {
        role: "role_2",
        page: "I miei vini",
        description: "Può vedere tutti i vini"
    },
    {
        role: "role_3",
        page: "I miei vini",
        description: "Permette di creare nuovi vini"
    },
    {
        role: "role_4",
        page: "I miei vini",
        description: "Permette la modifica di tutti i vini"
    },
    {
        role: "role_5",
        page: "I miei vini",
        description: "Permette di elimina tutti i vini"
    },
]

const UserTable = (props) => {
    const { t } = useTranslation();
    let { headers, data, onDelete } = props;
    const [modalIsOpen, setIsOpen] = React.useState(false);
    const [editingUser, setEditingUser] = React.useState('');
    const [userPermissions, setPermissions] = React.useState(data);

    function openModal() {
        setIsOpen(true);
    }

    function closeModal() {
        setIsOpen(false);
    }
    const editUser = (userId) => {
        setEditingUser(userId)
    }
    const cancelEdit = () => {
        setEditingUser()
        setPermissions(data)
    }
    const updatePermissions = (userId, role, e) => {
        console.log("Changin for this user: ", userId)
        console.log("this permission: ", role)
        console.log("E: ", e.target.checked)
        const newArr = userPermissions.map(item => item.userId === userId ? { ...item, [role]: e.target.checked } : item);
        console.log("Result: ", newArr)
        setPermissions(newArr);
    }
    // You should be able to pass titles and rows for this table
    return (
        <div className="row">
            <div className="col-12">
                <div className="card">
                    {/* Card Header */}
                    <div className="card-header">
                        <h4 className="card-title text-uppercase">{props.title}</h4>
                        {/* Heading Elemnts */}
                        <div className="heading-elements">
                            {/* <ul className="list-inline mb-0">
                                <li><a className="btn btn-sm btn-danger box-shadow-2 round btn-min-width pull-right" href="invoice-summary.html" target="_blank">Invoice Summary</a></li>
                            </ul> */}
                        </div>
                    </div>
                    <div className="card-content">
                        <div className="table-responsive">
                            <table id="recent-orders" className="table table-hover table-xl mb-0">
                                <thead className="text-center">
                                    <tr>
                                        <th className="border-top-0"><p>{t('admin.permissions')}</p></th>
                                        {headers.map((header, index) => (
                                            <th key={index} className="border-top-0">
                                                <div className="avatar">
                                                    <img src={avatar} alt="avatar" />
                                                </div>
                                                <div className="font-medium-1">{`${header.name} ${header.surname}`}</div>
                                                <div className="font-small-4">{`${header.field}`}</div>
                                                {editingUser === header.userId
                                                    ? <div>
                                                        {/* TODO: SAVE FOR REAL */}
                                                        <div>
                                                        <button type="button" className="btn btn-outline-danger round" style={{ "padding": "5px", "marginTop": "5px" }} onClick={() => alert("You can't remove any account into your organization")}>
                                                            <span style={{ "padding": "0 5px" }}>{t('settings.remove account')}</span>
                                                            <i className="las la-times"></i>
                                                        </button>
                                                        </div>
                                                        <button type="button" className="btn btn-outline-info round" style={{ "padding": "5px", "marginTop": "5px" }} onClick={() => editUser()}>
                                                            <span style={{ "padding": "0 5px" }}>{t('settings.save')}</span>
                                                            <i className="las la-check"></i>
                                                        </button>
                                                        <button type="button" className="btn btn-outline-secondary round ml-1" style={{ "padding": "5px", "marginTop": "5px" }} onClick={() => cancelEdit()}>
                                                            <span style={{ "padding": "0 5px" }}>{t('settings.cancel')}</span>
                                                            <i className="las la-times"></i>
                                                        </button>
                                                    </div>
                                                    : <button type="button" className="btn btn-outline-info round" style={{ "padding": "5px", "marginTop": "5px" }} onClick={() => editUser(header.userId)}>
                                                        <span style={{ "padding": "0 5px" }}>{t('settings.edit')}</span>
                                                        <i className="las la-cog"></i>
                                                    </button>
                                                }
                                            </th>
                                        ))}
                                        <th className="border-top-0">
                                            <button onClick={openModal}>
                                                <i className="las la-plus-circle la-2x" />
                                            </button>
                                            <p>{t('admin.add new user')}</p>
                                        </th>
                                        {onDelete && (
                                            <th className="border-top-0"></th>
                                        )}
                                    </tr>
                                </thead>
                                <tbody>
                                    {/* A row for each permission */}
                                    {permissions.map(permission =>
                                        <tr key={permission.role} className="text-center">
                                            <td>
                                                <div className="font-small-4 font-weight-bold">{permission.page}</div>
                                                <div className="font-small-3">{permission.description}</div>
                                            </td>
                                            {userPermissions.map(user =>
                                                <React.Fragment key={user.userId}>
                                                    {editingUser === user.userId
                                                        ? <td>
                                                            <input type="checkbox" checked={user[permission.role]} onChange={(e) => updatePermissions(user.userId, permission.role, e)} />
                                                        </td>
                                                        : <td>
                                                            <i className={`las la-${user[permission.role] ? "check success" : "times danger"} la-2x`}></i>
                                                        </td>
                                                    }
                                                </React.Fragment>
                                            )}
                                            <td>
                                                <i className="las la-minus la-2x"></i>
                                            </td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <AddUserModal
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
            />
        </div >
    )
}

UserTable.defaultProps = {
    headers: [
        { name: "Tizio", surname: "Caio", field: "Proprietario" },
        { name: "Tizi", surname: "ana", field: "Proprietario" }
    ],
    data: [
        {
            id: 1,
            luogo: "Camera123",
            nome: "Alby123",
            stato: "OK",
            luogoUrl: "block"
        },
        {
            id: 2,
            luogo: "Camera23",
            nome: "Alby23",
            stato: "OK"
        },
        {
            id: 3,
            luogo: "Camera231",
            nome: "Alby231",
            stato: "OK"
        },
        {
            id: 4,
            luogo: "Camera34",
            nome: "Alby34",
            stato: "OK"
        },
        {
            id: 5,
            luogo: "Camera45",
            nome: "Alby45",
            stato: "OK"
        }
    ]
}

UserTable.propTypes = {
    title: PropTypes.string,
    headers: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        surname: PropTypes.string.isRequired,
        field: PropTypes.string.isRequired
    })),
    data: PropTypes.array.isRequired
}

export default UserTable