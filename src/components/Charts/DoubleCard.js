import React from 'react'
import PropTypes from 'prop-types'

const DoubleCard = (props) => {
    const left = props.left;
    const right = props.right;
    return (
        <div className="card">
            <div className="card-content">
                <div className="card-body pt-0">
                    <div className="card-header">
                        <h4 className="card-title text-center">{props.title}</h4>
                    </div>
                    <div className="row">
                        <div className="col-md-6 col-12 border-right-blue-grey border-right-lighten-5 text-center">
                            <i className={`font-large-4 ${left.icon}`}></i>
                            <h4 className="font-large-2 text-bold-400">{left.percentage}</h4>
                            <p className="blue-grey lighten-2 mb-0">{left.name}</p>
                        </div>
                        <div className="col-md-6 col-12 text-center">
                            <i className={`font-large-4 ${right.icon}`}></i>
                            <h4 className="font-large-2 text-bold-400">{right.percentage}</h4>
                            <p className="blue-grey lighten-2 mb-0">{right.name}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

DoubleCard.propTypes = {
    title: PropTypes.string,
    left: PropTypes.shape({
        name: PropTypes.string,
        icon: PropTypes.string,
        percentage: PropTypes.string,
    })
}

export default DoubleCard