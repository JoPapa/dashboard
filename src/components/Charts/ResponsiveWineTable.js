import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Translation } from 'react-i18next'
import loader from '../../images/loader_75_fast.gif'
import '../../components/Lazy/LazyLine'
import LazyLine from '../../components/Lazy/LazyLine'
import './ResponsiveWineTable.css'
import WineElement from '../WineElement'
import WineListFilter from '../WineListFilter'

class ResponsiveTable extends Component {
    // Update to use react hooks
    state = {
        searchQuery: '',
        showFilters: false,
        filter: false,
        filters: ["red", "white", "rose", "bag", "bottle", "onSale", "pending", "confirmed", "notOnSale"],
        yearRange: {
            min: 2012,
            max: 2018
        }
    }

    updateSearchQuery = (searchQuery) => {
        this.setState({ searchQuery })
    }

    toggleFilter = () => {
        this.setState({ showFilters: !this.state.showFilters })
    }

    handleInputChange = (e) => {
        const value = e.target.checked;
        const name = e.target.name;
        this.setState({
            filter: true,
            filters: value
                ? [...this.state.filters, name]
                : this.state.filters.filter(el => el !== name)
        });
        // [name]: value
    }

    handleYearRangeChange = (values) => {
        this.setState({ ...this.setState, yearRange: { min: values[0], max: values[1] } })
    }

    render() {
        const { filters, yearRange } = this.state;

        let props = this.props;
        const filteredResults = props.data.filter(wine =>
            wine.wineName.toLowerCase().indexOf(this.state.searchQuery.toLowerCase()) > -1
            && filters.includes(wine.type)
        )

        const getYearDomainRange = () => {
            const vintages = filteredResults.map((current) => current.vintages.map(element => element))
                .reduce((acc, current) => acc.concat(current))
            const min = vintages.reduce((min, el) => (el.wineVintage < min ? min = el.wineVintage : min), vintages[0].wineVintage)
            const max = vintages.reduce((max, el) => (el.wineVintage > max ? max = el.wineVintage : max), vintages[0].wineVintage)
            return {
                "min": parseInt(min),
                "max": parseInt(max)
            }
        }

        const getWinesOnSale = () => {
            const res = filteredResults.length < 1 ? [] : filteredResults.map((current) => current.vintages)
                .reduce((acc, current) => acc.concat(current))
                .filter(el => el.onSale)
            return res.length
        }

        const getCurrentWines = () => filteredResults.length
        return (
            <Translation>
                {(t) =>

                    <div className="row">
                        <div className="col-12">
                            <div className="card">
                                {/* Card Header */}
                                <div className="card-header d-flex justify-content-between">
                                    <div>
                                        <h4 className="card-title">
                                            <strong>
                                                {props.title.toUpperCase()}
                                            </strong>
                                        </h4>
                                        <div className="font-small-4 mt-1">Hai un totale di {props.data.length} vini.</div>
                                        <div className="font-small-4">Stai visualizzando {getCurrentWines()} di cui {!props.isLoading && filteredResults && getWinesOnSale()} sono in vendita</div>
                                    </div>

                                    {/* Heading Elements */}
                                    <div className="d-flex align-items-center">
                                        {/* Inserisci qui gli elementi a destra del titolo */}
                                        {props.createWine ? (
                                            <Link to='/new-wine' className="btn btn-outline-danger btn-md btn-round mr-1">
                                                <span className="font-medium-3 font-weight-bold">+ </span> <span className="">{t('wine.create wine')}</span>
                                            </Link>) : null}
                                        {props.withSearch &&
                                            <fieldset className="position-relative has-icon-left">
                                                <input
                                                    type="text"
                                                    className="form-control round"
                                                    placeholder={`${t('settings.search')}...`}
                                                    value={this.state.searchQuery}
                                                    onChange={(e) => this.updateSearchQuery(e.target.value)}
                                                />
                                                <div className="form-control-position">
                                                    <i className="la la-search"></i>
                                                </div>
                                            </fieldset>
                                        }
                                        <button className="ml-1" onClick={this.toggleFilter}>
                                            <i className="las la-filter la-2x"></i>
                                            <div>{t('settings.filter')}</div>
                                        </button>
                                    </div>
                                    {!props.isLoading &&
                                        <WineListFilter
                                            open={this.state.showFilters}
                                            onClose={this.toggleFilter}
                                            filters={this.state.filters}
                                            onInputChange={this.handleInputChange}
                                            domainRange={filteredResults.length > 0 && getYearDomainRange()}
                                            yearRange={yearRange}
                                            onRangeChange={filteredResults.length > 0 && this.handleYearRangeChange}
                                        />
                                    }
                                </div>

                                {/* Card content, AKA 'Table' */}
                                <div className="card-content">
                                    {filteredResults
                                        ? props.isLoading
                                            ? <>
                                                <LazyWineElement />
                                                <LazyWineElement />
                                            </>
                                            : filteredResults.map(wine => (
                                                <WineElement
                                                    key={wine.id}
                                                    wine={wine}
                                                    filters={filters}
                                                    headers={props.headers}
                                                    yearRange={this.state.yearRange}
                                                    isCollapsed={filteredResults.length < 10 ? true : false}
                                                />
                                            ))
                                        : 'There are no wines on your account'}
                                </div>
                            </div>
                        </div>
                    </div >
                }
            </Translation>
        )
    }
}

// TODO: Remove defaultProps, they can just be empty ==> []
ResponsiveTable.defaultProps = {
    headers: [],
    data: []
}

ResponsiveTable.propTypes = {
    title: PropTypes.string,
    headers: PropTypes.arrayOf(PropTypes.shape({
        title: PropTypes.string.isRequired,
        field: PropTypes.string.isRequired
    })),
    data: PropTypes.array.isRequired
}

export default ResponsiveTable


const LazyWineElement = () => {
    return (
        <>
            <div className="pl-3 py-2">
                <LazyLine width="350px" />
            </div>
            <div className="pl-3 pt-2 d-flex align-items-center">
                <div className="pr-3">
                    <LazyLine width="70px" />
                </div>
                <img src={loader} alt="Loading glass.." style={{
                    "width": "75px",
                    "height": "75px",
                }}></img>
                <div className="px-3">
                    <LazyLine width="120px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="80px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="240px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="140px" />
                </div>
            </div>
            <div className="pl-3 py-2 d-flex align-items-center">
                <div className="pr-3">
                    <LazyLine width="70px" />
                </div>
                <img src={loader} alt="Loading glass.." style={{
                    "width": "75px",
                    "height": "75px",
                }}></img>
                <div className="px-3">
                    <LazyLine width="120px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="80px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="240px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="140px" />
                </div>
            </div>
        </>
    )
}