import React from 'react'
import PropTypes from 'prop-types'

const TopFive = (props) => {
    //  Questo component ha bisogno di un array ed elementi con un ID, name e percentuale
    const rainbow = ['success', 'primary', 'info', 'warning', 'danger']

    // TODO: implementa funzione che porti il primo elemento a 70 e metta tutti gli altri
    // in proporzione ad esso. 
    // Es. Primo = 20%, Secondo = 10%, terzo = 5%
    //     Nel grafico si vedra' PRIMO = 70%, SECONDO = [10 * (70 / 20)]% = 35%
    //     TERZO = [5 * (70 / 20)]% = 17.5%

    // TODO: potrebbero esserci errori dato che la visualizzazione delle percentuali
    // è diversa dalla realtà

    // // Calculate the percentage based on the ratio between previous number
    // // and current one. This is needed to always have first item to be 70%
    // const calculateOnRatio = (previousNumber, currentNumber) => {
    //     // If the previousNumber is not defined then 
    //     // const prevNumber = previousNumber < 0 ? 70 : previousNumber
    //     // Ratio between the previous number percentage and the current one
    //     const ratio = previousNumber / currentNumber
    //     // Divide it by 
    //     return 70 / ratio
    // }
    return (
        <div className="card">
            <div className="card-header pb-1">
                <h4 className="card-title text-center">{props.title}</h4>
            </div>
            <div className="card-body text-center pt-1 pb-3">
                {props.data.slice(0, props.top).map((el, index) => (
                    <div key={index} className="row align-items-center mb-1">
                        <div className="col font-medium-3 font-weight-bold p-0">
                            {index + 1}.
                            <i className="success las la-arrow-up" style={{ "paddingLeft": "5px" }}></i>
                        </div>
                        <div className="col-8">
                            <p className="text-left m-0 h5">
                                {el.name}
                            </p>
                            <div className="progress progress-md mt-1 mb-0">
                                <div
                                    className={`progress-bar bg-${rainbow[index]}`}
                                    role="progressbar"
                                    style={{ "width": `${parseInt(el.percentage) + 35}%` }}
                                    aria-valuenow={el.percentage}
                                    aria-valuemin="0"
                                    aria-valuemax="100">
                                </div>
                            </div>
                        </div>
                        <div className="col p-0">
                            <div className={`${rainbow[index]} font-medium-1 font-weight-bold`} >{el.percentage}%</div>
                        </div>
                    </div>
                ))}
                {props.children}
            </div>
        </div>
    )
}

TopFive.defaultProps = {
    top: 5,
    data: []
}

TopFive.propTypes = {
    title: PropTypes.string,
    top: PropTypes.number,
    data: PropTypes.array
}

export default TopFive