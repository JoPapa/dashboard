import React from 'react'
import ReactEcharts from 'echarts-for-react'
import PropTypes from 'prop-types'

const RosePie = (props) => { 
    const theOption = {
        legend: {
            orient: 'horizontal',
            x : 'center',
            y : 'top',
            data: props.data
        },
        tooltip: {
            trigger: 'item',
            formatter: '{d}%'
        },
        calculable : true,
        series : [
            {
                name:'Fasce Orarie',
                type:'pie',
                radius : [10, 110],
                center : ['50%', '60%'],
                roseType : 'radius',
                itemStyle : {
                    normal : {
                        label : {
                            show : false
                        },
                        labelLine : {
                            show : false
                        }
                    },
                    emphasis : {
                        label : {
                            show : true
                        },
                        labelLine : {
                            show : true
                        }
                    }
                },
                data: props.data
            }
        ]
    };
                                       
    return (
        <ReactEcharts option={theOption} />
    )
}

RosePie.defaultProps = {
    data: [
        {value:13, name:'24:00 - 4:00'},
        {value:14, name:'4:00 - 8:00'},
        {value:15, name:'8:00 - 12:00'},
        {value:18, name:'12:00 - 16:00'},
        {value:20, name:'16:00 - 20:00'},
        {value:16, name:'20:00 - 24:00'}
    ]
}

RosePie.propTypes = {
    data: PropTypes.array    
}

export default RosePie