import React from 'react'
import { useTranslation } from 'react-i18next'
import Dispenser from '../images/dispenser-icon.png'
import Label from '../images/albi-wine-label.jpg'
import WineLevel from './WineLevel'

const DispenserInfo = ({ dispenser, checked, onCheck }) => {
    const { t } = useTranslation()
    const { wineBag } = dispenser
    const checkWine = wineBag.constructor === Object && Object.entries(wineBag).length !== 0
    const batteryIcon = {
        0: "empty",
        25: "quarter",
        50: "half",
        75: "three-quarters",
        100: "full"
    }
    return (
        <div onClick={onCheck ? onCheck : null} className={`card ${checked && "border border-info"}`} style={{ "cursor": "pointer", "width": "100%", "margin": "10px 5px", "userSelect": "none" }}>
            <div className="row m-1 mt-2">
                {/* Checkbox */}
                {checked &&
                    <fieldset>
                        <input type="checkbox" checked={checked} onChange={onCheck} style={{ "position": "absolute", "right": "15px", "zIndex": "1" }} />
                    </fieldset>
                }
                {/* Left side */}
                <div className="col-4 px-3">
                    <div className="d-flex justify-content-center align-items-center">
                        <div className="text-left">
                            <img className="img-fluid" src={checkWine ? Label : Dispenser} width={"125px"} height={"175px"} alt="Dispenser Icon" />
                        </div>
                    </div>
                </div>
                {/* Center */}
                <div className="col-8 pl-1">
                    <div>
                        <div className="font-medium-5">{dispenser.dispenserName}</div>
                        <div>
                            <div className="">
                                <i className="la la-wifi"></i>
                                <span className=""> {dispenser.wifiName}</span>
                            </div>
                            <div>{t('room.dispenser.mac')} {dispenser.macAddress}</div>
                            <div>{t('room.dispenser.serial')} {dispenser.serial}</div>
                        </div>
                    </div>
                    <hr />
                    <div className="border-top-1">
                        {checkWine && <div className="font-medium-2">{wineBag.wineName}</div>}
                        <div className="row">
                            <div className="col-12 col-md-6 align-items-middle">
                                {!checkWine
                                    ? <div className="text-center my-1 pt-1">{t('room.dispenser.no wine inside')}</div>
                                    : <>
                                        <div className="font-medium-1 font-weight-bold">{wineBag.wineryName}</div>
                                        <WineLevel
                                            totalGlasses={wineBag.bagCapacity}
                                            glassesLeft={wineBag.quantityLeft}
                                        />
                                    </>
                                }
                            </div>
                            <div className="col-12 col-md-6 font-medium-1 text-right">
                                <div className="font-weight-bold">{t('room.last service')}</div>
                                <div>{dispenser.lastService}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="card-footer d-flex justify-content-between align-middle mt-1">
                {<div className="text-center ml-1">
                    <i className={`las la-wifi la-2x ${dispenser.status.toLowerCase() === "online" ? "success" : "danger"}`}></i>
                    <div>{dispenser.status.toLowerCase() === "online" ? t('room.dispenser.connected') : t('room.dispenser.disconnected')}</div>
                </div>}
                {dispenser.batteryStatus.toLowerCase() === "charging"
                    ? <div className="text-center ml-1">
                        <i className="las la-plug la-2x info"></i>
                        <div>{t('room.dispenser.charging')}</div>
                    </div>
                    : <div className="text-center ml-1">
                        <i className={`las la-battery-${batteryIcon[dispenser.batteryStatus]} la-2x info`}></i>
                        <div>{t('room.dispenser.battery at')} {dispenser.batteryStatus}%</div>
                    </div>
                }
                <div className="text-center ml-1">
                    <div className="font-medium-1 font-weight-bold">{checkWine ? dispenser.wineTemperature + "°C" : "N/A"}</div>
                    <div className="">{t('room.dispenser.wine temperature')}</div>
                </div>
                <div className="text-center ml-1">
                    <div className="font-medium-1 font-weight-bold">{dispenser.roomTemperature}°C</div>
                    <div className="">{t('room.dispenser.room temperature')}</div>
                </div>
            </div>
        </div>
    )
}

export default DispenserInfo