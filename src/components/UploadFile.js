import React from 'react'
import './UploadFile.css'

class UploadFile extends React.Component {
    state = {
        uploadedFiles: [],
        dragging: false
    }
    dropboxRef = React.createRef();

    selectFile = (e) => {
        const fileElem = document.getElementById("fileElem");
        if (fileElem) {
            fileElem.click();
        }
    }

    handleFiles = (files) => {
        for (let i = 0; i < files.length; i++) {
            const file = files[i];
            console.log("File", file.current)
            if (!file.type.startsWith('image/')) { continue }
            // Add to state each image file
            this.setState({
                uploadedFiles: [...this.state.uploadedFiles, {
                    name: file.name,
                    size: file.size,
                    current: file,
                    url: URL.createObjectURL(file)
                }]
            })
            this.props.onUpload(this.state.uploadedFiles)
        }
    }

    // delete file from uploaded list
    onClear = (file) => {
        let array = [...this.state.uploadedFiles];
        let index = array.indexOf(this.state.uploadedFiles.find(el => el.url === file))
        if (index !== -1) {
            array.splice(index, 1);
        }
        console.log("Files", array)
        this.setState({ uploadedFiles: array })
    }

    handleDrag = (e) => {
        e.preventDefault()
        e.stopPropagation()
    }
    handleDragIn = (e) => {
        e.preventDefault()
        e.stopPropagation()
        if (e.dataTransfer.items && e.dataTransfer.items.length > 0) {
            this.setState({ dragging: true })
        }
    }
    handleDragOut = (e) => {
        // this.setState({ dragging: false })
        // e.preventDefault()
        // e.stopPropagation()
    }
    handleDrop = (e) => {
        e.preventDefault()
        e.stopPropagation()
        let dt = e.dataTransfer;
        let files = dt.files;
        this.handleFiles(files);
        this.setState({ dragging: false })
    }

    componentDidMount() {
        let dropbox = this.dropboxRef.current
        dropbox.addEventListener('dragenter', this.handleDragIn)
        dropbox.addEventListener('dragleave', this.handleDragOut)
        dropbox.addEventListener('dragover', this.handleDrag)
        dropbox.addEventListener('drop', this.handleDrop)
    }
    componentWillUnmount() {
        let dropbox = this.dropboxRef.current
        dropbox.removeEventListener('dragenter', this.handleDragIn)
        dropbox.removeEventListener('dragleave', this.handleDragOut)
        dropbox.removeEventListener('dragover', this.handleDrag)
        dropbox.removeEventListener('drop', this.handleDrop)
    }

    render() {
        const { placeholder, disabled } = this.props
        return (
            <div className="text-center">
                {/* Drop Container */}
                <div ref={this.dropboxRef} id="dropBox" className={`border-1 mx-auto text-center ${this.state.dragging && 'dropbox-active'}`} onClick={(e) => this.selectFile(e)}>
                    <i className="las la-file-alt la-5x"></i>
                    <div className="text-small-3"> {!placeholder ? 'Drop your file here or browse it' : placeholder} </div>
                    <input
                        type="file"
                        id="fileElem"
                        accept="image/png, image/jpeg"
                        onChange={(e) => this.handleFiles(e.target.files)}
                        disabled={disabled}
                    />
                </div>
                {/* Listing uploaded files */}
                <div className="uploaded-list">
                    {this.state.uploadedFiles.map((file) =>
                        <div key={file.url} className="uploaded-item">
                            <img src={file.url} alt={'name'} className="item-thumbnail" />
                            <div className="item-description font-small-3">
                                <div>{file.name}</div>
                                <strong>Size</strong>
                                <span> {file.size} bytes</span>
                            </div>
                            <div className="item-actions">
                                <button
                                    className="float-right font-medium-4"
                                    onClick={() => this.onClear(file.url)}
                                >
                                    <i className="las la-times"></i>
                                </button>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

export default UploadFile