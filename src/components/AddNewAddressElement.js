import React, { useState } from 'react'
import { useTranslation } from 'react-i18next';
import Modal from 'react-modal'

const modalStyle = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        border: '0',
        borderRadius: '15px',
        boxShadow: '0px 0px 20px 0px #c5c5c5',
        marginRight: '-50%',
        padding: '50px',
        transform: 'translate(-50%, -50%)',
        width: '500px'
    }
}

const EditModal = ({ isOpen, onRequestClose, title, onSave }) => {
    const { t } = useTranslation();
    const [newAddress, setNewAddress] = useState({
        addressName: "",
        address: "",
        city: "",
        code: "",
        region: "",
        country: ""
    });

    const saveAddress = () => {
        onSave(newAddress)
        setNewAddress({})
        onRequestClose()
    }


    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            style={modalStyle}
            contentLabel=""
        >
            <div className="text-center">
                <button
                    type="button"
                    onClick={onRequestClose}
                    className="close"
                    aria-label="Close"
                    style={{
                        position: 'absolute',
                        top: '20px',
                        right: '25px',
                    }}>
                    <i className="la la-times"></i>
                </button>
                <h3>{title}</h3>
                <form>
                    <p className="font-medium-2 mt-1">{t('admin.add new address')}</p>
                    {/* TODO: Aggiungi possibilità di scegliere icona insieme all'indirizzo */}
                    <div className="p-1 m-1">
                        {/* FORM */}
                        <fieldset className="form-group position-relative">
                            <input type="text" className="form-control" placeholder={t('admin.address name')} id="addressName" value={newAddress.addressName} onChange={(e) => setNewAddress({ ...newAddress, addressName: e.target.value })} required />
                        </fieldset>
                        <fieldset className="form-group position-relative">
                            <input type="text" className="form-control" placeholder={t('admin.address')} id="address" value={newAddress.address} onChange={(e) => setNewAddress({ ...newAddress, address: e.target.value })} required />
                        </fieldset>
                        <fieldset className="form-group position-relative">
                            <input type="text" className="form-control" placeholder={t('admin.city')} id="city" value={newAddress.city} onChange={(e) => setNewAddress({ ...newAddress, city: e.target.value })} required />
                        </fieldset>
                        <fieldset className="form-group position-relative">
                            <input type="text" className="form-control" placeholder={t('admin.postal code')} id="code" value={newAddress.code} onChange={(e) => setNewAddress({ ...newAddress, code: e.target.value })} required />
                        </fieldset>
                        <fieldset className="form-group position-relative">
                            <input type="text" className="form-control" placeholder={t('admin.region')} id="region" value={newAddress.region} onChange={(e) => setNewAddress({ ...newAddress, region: e.target.value })} required />
                        </fieldset>
                        <fieldset className="form-group position-relative">
                            <input type="text" className="form-control" placeholder={t('admin.country')} id="country" value={newAddress.country} onChange={(e) => setNewAddress({ ...newAddress, country: e.target.value })} required />
                        </fieldset>
                    </div>
                    <div className="d-flex justify-content-between">
                        <button type="button" className="btn btn-outline-info round" onClick={onRequestClose}>
                            <span className="mx-1">{t('settings.cancel').toUpperCase()}</span>
                            <i className="las la-times"></i>
                        </button>
                        <button type="button" className="btn btn-outline-danger round" onClick={() => saveAddress()}>
                            <span className="mx-1">{t('settings.save').toUpperCase()}</span>
                            <i className="las la-arrow-right"></i>
                        </button>
                    </div>
                </form>
            </div>
        </Modal>
    )
}

const AddressElement = ({ title, content, iconName, onSave }) => {
    const { t } = useTranslation();
    const [isEditing, setEditing] = useState(false)

    return (
        <span className="rounded p-1 px-2 my-1" style={{ "cursor": "pointer", "minHeight": "125px" }}>
            {isEditing && <EditModal isOpen={isEditing} onRequestClose={() => setEditing(false)} title={title} content={content} onSave={onSave} />}
            <button type="button" className="btn btn-outline-info round" onClick={() => setEditing(!isEditing)}>
                <span className="mx-1">{t('admin.add new address')}</span>
                <i className="las la-plus font-weight-bold"></i>
            </button>
        </span>
    )
}

export default AddressElement