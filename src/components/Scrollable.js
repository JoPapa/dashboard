import React from 'react'
import './Scrollable.css'

const Scrollable = (props) => {
    const easeInOutQuad = function (t, b, c, d) {
        t /= d / 2;
        if (t < 1) return c / 2 * t * t + b;
        t--;
        return -c / 2 * (t * (t - 2) - 1) + b;
    };

    const scroll = (element, change, duration) => {
        let start = element.scrollLeft,
            currentTime = 0,
            increment = 20;

        console.log(start)

        let animateScroll = function () {
            currentTime += increment;
            let val = easeInOutQuad(currentTime, start, change, duration);
            element.scrollLeft = val;
            if (currentTime < duration) {
                setTimeout(animateScroll, increment);
            }
        };
        animateScroll();
    }
    return (
        <section>
            <button type="button" className="arrow arrow-prev" onClick={() => scroll(document.getElementById('content'), -400, 500)}>
                <i className="la la-angle-left"></i>
            </button>
            <div className="carousel-center" id="content">
                <div className="carousel-content">
                    {props.children}
                </div>
            </div>
            <button type="button" className="arrow arrow-next" onClick={() => scroll(document.getElementById('content'), 400, 500)}>
                <i className="la la-angle-right"></i>
            </button>
        </section>
    );
}

export default Scrollable