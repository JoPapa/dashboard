import React from 'react';
import { useTranslation } from 'react-i18next'
import Modal from 'react-modal'

const modalStyle = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        border: '0',
        borderRadius: '15px',
        boxShadow: '0px 0px 20px 0px #c5c5c5',
        marginRight: '-50%',
        padding: '50px',
        maxWidth: '600px',
        transform: 'translate(-50%, -50%)'
    }
}

const ConfirmModal = ({ isOpen, onRequestClose, onSave, message }) => {
    const { t } = useTranslation()

    const onClose = () => onRequestClose()

    const onConfirm = () => {
        onSave()
        onRequestClose()
    }

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onClose}
            style={modalStyle}
            contentLabel="Become a PRO user"
        >
            <div className="text-center">
                <button
                    type="button"
                    onClick={onClose}
                    className="close"
                    aria-label="Close"
                    style={{
                        position: 'absolute',
                        top: '20px',
                        right: '25px',
                    }}>
                    <i className="la la-times"></i>
                </button>
                <h3>{t('settings.are you sure')}</h3>
                <p className="font-medium-3 p-2">{message}</p>
                <div className="d-flex justify-content-around">
                    <button type="button" className="btn btn-outline-info round" onClick={() => onClose()}>
                        <span className="text-uppercase mx-1">{t('settings.cancel')}</span>
                        <i className="las la-times"></i>
                    </button>
                    <button type="button" className="btn btn-outline-danger round ml-1" onClick={() => onConfirm()}>
                        <span className="text-uppercase mx-1">{t('settings.confirm')}</span>
                        <i className="las la-check"></i>
                    </button>
                </div>
            </div>
        </Modal >
    )
}

export default ConfirmModal