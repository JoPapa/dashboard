import React from 'react'
import PropTypes from 'prop-types'
import { Slider, Handles } from 'react-compound-slider'

// TODO: Work on this component

const sliderStyle = {
    position: 'relative',
    width: '100%',
    height: 40,
    marginBottom: 10,
}

const railStyle = {
    position: 'absolute',
    width: '100%',
    height: 10,
    marginTop: 35,
    borderRadius: 5,
    backgroundColor: '#8B9CB6',
}

const RangeSlider = ({ domainRange, yearRange, onChange }) => {
    function Handle({
        handle: { id, value, percent },
        getHandleProps
    }) {
        return (
            <div
                style={{
                    left: `${percent}%`,
                    position: 'absolute',
                    marginLeft: -15,
                    marginTop: 30,
                    zIndex: 2,
                    width: 20,
                    height: 20,
                    border: 0,
                    textAlign: 'center',
                    cursor: 'pointer',
                    borderRadius: '50%',
                    backgroundColor: '#2C4870',
                    color: '#333',
                }}
                {...getHandleProps(id)}
            >
                <div style={{ fontFamily: 'Lato', fontSize: 12, marginTop: -20, fontWeight: "bold", minWidth: 30, marginLeft: -5}}>
                    {value}
                </div>
            </div>
        )
    }

    return (
        <Slider
            rootStyle={sliderStyle}
            domain={[domainRange.min || 0, domainRange.max || 1]}
            step={1}
            mode={2}
            values={[yearRange.min || 0, yearRange.max || 1]}
            onChange={onChange ? onChange : () => null}
        >
            <div style={railStyle} />
            <Handles>
                {({ handles, getHandleProps }) => (
                    <div className="slider-handles">
                        {handles.map(handle => (
                            <Handle
                                key={handle.id}
                                handle={handle}
                                getHandleProps={getHandleProps}
                            />
                        ))}
                    </div>
                )}
            </Handles>
        </Slider>
    )
}

RangeSlider.propTypes = {
    domainRange: PropTypes.shape({
        min: PropTypes.number || PropTypes.string,
        max: PropTypes.number || PropTypes.string
    }),
    yearRange: PropTypes.shape({
        min: PropTypes.number || PropTypes.string,
        max: PropTypes.number || PropTypes.string
    }),
}

export default RangeSlider