import React from 'react'
import PropTypes from 'prop-types'

const DataPicker = (props) => {
    return (
        <>
            { props.notInline ? (
                <div class={"form-inline" + props.noFloat ? "" : "float-right" }>
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text">dal</span>
                        </div>
                        <input type="date" className="form-control" placeholder="" max="3000-12-31" min="1000-01-01"/>
                    </div>
                    <div className="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">al</span>
                        </div>
                        <input type="date" className="form-control" placeholder="" max="3000-12-31" min="1000-01-01"/>
                    </div>
                </div>
            ) : (
                <div class={"form-inline" + props.noFloat ? "" : "float-right" }>
                    <div class="input-group m-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text">dal</span>
                        </div>
                        <input type="date" className="form-control" placeholder="" max="3000-12-31" min="1000-01-01"/>

                        <div class="input-group-prepend">
                            <span class="input-group-text">al</span>
                        </div>
                        <input type="date" className="form-control" placeholder="" max="3000-12-31" min="1000-01-01"/>
                    </div>
                </div>
            )}
        </>
// Old form
//
//
///////////////////////////////////////////////////////////////////////////////////
        // {/* // <div class="form-group">
        // //     <label>Predefined Ranges</label>
        // //     <div class='input-group'>
        // //         <input type='text' class="form-control dateranges" />
        // //         <div class="input-group-append">
        // //             <span class="input-group-text">
        // //                 <span class="la la-calendar"></span>
        // //             </span>
        // //         </div>
        // //     </div>
        // // </div> */}
    )
}

DataPicker.propTypes = {
    data: PropTypes.array
}

export default DataPicker