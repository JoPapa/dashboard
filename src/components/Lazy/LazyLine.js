import React from 'react'
import './Lazy.css'

const LazyLine = (props) => {
    return (
        <div className="placeholder pulse">
            <div className="line" style={{"width": props.width}}></div>
        </div>
    )
}

export default LazyLine