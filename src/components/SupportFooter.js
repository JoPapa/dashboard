import React from 'react'
import './SupportFooter.css'

const SupportFooter = () => {
    return (
        <div className="support-footer">
            <div className="block talk-to-expert">
                <span className="icons">
                    <i className="las la-user-astronaut"></i>
                </span>
                <div className="support-content">
                    <div className="title">Concierge</div>
                    <div className="desc">Ottieni l'aiuto degli esperti da un consulente aziendale personale per conoscere come far crescere la propria attività con Albicchiere. Non preoccuparti, non si tratterà di una chiamata di vendita.</div>
                </div>
            </div>
            <div className="block customer-support">
                <span className="icons">
                    <i className="las la-question-circle"></i>
                </span>
                <div className="support-content">
                    <div className="title">Supporto clienti</div>
                    <div className="desc">Inviaci un’e-mail a
                <a href="mailto:info@albicchiere.com" rel="noopener"> info@albicchiere.com </a>
                per richiedere assistenza tecnica o funzionale.</div>
                </div>
            </div>
        </div>
    )
}

export default SupportFooter