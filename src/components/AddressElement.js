import React, { useState } from 'react'
import { useTranslation } from 'react-i18next';
import Modal from 'react-modal'

const modalStyle = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        border: '0',
        maxHeight: '65vh',
        maxWidth: '65vw',
        borderRadius: '15px',
        boxShadow: '0px 0px 20px 0px #c5c5c5',
        padding: '50px',
        transform: 'translate(-50%, -50%)'
    }
}

const EditModal = ({ isOpen, onRequestClose, title, content, onSave }) => {
    const { t } = useTranslation();
    const [newAddress, setNewAddress] = useState({
        id: content.id,
        addressName: content.addressName,
        address: content.address,
        city: content.city,
        code: content.code,
        region: content.region,
        country: content.country
    });

    const saveAddress = () => {
        onSave(newAddress)
        setNewAddress({})
        onRequestClose()
    }

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            style={modalStyle}
            contentLabel=""
        >
            <div className="text-center">
                <button
                    type="button"
                    onClick={onRequestClose}
                    className="close"
                    aria-label="Close"
                    style={{
                        position: 'absolute',
                        top: '20px',
                        right: '25px',
                    }}>
                    <i className="la la-times"></i>
                </button>
                <h3>{title}</h3>
                {/* <div className="h2">Nome</div> */}
                <form>
                    <p className="font-medium-2 mt-1">Scegli uno degli indirizzi salvati oppure modifica quello esistente</p>
                    <div className="row my-1">
                        <div className="col border border-light rounded p-1 m-1">
                            <div className="px-3 mb-1">
                                <h4>{content.addressName}</h4>
                                <div>{content.address}</div>
                                <div>{content.city} {content.code}, {content.region}</div>
                                <div>{content.country}</div>
                            </div>
                            {/* FORM */}
                            <fieldset className="form-group position-relative">
                                <input type="text" className="form-control" placeholder={t('admin.address name')} id="addressName" value={newAddress.addressName} onChange={(e) => setNewAddress({ ...newAddress, addressName: e.target.value })} required />
                            </fieldset>
                            <fieldset className="form-group position-relative">
                                <input type="text" className="form-control" placeholder={t('admin.address')} id="address" value={newAddress.address} onChange={(e) => setNewAddress({ ...newAddress, address: e.target.value })} required />
                            </fieldset>
                            <fieldset className="form-group position-relative">
                                <input type="text" className="form-control" placeholder={t('admin.city')} id="city" value={newAddress.city} onChange={(e) => setNewAddress({ ...newAddress, city: e.target.value })} required />
                            </fieldset>
                            <fieldset className="form-group position-relative">
                                <input type="text" className="form-control" placeholder={t('admin.postal code')} id="code" value={newAddress.code} onChange={(e) => setNewAddress({ ...newAddress, code: e.target.value })} required />
                            </fieldset>
                            <fieldset className="form-group position-relative">
                                <input type="text" className="form-control" placeholder={t('admin.region')} id="region" value={newAddress.region} onChange={(e) => setNewAddress({ ...newAddress, region: e.target.value })} required />
                            </fieldset>
                            <fieldset className="form-group position-relative">
                                <input type="text" className="form-control" placeholder={t('admin.country')} id="country" value={newAddress.country} onChange={(e) => setNewAddress({ ...newAddress, country: e.target.value })} required />
                            </fieldset>
                        </div>
                    </div>
                    <div className="d-flex justify-content-between">
                        <button type="button" className="btn btn-outline-info round" onClick={onRequestClose}>
                            <span className="mx-1">{t('settings.cancel').toUpperCase()}</span>
                            <i className="las la-times"></i>
                        </button>
                        <button type="button" className="btn btn-outline-danger round" onClick={() => saveAddress()}>
                            <span className="mx-1">{t('settings.save').toUpperCase()}</span>
                            <i className="las la-arrow-right"></i>
                        </button>
                    </div>
                </form>
            </div>
        </Modal>
    )
}

const ChangeAddressModal = ({ isOpen, onRequestClose, title, content, onSave, addresses }) => {
    const { t } = useTranslation()
    const [selected, setSelected] = useState(content)

    const saveAddress = () => {
        onSave(selected)
        setSelected({})
        onRequestClose()
    }

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            style={modalStyle}
            contentLabel=""
        >
            <div className="text-center">
                <button
                    type="button"
                    onClick={onRequestClose}
                    className="close"
                    aria-label="Close"
                    style={{
                        position: 'absolute',
                        top: '20px',
                        right: '25px',
                    }}>
                    <i className="la la-times"></i>
                </button>
                <h3>{title}</h3>
                {/* <div className="h2">Nome</div> */}
                <form className="container">
                    <p className="font-medium-2 mt-1">{selected.addressName}</p>
                    <div className="row my-2" style={{ 'maxHeight': '350px', overflow: 'auto' }}>
                        {addresses.map(address =>
                            <div className="col-6 p-1">
                                <div
                                    className={`bg-white border border-${selected.id === address.id ? 'info border-3' : 'light'} rounded p-1`}
                                    style={{ 'user-select': 'none', cursor: 'pointer', minWidth: '200px' }}
                                    onClick={() => setSelected(address)}
                                >
                                    <div className="text-left">
                                        <h4>{address.addressName}</h4>
                                        <div>{address.address}</div>
                                        <div>{address.city} {address.code}, {address.region}</div>
                                        <div>{address.country}</div>
                                    </div>
                                    {/* FORM */}
                                </div>
                            </div>
                        )}
                    </div>
                    <div className="d-flex justify-content-between">
                        <button type="button" className="btn btn-outline-info round" onClick={onRequestClose}>
                            <span className="mx-1">{t('settings.cancel').toUpperCase()}</span>
                            <i className="las la-times"></i>
                        </button>
                        <button type="button" className="btn btn-outline-danger round" onClick={() => saveAddress()}>
                            <span className="mx-1">{t('settings.save').toUpperCase()}</span>
                            <i className="las la-arrow-right"></i>
                        </button>
                    </div>
                </form>
            </div>
        </Modal>
    )
}

const AddressElement = ({ title, content, iconName, onSave, onChange, addresses }) => {
    const [isEditing, setEditing] = useState(false)
    const [isChanging, setChanging] = useState(false)

    return (
        <div className="font-small-4">
            <div className="d-flex">
                <div className="font-weight-bold">{title}</div>
                {onChange &&
                    <div className="ml-1" onClick={() => setChanging(!isChanging)} style={{ "cursor": "pointer" }}>
                        <i className={`las la-pen la-1x`}></i>
                    </div>
                }
                {iconName && <i className={`la la-${iconName}`} style={{ "marginLeft": ".175rem" }}></i>}
            </div>
            <div className="d-flex">
                {isEditing &&
                    <EditModal
                        isOpen={isEditing}
                        onRequestClose={() => setEditing(false)}
                        title={title}
                        content={content}
                        onSave={(address) => onSave(address)}
                    />}
                {isChanging &&
                    <ChangeAddressModal
                        isOpen={isChanging}
                        onRequestClose={() => setChanging(false)}
                        title={title}
                        content={content}
                        addresses={addresses}
                        onSave={(address) => onChange(address)}
                    />}
                <div className="font-medium-1">
                    <div className="border border-light rounded p-1 px-2 my-1">
                        <div className="d-flex">
                            <h4>{content.addressName}</h4>
                            {onSave &&
                                <div className="ml-1" onClick={() => setEditing(!isEditing)} style={{ "cursor": "pointer" }}>
                                    <i className={`las la-pen la-1x`}></i>
                                </div>
                            }
                        </div>
                        <div className="" style={{ "width": "235px" }}>
                            <div>{content.address}</div>
                            <div>{content.city} {content.code}, {content.region}</div>
                            <div>{content.country}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default AddressElement