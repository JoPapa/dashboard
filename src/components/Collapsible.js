import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next';

const Collapsible = (props) => {
    const { t } = useTranslation();
    const [collapseTable, setCollapseTable] = useState(false)

    return (
        <React.Fragment>
            <div className="wine-title-header d-flex justify-content-between align-items-center">
                <div className="wine-title text-left" onClick={() => setCollapseTable(!collapseTable)}>
                    <i className={`la la-chevron-${collapseTable ? "down" : "right"}`}></i>
                    <strong className="h4 ml-1">{props.title}</strong>
                </div>
                {props.data && props.data.map((element, index) =>
                    <strong key={index} className="wine-title h4 ml-1">{element}</strong>
                )}
                {/* TODO: Do something about the link, this is not the solution. You should link only when requested, not when there is data shown */}
                {!props.data &&
                    <Link to="room" className="text-center text-dark"><i className="las la-arrow-right la-2x"></i><p>{t('settings.details')}</p></Link>
                }
            </div>
            {collapseTable && props.children}
        </React.Fragment>
    )
}

export default Collapsible