import React from 'react'
import { useTranslation } from 'react-i18next'
import PropTypes from 'prop-types'

const BlockchainScoreCard = (props) => {
    const { t } = useTranslation();
    // TODO: Calculate percentago based on total scores
    const colors = {
        0: "primary",
        1: "info",
        2: "success",
        3: "warning"
    }
    const tierTitles = {
        90: t('blockchain.scores.excellent'),
        80: t('blockchain.scores.great'),
        70: t('blockchain.scores.good'),
        60: t('blockchain.scores.acceptable')
    }
    const tierIcons = {
        90: "grin-stars",
        80: "grin-wink",
        70: "laugh",
        60: "smile"
    }
    return (
        <div className="card">
            <div className="card-header">
                <div className="card-title text-center">{t('blockchain.scores.quality score')}</div>
            </div>
            <div className="card-body pt-0">
                <div className="font-medium-2 mb-2 mt-1">
                    <div className="d-flex justify-content-around text-center my-4 pt-1">
                        {props.scores.map((tier, index) =>
                            <div key={index}>
                                <i className={`las la-${tierIcons[tier.tier]} la-3x ${colors[index]}`}></i>
                                <div className="font-weight-bold">{tierTitles[tier.tier]}</div>
                                <div className="font-large-1">{tier.wines}</div>
                            </div>
                        )}
                    </div>
                </div>
                <button className="btn btn-primary btn-lg btn-block">{t('blockchain.scores.download certificate')}</button>
                <button className="btn btn-secondary btn-lg btn-block">{t('blockchain.scores.go to blockchain')}</button>
            </div>
        </div>
    )
}

BlockchainScoreCard.defaultProps = {
    score: "90"
}

BlockchainScoreCard.propTypes = {
    score: PropTypes.string.isRequired
}

export default BlockchainScoreCard

// const TierScore = ({ title, percentage, colorStyle, value }) => {
//     return (
//         <div className="row align-items-center">
//             <div className="col p-0 text-right">
//                 <div className="font-medium-2 font-weight-bold">{title}</div>
//             </div>
//             <div className="col-8">
//                 <div className="progress progress-lg my-1">
//                     <div
//                         className={`progress-bar bg-${colorStyle ? colorStyle : "success"}`}
//                         role="progressbar"
//                         style={{ "width": `${percentage}%` }}
//                         aria-valuenow={percentage}
//                         aria-valuemin="0"
//                         aria-valuemax="100">
//                     </div>
//                 </div>
//             </div>
//             <div className="col p-0">
//                 <div className={`font-medium-2 font-weight-bold`} >{value}</div>
//             </div>
//         </div>
//     )
// }
