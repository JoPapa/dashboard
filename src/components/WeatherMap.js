import React, { useState, useEffect } from 'react'
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types'
import mapMarker from '../images/map-marker.png'
import MapGL, { Marker, GeolocateControl, NavigationControl } from '@urbica/react-map-gl'
import 'mapbox-gl/dist/mapbox-gl.css'

// TODO: Cambia con la nostra chiave
const MAPBOX_ACCESS_TOKEN = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA'

const markerStyle = {
    "backgroundSize": "cover",
    "width": "35px",
    "height": "35px",
    "marginBottom": "35px",
    "borderRadius": "50%",
    "cursor": "pointer"
}

const WeatherMap = (props) => {
    const { t } = useTranslation();
    const { latitude, longitude } = props
    const [viewport, setViewport] = useState({
        latitude: latitude,
        longitude: longitude,
        zoom: 11
    });

    useEffect(() => {
        setViewport({
            latitude: latitude,
            longitude: longitude,
            zoom: 11
        });
    }, [latitude, longitude]);

    return (
        <div className="card">
            <div className="card-header">
                <span className="card-title">{t('blockchain.last location')}: <strong>Corciano, PG</strong>
                    <span className="float-right">
                        <i className="la la-sun"></i>
                           <span>12°C - 53.6°F</span>
                          </span>
                </span>
            </div>
            <div className="card-content" style={{ "height": "275px" }}>
                {props.latitude && props.longitude
                    ? <MapGL
                        style={{ width: '100%', height: '100%' }}
                        mapStyle='mapbox://styles/mapbox/streets-v11'
                        accessToken={MAPBOX_ACCESS_TOKEN}
                        latitude={viewport.latitude}
                        longitude={viewport.longitude}
                        zoom={viewport.zoom}
                        // onViewportChange={setViewport}
                    >
                        <GeolocateControl position='top-right' />
                        <NavigationControl showCompass position='top-right' />
                        <Marker latitude={latitude} longitude={longitude}>
                            <img src={mapMarker} alt="map marker" style={markerStyle} />
                        </Marker>
                    </MapGL>
                    : <p className="text-center pt-5 h3">Map is loading...</p>}
            </div>
        </div>
    )
}

WeatherMap.defaultProps = {
    latitude: null,
    longitude: null
}

WeatherMap.propTypes = {
    latitude: PropTypes.number.isRequired,
    longitude: PropTypes.number.isRequired
}

export default WeatherMap