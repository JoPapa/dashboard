import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Modal from 'react-modal'
import logo from '../images/logo/logo-dark.png';
import PropTypes from 'prop-types'

const SubscribeModal = (props) => {
    const { t } = useTranslation();
    const { isOpen, onRequestClose, style } = props
    const [email, setEmail] = useState()
    const [isLoading, setIsLoading] = useState(false)

    const sendEmail = () => {
            setIsLoading(true)
    }
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            style={style}
            contentLabel="Add a new user to your organization"
        >
            <div className="text-center">
                <button
                    type="button"
                    onClick={onRequestClose}
                    className="close"
                    aria-label="Close"
                    style={{
                        position: 'absolute',
                        top: '20px',
                        right: '25px',
                    }}>
                    <i className="la la-times"></i>
                </button>
                <img className="brand-logo" alt="albicchiere's logo" src={logo} />
                <h3>{t('admin.new user.title')}</h3>
                {/* <div className="h2">Nome</div> */}
                <form>
                    <p className="font-medium-2 mt-1">{t('admin.new user.subtitle')}</p>
                    <fieldset className="form-group position-relative has-icon-left">
                        <input type="email" className="form-control" id="email" placeholder={t('admin.new user.placeholder')} value={email} onChange={setEmail} required />
                        <div className="form-control-position">
                            <i className="la la-user"></i>
                        </div>
                    </fieldset>
                    <button type="button" onClick={sendEmail} className="btn btn-outline-danger round">
                        <span className="mx-1">{t('admin.new user.button')}</span>
                        {isLoading ? <i className="las la-circle-notch spinner"></i> : <i className="las la-arrow-right"></i>}
                    </button>
                </form>
            </div>
        </Modal>
    )
}

SubscribeModal.defaultProps = {
    style: {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            border: '0',
            borderRadius: '15px',
            boxShadow: '0px 0px 20px 0px #c5c5c5',
            marginRight: '-50%',
            padding: '50px',
            transform: 'translate(-50%, -50%)'
        }
    }
}

SubscribeModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onRequestClose: PropTypes.func,
    style: PropTypes.object
}

export default SubscribeModal