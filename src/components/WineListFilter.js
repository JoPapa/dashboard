import React from 'react'
import RangeSlider from './RangeSlider'
import { useTranslation } from 'react-i18next';

const WineListFilter = ({ open, onClose, filters, onInputChange, yearRange, domainRange, onRangeChange }) => {
    const { t } = useTranslation();
    return (
        <div className="p-2" style={{
            "position": "fixed",
            "top": "5rem",
            "right": "0",
            "zIndex": "2",
            "width": "250px",
            "height": "calc(100% - 5rem)",
            "background": "white",
            "boxShadow": "1px 0 30px rgba(0, 0, 0, .1)",
            "display": open ? "block" : "none"
        }}
        >
            <button className="float-right font-medium-4" onClick={onClose}>
                <i className="las la-times"></i>
            </button>
            <div className="mb-1 mt-2">
                <div className="font-weight-bold font-medium-1">{t('wine.colors.colors')}</div>
                <div className="input-group">
                    <div className="form-check form-check-inline mt-1">
                        <input className="form-check-input" type="checkbox" name="red" id="red" value="red" checked={filters.includes("red")} onChange={onInputChange} />
                        <label className="form-check-label" htmlFor="red">{t('wine.colors.red')}</label>
                    </div>
                    <div className="form-check form-check-inline mt-1">
                        <input className="form-check-input" type="checkbox" name="white" id="white" value="white" checked={filters.includes("white")} onChange={onInputChange} />
                        <label className="form-check-label" htmlFor="white">{t('wine.colors.white')}</label>
                    </div>
                    <div className="form-check form-check-inline mt-1">
                        <input className="form-check-input" type="checkbox" name="rose" id="rose" value="rose" checked={filters.includes("rose")} onChange={onInputChange} />
                        <label className="form-check-label" htmlFor="rose">{t('wine.colors.rose')}</label>
                    </div>
                </div>
            </div>
            <div className="my-1">
                <div className="font-weight-bold font-medium-1">{t('wine.vintage')}</div>
                <div className="d-flex text-center mx-2">
                    <RangeSlider
                        yearRange={yearRange}
                        domainRange={domainRange}
                        onChange={onRangeChange}
                    />
                </div>
            </div>
            <div className="my-1">
                <div className="font-weight-bold font-medium-1">{t('wine.package type')}</div>
                <div className="form-check form-check-inline mt-1">
                    <input className="form-check-input" type="checkbox" name="bag" id="Bag" value="Bag" checked={filters.includes("bag")} onChange={onInputChange} />
                    <label className="form-check-label" htmlFor="Bag">{t('wine.bag')}</label>
                </div>
                <div className="form-check form-check-inline mt-1">
                    <input className="form-check-input" type="checkbox" name="bottle" id="Bottle" value="Bottle" checked={filters.includes("bottle")} onChange={onInputChange} />
                    <label className="form-check-label" htmlFor="Bottle">{t('wine.bottle')}</label>
                </div>
            </div>
            <div className="my-1">
                <div className="font-weight-bold font-medium-1">{t('wine.confirmation status')}</div>
                <div className="form-check form-check-inline mt-1">
                    <input className="form-check-input" type="checkbox" name="pending" id="pending" value="pending" checked={filters.includes("pending")} onChange={onInputChange} />
                    <label className="form-check-label" htmlFor="pending">{t('wine.pending')}</label>
                </div>
                <div className="form-check form-check-inline mt-1">
                    <input className="form-check-input" type="checkbox" name="confirmed" id="confirmed" value="confirmed" checked={filters.includes("confirmed")} onChange={onInputChange} />
                    <label className="form-check-label" htmlFor="confirmed">{t('wine.confirmed')}</label>
                </div>
            </div>
            <div className="my-1">
                <div className="font-weight-bold font-medium-1">{t('wine.sale status')}</div>
                <div className="form-check form-check-inline mt-1">
                    <input className="form-check-input" type="checkbox" name="onSale" id="onSale" value="onSale" checked={filters.includes("onSale")} onChange={onInputChange} />
                    <label className="form-check-label" htmlFor="onSale">{t('wine.on sale')}</label>
                </div>
                <div className="form-check form-check-inline mt-1">
                    <input className="form-check-input" type="checkbox" name="notOnSale" id="notOnSale" value="notOnSale" checked={filters.includes("notOnSale")} onChange={onInputChange} />
                    <label className="form-check-label" htmlFor="notOnSale">{t('wine.not on sale')}</label>
                </div>
            </div>
        </div>
    )
}

export default WineListFilter