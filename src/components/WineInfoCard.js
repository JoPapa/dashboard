import React from 'react'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next';
import PropTypes from 'prop-types'
import loader from '../images/loader_200.gif'
import ReactImageMagnify from 'react-image-magnify';

const WineInfoCard = (props) => {
    const { t } = useTranslation();
    const onChange = (e) => {
        e.preventDefault()
    }
    // TODO: delete log
    console.log("wins", props.wine)
    return (
        <div className="card">
            <div className="row no-gutters">
                {/* Image column */}
                <div className={"col-12 col-lg-4 px-2 mt-2 d-flex"}>
                    {props.isLoading
                        ? <img src={loader} width="75%" height="auto" alt="wine loader.." />
                        : <div className="row no-gutters">
                            <div className="col-9">
                                <ReactImageMagnify
                                    enlargedImagePosition={"over"}
                                    smallImage={{
                                        alt: 'Wristwatch by Ted Baker London',
                                        isFluidWidth: true,
                                        src: process.env.PUBLIC_URL + `/images/wines/${props.wine.id}.jpg`
                                    }}
                                    largeImage={{
                                        src: process.env.PUBLIC_URL + `/images/wines/${props.wine.id}.jpg`,
                                        width: 950,
                                        height: 900
                                    }}
                                />
                                {/* <img className="img-fluid" max-height="100%" width="auto" src={process.env.PUBLIC_URL + `/images/wines/${props.wine.id}.jpg`} alt="wine label" /> */}
                            </div>
                            <div className="col-3 mx-auto">
                                <img
                                    className="img-fluid"
                                    src={process.env.PUBLIC_URL + `/images/wines/${props.wine.id}.jpg`}
                                    width="auto"
                                    max-height="100%"
                                    alt="wine label" />
                                <i className="las la-tags la-5x py-2">
                                    <div className="h2 text-center" style={{ "height": "40%" }}>+ 2</div>
                                </i>
                            </div>
                        </div>
                    }
                </div>
                {/* Attribute column */}
                <div className="col-12 col-lg-8 px-2 my-2">
                    <div className="row">
                        <div className="col-10">
                            <h2>{props.wine.wine_name}
                            </h2>
                        </div>
                        <div className="col-2 h5 text-center"><Link to="new-wine">{t('settings.edit')}</Link></div>
                    </div>
                    <div className="row no-gutters">
                        {/* Wine attributes */}
                        <div className="col-12 col-lg-6 px-2 my-2">
                            <div className="wine-attribute">
                                <i className="las la-calendar la-2x mr-1"></i>
                                <label htmlFor="annata"><strong>{t('wine.vintage')}: </strong></label>
                                <select className="single-input border-0" id="annata" value={!props.isLoading ? props.wineYear : ''} onChange={e => onChange(e)}>
                                    {!props.isLoading ? props.wine.wines.map(year => <option key={year.year} value={year.year}>{year.year}</option>) : <option>{props.wineYear}</option>}
                                </select>
                            </div>
                            <div className="wine-attribute">
                                <i className="las la-stroopwafel la-2x mr-1"></i>
                                <strong>{t('wine.grapes')}: </strong> {props.wine.vitigno}
                            </div>
                            <div className="wine-attribute">
                                <i className="las la-tags la-2x mr-1"></i>
                                <strong>{t('wine.type')}: </strong> {props.wine.type}
                            </div>
                            <div className="wine-attribute">
                                <i className="las la-percent la-2x mr-1"></i>
                                <strong>{t('wine.alcohol')}: </strong> {props.wine.grado_alcolico}
                            </div>
                            <div className="wine-attribute">
                                <i className="las la-temperature-high la-2x mr-1"></i>
                                <strong>{t('wine.recommended temperature')}: </strong> {props.wine.temperatura_consigliata}
                            </div>
                            <div className="wine-attribute">
                                <i className="las la-seedling la-2x mr-1"></i>
                                <strong>{t('wine.aroma')}: </strong> {props.wine.piatti_suggeriti}
                            </div>
                            <div className="wine-attribute">
                                <i className="las la-dna la-2x mr-1"></i>
                                <strong>{t('wine.structure')}: </strong> {props.wine.mood}
                            </div>
                            <div className="wine-attribute">
                                <i className="las la-smile-wink la-2x mr-1"></i>
                                <strong>{t('wine.mood')}: </strong> {props.wine.mood}
                            </div>
                            <div className="wine-attribute">
                                <i className="las la-utensils la-2x mr-1"></i>
                                <strong>{t('wine.pairing')}: </strong> {props.wine.abbinamenti}
                            </div>
                        </div>
                        {/* Description */}
                        <div className="col-12 col-lg-6 px-2 my-2">
                            <div className="wine-attribute">
                                <i className="las la-user-edit la-2x"></i>
                                <strong>{t('wine.description')}:</strong>
                            </div>
                            {/* <div className="h6">Clicca il testo per modificare</div> */}
                            <textarea
                                rows={14}
                                className="rounded border-0 wine-attribute"
                                value={props.wine.descrizione}
                                style={{ "width": "90%", "height": "auto" }} disabled />
                        </div>
                    </div>
                </div>
            </div>
            {/* Card footer */}
            <div className="row no-gutters card-footer px-2">
                <div className="col-4 col-md-2 pl-3">
                    <i className="las la-award la-3x"></i>
                    <div>{t('wine.awards')}</div>
                </div>
                <div className="col-8 col-md-7 pt-2">{t('wine.no awards message')}</div>
                <div className="col-12 col-md-3 pt-2 pr-3 text-right h4">{t('wine.retail price')}: 20.00$</div>
            </div>
        </div>
    )
}

WineInfoCard.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    wine: PropTypes.object.isRequired,
    wineYear: PropTypes.string
}

export default WineInfoCard