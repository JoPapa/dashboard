import React, { useState } from 'react'
import Modal from 'react-modal'
import AddressElement from './AddressElement'

const modalStyle = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        border: '0',
        borderRadius: '15px',
        boxShadow: '0px 0px 20px 0px #c5c5c5',
        marginRight: '-50%',
        padding: '50px',
        transform: 'translate(-50%, -50%)',
        width: 'auto'
    }
}

const OrderDetailsModal = ({ isOpen, onRequestClose, title }) => {
    const [shippingAddress, setShippingAddress] = useState({
        addressName: "SEDE LEGALE",
        address: "Via delle cantine 123/A",
        city: "Corciano",
        code: "06073",
        region: "PG",
        country: "Italia"
    })
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            style={modalStyle}
            contentLabel=""
        >
            <div className="">
                <button
                    type="button"
                    onClick={onRequestClose}
                    className="close"
                    aria-label="Close"
                    style={{
                        position: 'absolute',
                        top: '20px',
                        right: '25px',
                    }}>
                    <i className="la la-times"></i>
                </button>
                <h3>{title}</h3>
                <p className="font-medium-2 mt-1">Dettagli dell'ordine</p>
                {/* TODO: Aggiungi possibilità di scegliere icona insieme all'indirizzo */}
                <div className="d-flex justify-content-around p-1 m-1">
                    <AddressElement
                        title="Indirizzo Fatturazione"
                        content={shippingAddress}
                        onSave={(address) => setShippingAddress(address)}
                    />
                    <div className="mx-3">
                        <div className="font-weight-bold">{"Metodo di pagamento"}</div>
                        <div className="d-flex align-items-center mt-1"><i className="lab la-cc-visa la-2x mr-1"></i> **** 5555</div>
                    </div>
                    <div className="mx-2">
                        <div className="font-weight-bold">{"Riepilogo ordine"}</div>
                        <div className="mt-1"><strong>Totale:</strong> 0,00€</div>
                    </div>
                </div>
            </div>
        </Modal>
    )
}

export default OrderDetailsModal