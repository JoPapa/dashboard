import React from 'react'
import { useTranslation } from 'react-i18next'

const AccountWarning = () => {
    const { t } = useTranslation()
    return (
        <div
            className="text-white bg-danger p-1"
            style={{ "userSelect": "none" }}>
            {t('settings.waiting for admin confirmation')}
            {/* {t('settings.verify your company')} */}
        </div>
    )
}

export default AccountWarning