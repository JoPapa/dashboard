import React from 'react'
import PropTypes from 'prop-types'

const Card = ({ title, description, ...props }) => {
    return (
        <div className="inline-flex flex-col items-start justify-start w-full my-3 bg-white shadow rounded">
            <div className="inline-flex space-x-6 items-start justify-center w-full p-2 border-b-2 border-gray-200">
                <div className="inline-flex flex-col space-y-1 items-start justify-start w-full pl-2">
                    <p className="text-sm text-gray-700 uppercase w-full">{title}</p>
                    <p className="text-sm font-light leading-snug w-full">{description}</p>
                </div>
                <div className="flex space-x-2 items-start justify-start p-1">
                    <div className="w-full h-full">
                        <div className="flex-1 h-full">
                            {/* <div className="flex-1 h-full bg-white border rounded-sm border-gray-500">
                                <p className="flex-1 h-full text-xs font-bold leading-tight text-center text-gray-500 uppercase">LEARN MORE</p>
                            </div> */}
                        </div>
                    </div>
                    <div className="w-full h-full">
                        <div className="flex-1 h-full">
                            {/* <div className="flex-1 h-full bg-white border rounded-sm border-gray-500">
                                <p className="flex-1 h-full text-xs font-bold leading-tight text-center text-gray-500 uppercase">LEARN MORE</p>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
            <div className="w-full p-4">
                {props.children}
            </div>
        </div>
    )
}

Card.defaultProps = {
    title: '',
    description: ''
}

Card.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
}

export default Card