import React, { useState, useRef } from 'react'
import PropTypes from 'prop-types'
import { useOutsideClick } from '../../utils/helpers'
import { useTranslation } from 'react-i18next';

const Notifications = ({ type, messages }) => {
    const { t } = useTranslation();
    const [show, setShow] = useState(false);
    const ref = useRef();
    useOutsideClick(ref, () => setShow(false));

    return (
        <li ref={ref} className={`dropdown dropdown-notification nav-item ${show && 'show'}`}>
            <a className="nav-link nav-link-label pb-0" href="##" data-toggle="dropdown" onClick={() => setShow(!show)}>
                <i className={`las la-${type === "notifications" ? 'bell' : type === "communications" && 'envelope'} la-2x avatar avatar-busy`}>
                    <i></i>
                </i>
                {/* <span className="badge badge-pill badge-default badge-danger badge-up badge-glow">{messages.length}</span> */}
            </a>
            {/* // TODO: Add counter and notifications from props */}
            <ul className={`dropdown-menu dropdown-menu-media dropdown-menu-right`}>
                <li className="dropdown-menu-header">
                    <h6 className="dropdown-header m-0">
                        <span className="grey darken-2">{type === "notifications" ? t('navbar.notifications') : type === "communications" && t('navbar.communications')}</span>
                    </h6>
                    <span className="notification-tag badge badge-default badge-danger float-right m-0">{messages.length} New</span>
                </li>
                {/* TODO: Add notifications through props with limit */}
                {/* TODO: Change scroll slider style on hover and on hover */}
                <li className="scrollable-container media-list w-100">
                    {messages.map((notification, i) =>
                        <a href="##" key={i}>
                            <div className="media">
                                <div className="media-left align-self-center">
                                    {/* <i className="la la-user icon-bg-circle bg-cyan"></i> */}
                                </div>
                                <div className="media-body">
                                    <h6 className="media-heading">{notification.title}</h6>
                                    <p className="notification-text font-small-3 text-muted">{notification.message}</p>
                                    {/* <small><time className="media-meta text-muted" dateTime="2015-06-11T18:29:20+08:00">30 minutes ago</time></small> */}
                                </div>
                            </div>
                        </a>
                    )}
                </li>
            </ul>
        </li >
    )
}

Notifications.defaultProps = {
    type: "notifications",
    messages: []
}

Notifications.propTypes = {
    type: PropTypes.string.isRequired,
    messages: PropTypes.array.isRequired
}

export default Notifications