import React from 'react'
import PropTypes from 'prop-types'
// import { Link } from 'react-router-dom'
// import { useAppContext } from "../../utils/contexts"
// components
import { CompanySwitcher } from './CompanySwitcher'
// import UserMenu from './UserMenu'
// import LanguageFlag from './LanguageFlag'
// import Notifications from './Notifications'
// assets
import LogoLarge from '../../images/logo/albicchiere-logo-white.svg';
import LogoSmall from '../../images/logo/Albi-Logo-Black-Cropped.png';
import userAvatar from '../../images/user-avatar.png';

// TODO: Remove commented code

// const notifications = [
//     {
//         "title": "You have a new order!",
//         "message": "Lorem ipsum dolor sit amet, consectetuer elit.",
//         "timestamp": "24565432653"
//     },
//     {
//         "title": "99% Server load",
//         "message": "Aliquam tincidunt mauris eu risus.",
//         "timestamp": "24565432653"
//     },
//     {
//         "title": "Warning notification",
//         "message": "Vestibulum auctor dapibus neque.",
//         "timestamp": "24565432653"
//     },
//     {
//         "title": "Complete the task",
//         "message": "",
//         "timestamp": "24565432653"
//     },
//     {
//         "title": "Generate monthly report",
//         "message": "",
//         "timestamp": "24565432653"
//     }
// ]

const Header = (props) => {
    // const { currentUser } = useAppContext();
    return (
        <header className="fixed h-14 top-0 flex w-full items-center justify-between py-1 px-4 bg-black">
            <CompanySwitcher company={{
                name: "Albicchiere Winery"
            }} />
            <img className="hidden md:inline-block w-48 h-full" src={LogoLarge} alt="albicchiere logo" />
            {/* TODO: Find an svg image for the small logo */}
            <img className="md:hidden w-12 h-12 object-cover rounded-full" src={LogoSmall} alt="albicchiere logo" />
            <div className="flex space-x-2 items-center justify-end">
                <div className="flex items-center justify-center w-6 h-6">
                    {/* <img className="w-8 h-7" src="https://via.placeholder.com/30x27" /> */}
                </div>
                <div className="max-w-36 h-full">
                    <div className="flex w-full space-x-4 items-center justify-center flex-1">
                        {/* Language */}
                        {/* {process.env.NODE_ENV === 'development' && <LanguageFlag />} */}
                        {/* Notification */}
                        {/* <Notifications
                            type="notifications"
                            messages={notifications}
                        /> */}
                        {/* User Info */}
                        {/* <UserMenu userAvatar={userAvatar} /> */}
                        <p className="flex-1 h-full text-sm text-right text-white">Hello, Mario</p>
                        <img className="w-1/5 h-full rounded-full" src={userAvatar} alt={"User profile"} />
                    </div>
                </div>
            </div>
        </header>
    )
}

Header.propTypes = {
    toggleMenu: PropTypes.func,
    userRole: PropTypes.oneOf(['all', 'winery', 'pro'])
}

Header.defaultProps = {
    toggleMenu: undefined,
    userRole: 'all'
};

export default Header
