import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'

import CompanyLogoPLaceHolder from '../../images/logo/Albi-Logo-Black-Cropped32.png'

// TODO: Remove commented code

// const DropdownItem = ({ ...props }) => {
//     return (
//         <>
//             <span role="img" aria-label="mogji">🤯 You can't see me!</span>
//         </>
//     )
// }

// const CompanySwitcherDropdown = ({ companies, onCompanySelection, ...rest }) => {
//     return (
//         <div className="d-flex flex-column">
//             {/* List the companies */}
//             {companies.map(company =>
//                 <CompanyItem
//                     key={company.id}
//                     company={company}
//                     onClick={() => onCompanySelection(company)}
//                     className="mt-1 ml-0 bg-secondary"
//                 />
//             )}
//             {/* Add a new company */}
//             {/* <DropdownItem /> */}
//         </div>
//     )
// }

const CompanyItem = ({ onClick, company, ...rest }) => {
    return (
        <button
            onClick={onClick}
            className="inline-flex space-x-2 items-center justify-center w-24 sm:w-64 px-2 py-1 bg-gray-900 hover:bg-gray-800 shadow rounded">
            {/* Company Logo */}
            <img className="w-8 h-full rounded-full" src={CompanyLogoPLaceHolder} alt={`Logo of company ${company.name}`} />
            {/* Company name */}
            <p className="hidden sm:inline-block w-3/4 h-full text-base leading-snug text-white truncate">{company.name}</p>
            <div className="w-4 h-4 rounded-full bg-gray-500"></div>
        </button>
    )
}

export const CompanySwitcher = ({ company, linkedCompanies, ...rest }) => {
    const [selectedCompany, setSelectedCompany] = useState({})
    const [dropdown, setDropdown] = useState(false);

    useEffect(() => {
        setSelectedCompany(company)
    }, [company])


    return (
        <>
            <CompanyItem onClick={() => setDropdown(!dropdown)} company={selectedCompany} />
            {/* {dropdown &&
                <CompanySwitcherDropdown
                    companies={linkedCompanies}
                    onCompanySelection={(company) => setSelectedCompany(company)}
                />} */}
        </>
    )
}

CompanySwitcher.propTypes = {
    company: PropTypes.shape({
        logo: PropTypes.string,
        name: PropTypes.string
    }),
    linkedCompanies: PropTypes.arrayOf(PropTypes.object)
}

CompanySwitcher.defaultProps = {
    company: {
        logo: 'https://www.w3schools.com/bootstrap/cinqueterre.jpg',
        name: 'Random Brand Name'
    },
    linkedCompanies: [
        {
            id: "00",
            logo: "https://www.w3schools.com/bootstrap/cinqueterre.jpg",
            name: "ALBICCHIERE WINERY"
        },
        {
            id: "01",
            logo: "https://www.w3schools.com/bootstrap/cinqueterre.jpg",
            name: "Cantina Albi Ltd"
        },
        {
            id: "02",
            logo: "https://www.w3schools.com/bootstrap/cinqueterre.jpg",
            name: "Albi Luxury Hotels Corporation"
        },
    ]
}
