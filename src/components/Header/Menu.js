import React from 'react'
import { NavLink } from 'react-router-dom'
// import SubscribeModal from '../SubscribeModal'
// Assets
import { useTranslation } from 'react-i18next';
// import Dispenser from '../../images/dispenser-icon.png'
// import './Menu.css'

export const MenuLink = (props) => {
    return (
        <>
            <NavLink
                to={props.path}
                activeClassName={"border-l-4 border-pink-700"}
                className="inline-flex items-start justify-start min-w-24 w-full h-20 group hover:bg-gray-200"
                disabled
                {...props}
            >
                <div className="inline-flex flex-col space-y-1.5 items-center justify-center flex-1 h-full">
                    {/* Icon */}
                    {/* {props.icon} */}
                    <div className="w-1/4 h-6">
                        <div className="flex-1 h-full bg-gray-200 group-hover:bg-white rounded-md" />
                    </div>

                    {/* Title */}
                    <p className="w-2/3 h-5 text-xs leading-tight text-center">{props.title}</p>
                </div>
            </NavLink>
        </>
    )
}

const Menu = (props) => {
    const { t } = useTranslation();
    // const userIsPaying = false;
    // const [modalIsOpen, setIsOpen] = React.useState(false);

    // function openModal() {
    //     setIsOpen(true);
    // }

    // function closeModal() {
    //     setIsOpen(false);
    // }
    return (
        <nav className="fixed top-14 bottom-0 w-24 flex flex-col justify-between bg-white shadow">
        <div>
          <MenuLink
            path="/wines"
            state=""
            icon="jio"
            title={t("menu.my wines")}
          />
          <MenuLink
            path="/404-1"
            state=""
            icon="jio"
            title="Analytics"
            disabled
          />
          <MenuLink
            path="/404-2"
            state=""
            icon="jio"
            title="Blockchain"
          />
          <MenuLink
            path="/404-3"
            state=""
            icon="jio"
            title="Dispenser"
          />
          <MenuLink
            path="/404-4"
            state=""
            icon="jio"
            title="Seller panel"
          />
        </div>
        <div>
          <MenuLink
            path="/404-5"
            state=""
            icon="jio"
            title="Shop"
          />
          <MenuLink
            path="/404-6"
            state=""
            icon="jio"
            title="Admin"
          />
        </div>
      </nav>
        // <div className="w-24">
        //     <div className="inline-flex flex-col space-y-9 items-center justify-start flex-1 pt-12 bg-white shadow">
        //         <div className="inline-flex items-start justify-start w-full">
        //             <div className="w-1 h-full bg-pink-700" />
        //             <div className="inline-flex flex-col space-y-0.5 items-center justify-center flex-1 h-full">
        //                 <div className="w-1/4 h-6">
        //                     <div className="flex-1 h-full bg-gray-200 rounded-md" />
        //                 </div>
        //                 <p className="w-2/3 h-5 text-xs leading-tight text-center">Analytics</p>
        //             </div>
        //         </div>
        //         <div className="relative" style={{ width: 100, height: 75, }}>
        //             <p className="w-16 h-5 absolute text-xs leading-tight text-center" style={{ left: 18, top: 37.50, }}>My Wines</p>
        //             <div className="w-6 h-6 absolute" style={{ left: 38, top: 12, }}>
        //                 <div className="flex-1 h-full bg-gray-200 rounded-md" />
        //             </div>
        //         </div>
        //         <div className="relative bg-gray-100" style={{ width: 100, height: 75, }}>
        //             <p className="w-16 h-5 absolute text-xs leading-tight text-center text-gray-500" style={{ left: 18, top: 37.50, }}>Analytics</p>
        //             <div className="w-6 h-6 absolute" style={{ left: 38, top: 12, }}>
        //                 <div className="flex-1 h-full bg-gray-200 rounded-md" />
        //             </div>
        //         </div>
        //         <img className="w-1/4 h-4" src="https://via.placeholder.com/25x15" />
        //         <div className="relative bg-gray-100" style={{ width: 100, height: 75, }}>
        //             <p className="w-16 h-5 absolute text-xs leading-tight text-center text-gray-500" style={{ left: 18, top: 37.50, }}>Blockchain</p>
        //             <div className="w-6 h-6 absolute" style={{ left: 38, top: 12, }}>
        //                 <div className="flex-1 h-full bg-gray-200 rounded-md" />
        //             </div>
        //         </div>
        //         <div className="w-20 h-0.5 border-gray-300" />
        //         <div className="relative bg-gray-100" style={{ width: 100, height: 75, }}>
        //             <p className="w-16 h-5 absolute text-xs leading-tight text-center text-gray-500" style={{ left: 18, top: 37.50, }}>Dispenser</p>
        //             <div className="w-6 h-6 absolute" style={{ left: 38, top: 12, }}>
        //                 <div className="flex-1 h-full bg-gray-200 rounded-md" />
        //             </div>
        //         </div>
        //         <div className="w-20 h-0.5 border-gray-300" />
        //         <div className="relative bg-gray-100" style={{ width: 100, height: 75, }}>
        //             <p className="w-16 h-5 absolute text-xs leading-tight text-center text-gray-500" style={{ left: 18, top: 37.50, }}>Seller panel</p>
        //             <div className="w-6 h-6 absolute" style={{ left: 38, top: 12, }}>
        //                 <div className="flex-1 h-full bg-gray-200 rounded-md" />
        //             </div>
        //         </div>
        //         <div className="relative" style={{ width: 100, height: 75, }}>
        //             <p className="w-16 h-5 absolute text-xs leading-tight text-center" style={{ left: 18, top: 37.50, }}>Shop</p>
        //             <div className="w-6 h-6 absolute" style={{ left: 38, top: 12, }}>
        //                 <div className="flex-1 h-full bg-gray-200 rounded-md" />
        //             </div>
        //         </div>
        //         <div className="relative" style={{ width: 100, height: 75, }}>
        //             <p className="w-16 h-5 absolute text-xs leading-tight text-center" style={{ left: 18, top: 37.50, }}>Admin</p>
        //             <div className="w-6 h-6 absolute" style={{ left: 38, top: 12, }}>
        //                 <div className="flex-1 h-full bg-gray-200 rounded-md" />
        //             </div>
        //         </div>
        //     </div>
        // </div>
        
        // <div className={`h-screen w-24`}>
        //     {/* List of page links */}
        //     <ul className="navigation navigation-main">
        //         {(props.userRole === "all" || props.userRole === "winery") &&
        //             <>
        //                 {/* Winery links */}
        //                 <MenuLink
        //                     path="/analytics"
        //                     state="active"
        //                     icon={<i className="las la-chart-bar"></i>}
        //                     title={t('menu.analytics')}
        //                 />
        //                 {!userIsPaying &&
        //                     <li className="nav-item">
        //                         {/* Change a to button */}
        //                         <a href="##" onClick={openModal}>
        //                             <i className="las la-chart-bar">
        //                                 <span className="badge badge-secondary font-small-2">PRO</span>
        //                             </i>
        //                             <span className="menu-title">{t('menu.analytics')}</span>
        //                         </a>
        //                     </li>}
        //                 <MenuLink
        //                     path="/my-wines"
        //                     state="active"
        //                     icon={<i className="las la-wine-glass-alt"></i>}
        //                     title={t('menu.my wines')}
        //                 />
        //                 <li className="nav-item">
        //                     {userIsPaying
        //                         ? <a href="##" onClick={openModal}>
        //                             <i className="la la-chain">
        //                                 <span className="badge badge-secondary font-small-2">PRO</span>
        //                             </i>
        //                             <span className="menu-title">{t('menu.blockchain')}</span>
        //                         </a>
        //                         : <MenuLink
        //                             path="/blockchain"
        //                             state="active"
        //                             icon={<i className="las la-chain"></i>}
        //                             title={t('menu.blockchain')}
        //                         />
        //                     }
        //                 </li>
        //             </>
        //         }
        //         <MenuLink
        //             path="/shop"
        //             state="active"
        //             icon={<i className="las la-shopping-cart"></i>}
        //             title={t('menu.shop')}
        //         />
        //         {(props.userRole === "all" || props.userRole === "pro") &&
        //             <>
        //                 <MenuLink
        //                     path="/homepro"
        //                     state="active"
        //                     icon={<i className="las la-desktop"></i>}
        //                     title={t('menu.dashboard')}
        //                 />
        //                 <MenuLink
        //                     path="/magazzino"
        //                     state="active"
        //                     icon={<i className="las la-cubes"></i>}
        //                     title={t('menu.stock')}
        //                 />
        //                 <MenuLink
        //                     path="/dispensers"
        //                     state="active"
        //                     icon={<i>
        //                         <img width="24" height="45" src={Dispenser} alt="Dispenser Icon" />
        //                     </i>}
        //                     title={"Dispenser"}
        //                 />
        //             </>
        //         }
        //         {/* All roles*/}
        //         {props.userRole !== '' &&
        //             <MenuLink
        //                 path="/admin"
        //                 state="active"
        //                 icon={<i className="las la-users"></i>}
        //                 title={t('menu.admin')}
        //             />
        //         }
        //     </ul>
        //     <SubscribeModal
        //         isOpen={modalIsOpen}
        //         onRequestClose={closeModal}
        //     />
        // </div >
    )
}

export default Menu