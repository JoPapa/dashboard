import React from 'react';
import Header from './Header'

export default {
  title: 'Components/Header',
  // component: Header,
};

const Template = (args) => <Header {...args} />;

export const Default = Template.bind({});
// Default.parameters = {
//   design: {
//     type: 'figma',
//     url: 'https://www.figma.com/file/cSfMt3LXntch6tgzzuCQua/Piattaforma?node-id=126%3A2357'
//   }
// }
Default.args = {
  // userRole: 'all'
};
