import React from 'react';

import { CompanySwitcher } from './CompanySwitcher'
import LogoBlack from '../../images/logo/Albi-Logo-Black-Cropped32.png'
import LogoWhite from '../../images/logo/Albi-Logo-White.png'

export default {
    title: 'Components/Header/CompanySwitcher',
    component: CompanySwitcher,
    argTypes: {
        currentCompany: {
            table: { disable: true }
        },
        logo: {
            description: 'Url of the company logo',
            options: ['Albi Black', 'Albi White'],
            control: { type: 'radio' },
            mapping: {
                'Albi Black': 'https://www.w3schools.com/bootstrap/cinqueterre.jpg',
                'Albi White': LogoWhite,
            }
        },
        name: {
            description: 'Name of the company'
        }
    }
}

const Template = (args) => <CompanySwitcher {...{
    currentCompany: {
        logo: args.logo,
        name: args.name
    }
}} />

export const Default = Template.bind({})
Default.parameters = {
    design: {
        type: 'figma',
        url: 'https://www.figma.com/file/cSfMt3LXntch6tgzzuCQua/Piattaforma?node-id=126%3A3187'
    }
}
Default.args = {
    logo: 'Albi Black',
    name: 'Cantina Albi Ltd'
}

export const LongCompanyName = Template.bind({})
LongCompanyName.parameters = {
    design: {
        type: 'figma',
        url: 'https://www.figma.com/file/cSfMt3LXntch6tgzzuCQua/Piattaforma?node-id=126%3A3187'
    }
}
LongCompanyName.args = {
    logo: 'Albi Black',
    name: 'Albi Luxury Hotels Corporation'
}