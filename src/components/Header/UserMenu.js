import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Translation } from 'react-i18next'
import { AppContext } from './../../utils/contexts'
import AlbiBlack from '../../images/logo/Albi-Logo-Black.png'
import AlbiWhite from '../../images/logo/Albi-Logo-White.png'

// Since this component "LanguageFlag" is a list element (<li/>) it should only be implemented in a list (<ul/> or <ol/>) NOTE: for the moment

class UserMenu extends Component {
    state = {
        show: false,
        showAccountSwitch: false
    }

    node = React.createRef();

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside, false);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside, false);
    }

    handleClickOutside = (e) => {
        if (this.node.contains(e.target)) {
            return
        }
        this.setState({ show: false });
    }

    showDropdown = () => {
        this.setState({ show: !this.state.show })
    }

    showAccountSwitch = () => {
        this.setState({ showAccountSwitch: !this.state.showAccountSwitch })
    }

    render() {
        return (
            <AppContext.Consumer>
                {value => (
                    <Translation>
                        {t =>
                            <li className={`dropdown dropdown-user nav-item ${this.state.show ? "show" : null}`} ref={node => this.node = node}>
                                <a className="dropdown-toggle nav-link dropdown-user-link pb-0" href="##" onClick={this.showDropdown}>
                                    <div className="d-flex">
                                        <div className="">
                                            <div className="mr-1 font-small-4">
                                                {value.currentCompany.companyName}
                                            </div>
                                            <div className="mr-1 text-right font-small-3">
                                                {t("welcome", { name: value.currentUser.firstName })}
                                            </div>
                                        </div>
                                        <div className="">
                                            <span className="avatar">
                                                <img src={this.props.userAvatar} alt="avatar" />
                                                <i className="las la-cog"></i>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right">
                                    <Link className="dropdown-item" to="/settings"><i className="la la-user-cog"></i> {t('navbar.account settings')}</Link>
                                    <div className="dropdown-divider"></div>
                                    <button className="dropdown-item" onClick={this.showAccountSwitch}>
                                        <i className="la la-sync-alt"></i> {t('navbar.switch account')}<i className={`las la-${this.state.showAccountSwitch ? 'angle-up' : 'angle-down'} ml-1`}></i></button>
                                    {this.state.showAccountSwitch &&
                                        value.currentUser.linkedCompanies.map((company, i) =>
                                            <button key={company.companyName + i} className="dropdown-item px-2" onClick={() => value.setCompanyById(company.id)}>
                                                <img className="img-thumbnail p-0 mr-1" src={i % 2 ? AlbiWhite : AlbiBlack} alt={company.companyName} width="25px" style={{ "borderRadius": "25px" }} />
                                                {company.companyName}
                                            </button>
                                        )
                                    }
                                    {/* <Link className="dropdown-item" to="/settings"><i className="la la-user-circle"></i> Profile Settings</Link> */}
                                    <div className="dropdown-divider"></div>
                                    <button className="dropdown-item" onClick={value.handleLogout}><i className="la la-power-off"></i> {t('navbar.logout')}</button>
                                </div>
                            </li>
                        }
                    </Translation>
                )}
            </AppContext.Consumer>
        )
    }
}

export default UserMenu