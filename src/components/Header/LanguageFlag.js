import React, { Component } from 'react'
import { Translation } from "react-i18next"

// Since this component "LanguageFlag" is a list element (<li/>) it should only be implemented in a list (<ul/> or <ol/>) NOTE: for the moment

class LanguageFlag extends Component {
    state = {
        show: false
    }

    node = React.createRef();

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside, false);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside, false);
    }

    handleClickOutside = (e) => {
        if (this.node.contains(e.target)) {
            return
        }
        this.setState({ show: false });
    }

    showDropdown = () => {
        this.setState({ show: !this.state.show })
    }

    render() {
        return (
            <Translation>
                {(t, { i18n }) =>
                    <li className={`dropdown dropdown-language nav-item ${this.state.show ? "show" : ""}`} ref={node => this.node = node}>
                        <a className="dropdown-toggle nav-link" id="dropdown-flag" href="##" onClick={this.showDropdown} aria-haspopup="true" aria-expanded="false">
                            <i className={`flag-icon flag-icon-${i18n.language.substring(0, 2) === 'en' ? 'gb' : i18n.language.substring(0, 2) }`}></i>
                            <span className="selected-language"></span>
                        </a>
                        <div className="dropdown-menu" aria-labelledby="dropdown-flag">
                            <button className="dropdown-item" onClick={() => i18n.changeLanguage('it')}><i className="flag-icon flag-icon-it"></i> Italiano</button>
                            <button className="dropdown-item" onClick={() => i18n.changeLanguage('en')}><i className="flag-icon flag-icon-gb"></i> English</button>
                            <button className="dropdown-item" onClick={() => i18n.changeLanguage('fr')}><i className="flag-icon flag-icon-fr"></i> Français</button>
                            <button className="dropdown-item" onClick={() => i18n.changeLanguage('es')}><i className="flag-icon flag-icon-es"></i> Español</button>
                        </div>
                    </li>
                }
            </Translation>
        )
    }
}

export default LanguageFlag