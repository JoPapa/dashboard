import React from 'react'
import PropTypes from 'prop-types'
import Dispenser from '../images/dispenser-icon.png'

const DispenserSummary = (props) => {
    return (
        <React.Fragment>
            <div className="d-flex justify-content-center align-items-center">
                <div className="text-left">
                    <img width={props.width} height={props.height} src={Dispenser} alt="Dispenser Icon" />
                </div>
                <div className="ml-2">
                    <strong className="h4">{props.title}</strong>
                    <div className="">
                        <i className="la la-wifi success"></i>
                        <span>{props.status}</span>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

DispenserSummary.defaultProps = {
    width: "55px",
    height: "95px",
    title: "Alby",
    status: "Connected"
}

DispenserSummary.propTypes = {
    width: PropTypes.string,
    height: PropTypes.string,
    title: PropTypes.string,
    status: PropTypes.string
}

export default DispenserSummary