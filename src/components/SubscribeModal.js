import React from 'react';
import Modal from 'react-modal'
import logo from '../images/logo/logo-dark.png';
import PropTypes from 'prop-types'

const SubscribeModal = (props) => {
    const { isOpen, onRequestClose, style } = props
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            style={style}
            contentLabel="Become a PRO user"
        >
            <div className="text-center">
                <button
                    type="button"
                    onClick={onRequestClose}
                    class="close"
                    aria-label="Close"
                    style={{
                        position: 'absolute',
                        top: '20px',
                        right: '25px',
                    }}>
                    <i className="la la-times"></i>
                </button>
                <img className="brand-logo" alt="albicchiere's logo" src={logo} />
                {/* <h3>Ti da il benvenuto</h3> */}
                <form>
                    <p className="font-medium-2 mt-1">Per accedere alla versione albicchiere <span className="badge badge-secondary font-small-2">PRO</span>
                        <br /> scegli il tipo di abbonamento adatto a te.</p>
                    <div className="row my-1">
                        <div className="col border border-light rounded p-1 m-1">
                            <h4>Opzione 1</h4>
                            <p>Descrizione testuale e vantaggi:</p>
                            <ul className="px-3">
                                <li>Vantaggio 1</li>
                                <li>Vantaggio 2</li>
                                <li>Vantaggio 3</li>
                            </ul>
                        </div>
                        <div className="col border border-light rounded p-1 m-1">
                            <h4>Opzione 2</h4>
                            <p>Descrizione testuale e vantaggi:</p>
                            <ul className="px-3">
                                <li>Vantaggio 1</li>
                                <li>Vantaggio 2</li>
                                <li>Vantaggio 3</li>
                            </ul>
                        </div>
                    </div>
                    <button type="button" class="btn btn-outline-danger round">
                        <span class="mx-1">INIZIA</span>
                        <i class="las la-arrow-right"></i>
                    </button>
                </form>
            </div>
        </Modal>
    )
}

SubscribeModal.defaultProps = {
    style: {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            border: '0',
            borderRadius: '15px',
            boxShadow: '0px 0px 20px 0px #c5c5c5',
            marginRight: '-50%',
            padding: '50px',
            transform: 'translate(-50%, -50%)'
        }
    }
}

SubscribeModal.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onRequestClose: PropTypes.func,
    style: PropTypes.object
}

export default SubscribeModal