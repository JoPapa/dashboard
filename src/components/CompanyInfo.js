import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next';
import AddressElement from './AddressElement'
import AddNewAddressElement from './AddNewAddressElement'
import CompanyLogo from '../images/logo/Albi-Logo-Black.png'
import UploadLogo from './UploadLogo'
import './CompanyInfo.css'
import { getDateFromTimestamp } from '../utils/helpers'

const CompanyElement = ({ title, content, iconName, onEdit }) => {
    const { t } = useTranslation();
    const [isEditing, setEditing] = useState(false)
    const [value, setValue] = useState(content)

    const onSave = () => {
        onEdit(value)
        setEditing(!isEditing)
    }

    return (
        <div className="font-small-4 mt-1">
            <div className="d-flex">
                <div className="font-weight-bold">{title}</div>
                {iconName && <i className={`la la-${iconName}`} style={{ "marginLeft": ".175rem" }}></i>}
            </div>
            <div className="d-flex">
                {isEditing
                    ? <input type="text" value={value} onChange={(e) => setValue(e.target.value)} />
                    : <div className="font-medium-3" onClick={() => setEditing(!isEditing)} style={{ "cursor": "pointer" }}>
                        {content}
                    </div>
                }
                {!isEditing
                    ? <div className="ml-1" onClick={() => setEditing(!isEditing)} style={{ "cursor": "pointer" }}>
                        <i className={`las la-pen la-1x`}></i>
                    </div>
                    : <>
                        <div className="edit-button font-small-4 ml-1" onClick={() => onSave()}>
                            <i className={`las la-check la-1x`}></i>
                            <span className="">{t('settings.save')}</span>
                        </div>
                        <div className="edit-button font-small-4 ml-1" onClick={() => setEditing(!isEditing)}>
                            <i className={`las la-times la-1x`}></i>
                            <span className="">{t('settings.cancel')}</span>
                        </div>
                    </>}
            </div>
        </div>
    )
}

const CompanyInfo = ({ company }) => {
    const { t } = useTranslation();
    const findAddress = (id) => allAddresses.filter(el => el.id === id)[0]

    const [logo, changeLogo] = useState(false)
    const [createdAt, setCreatedAt] = useState(company.createdAt)
    const [brandName, setBrandName] = useState(company.brandName)
    const [companyName, setCompanyName] = useState(company.companyName)
    const [phone, setPhone] = useState(company.phoneNumber)
    const [vatNumber, setVatNumber] = useState(company.vatNumber)
    const [website, setWebsite] = useState(company.website)
    const [email, setEmail] = useState(company.email)
    const [allAddresses, setAllAddresses] = useState(company.addresses);
    const [companyAddress, setCompanyAddress] = useState(findAddress(company.headquarters.id));
    const [shippingAddress, setShippingAddress] = useState(findAddress(company.shippingAddress.id));
    const [billingAddress, setBillingAddress] = useState(findAddress(company.billingAddress.id));

    useEffect(() => {
        const findAddress = (id) => allAddresses.filter(el => el.id === id)[0]
        setCompanyAddress(findAddress(company.headquarters.id))
        setShippingAddress(findAddress(company.shippingAddress.id))
        setBillingAddress(findAddress(company.billingAddress.id))
        setCreatedAt(getDateFromTimestamp(company.createdAt).getFullYear())
        // --------------------------------------------------------------------------------
        // _________________TODO: UNCOMMIT FOR BACKEND INTEGRATION_________________________
        // --------------------------------------------------------------------------------
        // --------------------------------------------------------------------------------
        // setLogo(company.logoUrl)
        // setBrandName(company.brandName)
        // setBrandName(company.brandName)
        // setCompanyName(company.companyName)
        // setPhone(company.phoneNumber)
        // setVatNumber(company.vatNumber)
        // setWebsite(company.website)
        // setEmail(company.email)
        // setAllAddresses(company.addresses)
        // setCompanyAddress(findAddress(company.headquarters.id))
        // setShippingAddress(findAddress(company.shippingAddress.id))
        // setBillingAddress(findAddress(company.billingAddress.id))
    }, [company, allAddresses])

    const setNewAddress = (address) => {
        setAllAddresses([
            ...allAddresses,
            address
        ])
    }

    // TODO: Save changes through API
    // TODO: Add addresses from company.addresses

    return (
        <div className="card">
            <div className="card-header pb-0">
                <div className="card-title text-uppercase">
                    {t('admin.company info')}
                </div>
            </div>
            <div className="card-content">
                <div className="card-body row pt-0">
                    <div className="col-12 col-md-3 text-center">
                        <div className="font-medium-1 font-weight-bold py-1">{t('admin.your logo')}</div>
                        {logo
                            ? <>
                                <UploadLogo circular />
                                <div className="mt-1">
                                    <button onClick={() => changeLogo(false)} className="info mx-1">{t('settings.cancel')}</button>
                                    <button onClick={() => changeLogo(false)} className="info mx-1">{t('settings.upload')}</button>
                                </div>
                            </>
                            : <>
                                <img width="120px" height="120px" src={CompanyLogo} alt="Company Logo" />
                                <div className="mt-1">
                                    <button onClick={() => changeLogo(true)} className="info">{t('admin.change logo')}</button>
                                </div>
                            </>
                        }
                    </div>
                    <div className="col-12 col-md-5">
                        <div className="row">
                            <div className="col-6">
                                <CompanyElement
                                    title={t('admin.brand name')}
                                    content={brandName}
                                    onEdit={(value) => setBrandName(value)}
                                />
                                <CompanyElement
                                    title={t('admin.company name')}
                                    content={companyName}
                                    onEdit={(value) => setCompanyName(value)}
                                />
                                <CompanyElement
                                    title={t('admin.phone')}
                                    iconName="phone"
                                    content={phone}
                                    onEdit={(value) => setPhone(value)}
                                />
                            </div>
                            <div className="col-6">
                                <CompanyElement
                                    title={t('admin.vat number')}
                                    iconName=""
                                    content={vatNumber}
                                    onEdit={(value) => setVatNumber(value)}
                                />
                                <CompanyElement
                                    title={t('admin.website')}
                                    iconName="globe"
                                    content={website}
                                    onEdit={(value) => setWebsite(value)}
                                />
                                <CompanyElement
                                    title={t('settings.email')}
                                    iconName="at"
                                    content={email}
                                    onEdit={(value) => setEmail(value)}
                                />
                            </div>
                        </div>
                    </div>
                    {/* <div className="col-12 col-md-2">
                    </div> */}
                    <div className="col-12 col-md-4">
                        <AddressElement
                            title={t('admin.headquarters')}
                            content={companyAddress}
                            addresses={allAddresses}
                            onChange={(address) => setCompanyAddress(address)}
                        />
                    </div>
                </div>
                <div className="d-flex my-1 px-3">
                    <div className="mr-3">
                        <AddressElement
                            title={t('admin.billing address')}
                            content={billingAddress}
                            addresses={allAddresses}
                            onChange={(address) => setBillingAddress(address)}
                        />
                    </div>
                    <div className="">
                        <AddressElement
                            title={t('admin.shipping address')}
                            content={shippingAddress}
                            addresses={allAddresses}
                            onChange={(address) => setShippingAddress(address)}
                        />
                    </div>
                </div>

                {/* All addresses */}
                <div className="font-small-4 font-weight-bold mb-0 pl-3">{t('admin.all addresses')} <AddNewAddressElement onSave={(address) => setNewAddress(address)} /></div>
                <div className="d-flex mb-1 px-3">
                    {allAddresses.map(address =>
                        <div key={address.id} className="mr-3">
                            <AddressElement
                                title=""
                                content={address}
                                onSave={(selectedAddress) => setAllAddresses(allAddresses.map(el =>
                                    el.id === selectedAddress.id
                                        ? { ...selectedAddress }
                                        : { ...el }
                                ))}
                            />
                        </div>
                    )}
                </div>
                <div className="card-footer">
                    <span className="mt-1 secondary h5">{t('admin.subscribed since')} {createdAt}</span>
                </div>
            </div>
        </div>
    )
}

export default CompanyInfo