const IconSelectionItem = ({ label, icon }) => {
    return (
        <button className="inline-flex flex-col space-y-1.5 items-center justify-start w-16 h-20 p-1 pt-2 mr-1 mt-1 bg-gray-100 hover:bg-gray-300 active:bg-hover-400 rounded-lg cursor-pointer select-none">
            <div className="w-9 h-9">{icon}</div>
            <p className="text-xxs text-center py-0.5">{label}</p>
        </button>
    )
}

IconSelectionItem.defaultProps = {
    label: '',
    icon: <div className="bg-white rounded-2xl w-full h-full"></div>
}

export default IconSelectionItem