import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import Card from './Card'
import UploadFile from './UploadFile'
import Autocomplete from './Autocomplete'
import MultiLanguageDescription from './MultiLanguageDescription'
import queryString from 'query-string'
import { getWineInfo, createNewWine, createNewVintage } from '../utils/api'
import config from '../config';
import { s3Upload } from "../utils/helpers";

const wineMoods = [
    "In Compagnia",
    "Serata romantica",
    "Party con amici",
    "Serata TV",
    "Da lettura"
];

const wineSuggestions = [
    "Carne",
    "Pesce",
    "Primi piatti",
    "Secondi",
    "Dessert"
];

const wineRegions = [
    "Bolgheri",
    "Chianti",
    "Chianti Classico",
    "Colli Apuani",
    "Colli dell'Etruria Centrale",
    "Colline Lucchesi",
    "Elba",
    "Montalcino",
    "Montescudaio",
    "Parrina",
    "Pitigliano",
    "San Gimignano",
    "Scansano",
    "Val di Chiana",
    "Val di Cornia",
    "Valdinievole",
    "Valle di Arbia",
];

// TODO: Use the 18next to translate the labels
const CreateWineCard = (props) => {
    let history = useHistory();
    const [editing, setEditing] = useState(null)
    const [uploadedLabel, setUploadedLabel] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const [wine, setWine] = useState({
        // id: '',
        name: "Vino nuovo",
        type: "red",
        description: {
            "it": "Il mio vino nuovo è il migliore",
            "en": "My new wine is the best",
        },
    })
    const [vintage, setVintage] = useState({
        year: '',
        grape: '',
        alcohol: '',
        temperature: '',
        // mood: [],
        // pairing: [],
        regioni: [],
        allergeni: [],
        description: {
            "it": "",
            "en": "",
        },
    })

    // TODO: Find a better solution for handleWineChange and handleVintageChange
    const handleWineChange = (event) => {
        console.log("efjwiero", wine)
        const { name, value } = event.target;
        setWine({
            ...wine,
            [name]: value,
        })
    }
    const handleVintageChange = (event) => {
        console.log("efjwiero", wine)
        const { name, value } = event.target;
        setVintage({
            ...vintage,
            [name]: value
        })
    }

    useEffect(() => {
        const { id, vintage, editing } = queryString.parse(props.location.search)
        if (editing === 'wine' || (!vintage && editing === 'vintage')) {
            setEditing(editing === 'wine' ? 'wine' : 'vintage')
            // Set wine data
            getWineInfo(id)
                .then(data => setWine({
                    id: data.wineId,
                    name: data.wine_name,
                    type: data.type,
                    description: { ...data.descrizione }
                }))
                .catch(err => console.log("There was an error receiving data", err))
        } else if (editing === 'vintage') {
            setEditing('vintage')
            // Set wine and vintage data
            // instead of using index 0, change the api to search for the year of the wine☺
            getWineInfo(id, vintage)
                .then(data => {
                    console.log(data)
                    // Set wine data
                    setWine({
                        name: data.wine.wine_name,
                        type: data.wine.type,
                        description: { ...data.wine.descrizione }
                    })
                    // Set vintage data
                    setVintage({
                        year: data.vintage.year,
                        grape: data.wine.vitigno,
                        alcohol: data.wine.grado_alcolico,
                        temperature: data.wine.temperatura_consigliata,
                        mood: data.vintage.mood,
                        pairing: data.vintage.abbinamenti,
                        regioni: data.vintage.year,
                        allergeni: data.vintage.year,
                        // TODO: needs to be changed for the vintage
                        description: data.wine.descrizione,
                    })
                })
                .catch(err => console.log("There was an error receiving data", err))
        }
        console.log("editing", editing)
    }, [setEditing, setWine, setVintage, props.location.search])

    async function handleSubmit(event) {
        event.preventDefault();
        // Check image file size
        if (uploadedLabel && uploadedLabel.size > config.MAX_ATTACHMENT_SIZE) {
            alert(
                `Please pick a file smaller than ${config.MAX_ATTACHMENT_SIZE /
                1000000} MB.`
            );
            return;
        }
        console.log("Loading before:", isLoading)
        console.log("Loading...")
        setIsLoading(true)
        console.log("Loading:", isLoading)
        try {
            if (editing === 'vintage') {
                // Upload image before the wine/vintage
                const labelKey = uploadedLabel.current ? await s3Upload(uploadedLabel.current) : null;
                // Add the response key as img value to the vintage/object
                setVintage({
                    ...vintage,
                    images: {
                        wineLabel: labelKey
                    }
                })
                // Save new wine on db
                const res = createNewVintage(wine.id, vintage)
                console.log("Vintage successfully created:", res)
            } else if (editing === 'wine' || editing === null) {
                console.log("Creating wine")
                // Create wine obj on db
                const res = await createNewWine({
                    ...wine,
                    description: JSON.stringify({ ...wine.description })
                })
                // Display confirmation
                console.log("Wine successfully created:", res)
            }
            // Redirect
            // const signedURL = await Storage.vault.get(attachment)
            setIsLoading(false);
        } catch (e) {
            console.log("error", e)
            // Display error information
            //   onError(e);
            setIsLoading(false);
        }
    }

    const getCurrentYear = () => {
        return new Date().getFullYear()
    }
    // TODO: allow autocomplete to work with state
    return (
        <form id="submit-wine" onSubmit={handleSubmit}>
            <div className="row">
                {/* Wine form */}
                <div className="col-12 col-lg-7 col-xl-8">
                    {/* Wine generic info */}
                    <Card title="Wine details">
                        <div className="card-body pt-0">
                            <InputElement title="Wine Name">
                                <fieldset className="form-group position-relative">
                                    <input
                                        type="text"
                                        className="form-control"
                                        name="name"
                                        placeholder="Wine name"
                                        value={wine.name}
                                        onChange={handleWineChange}
                                        disabled={editing === "vintage" ? true : false}
                                    />
                                </fieldset>
                            </InputElement>
                            <InputElement title="Type">
                                <fieldset className="form-group position-relative">
                                    <select
                                        className="form-control single-input"
                                        name="type"
                                        value={wine.type}
                                        onChange={handleWineChange}
                                        disabled={editing === "vintage" ? true : false}
                                    >
                                        <option value="red">Red</option>
                                        <option value="white">White</option>
                                        <option value="rosé">Rosé</option>
                                    </select>
                                </fieldset>
                            </InputElement>
                            <InputElement title="Description">
                                <fieldset className="form-group position-relative">
                                    <MultiLanguageDescription
                                        label={'Descrizione Vino:'}
                                        placeholder={'Scrivi la tua descrizione'}
                                        languages={wine.description}
                                        onChange={({ currentLanguage, value }) => setWine({
                                            ...wine,
                                            description: {
                                                ...wine.description,
                                                [currentLanguage]: value
                                            }
                                        })}
                                        disabled={editing === "vintage" ? true : false}
                                    />
                                </fieldset>
                            </InputElement>
                        </div>
                    </Card>
                    {/* Vintage specific info */}
                    {editing === 'vintage' &&
                        <Card title="Add new vintage">
                            <div className="card-body pt-0">
                                <InputElement title="Wine Name">
                                    <fieldset className="form-group position-relative">
                                        <input
                                            type="number"
                                            className="form-control"
                                            min="100"
                                            max={getCurrentYear()}
                                            placeholder={`${getCurrentYear() - 2}`}
                                            name="year"
                                            value={vintage.year}
                                            onChange={handleVintageChange}
                                            disabled={editing === "wine" ? true : false}
                                        />
                                    </fieldset>
                                </InputElement>
                                <InputElement title="Vitigno">
                                    <fieldset className="form-group position-relative">
                                        <input
                                            type="text"
                                            className="form-control"
                                            placeholder="Type in the grapes"
                                            name="grape"
                                            value={vintage.grape}
                                            onChange={handleVintageChange}
                                            disabled={editing === "wine" ? true : false}
                                        />
                                    </fieldset>
                                </InputElement>
                                <InputElement title="Alcohol (%)">
                                    <fieldset className="form-group position-relative">
                                        <input
                                            type="number"
                                            className="form-control"
                                            min="0"
                                            max="100"
                                            placeholder="12"
                                            name="alcohol"
                                            value={vintage.alcohol}
                                            onChange={handleVintageChange}
                                            disabled={editing === "wine" ? true : false}
                                        />
                                    </fieldset>
                                </InputElement>
                                <InputElement title="Temperatura consigliata (°C)">
                                    <fieldset className="form-group position-relative">
                                        <input
                                            type="number"
                                            min="0"
                                            max="32"
                                            className="form-control"
                                            placeholder="4"
                                            name="temperature"
                                            value={vintage.temperature}
                                            onChange={handleVintageChange}
                                            disabled={editing === "wine" ? true : false}
                                        />
                                    </fieldset>
                                </InputElement>
                                <InputElement title="Mood">
                                    <fieldset className="form-group position-relative">
                                        <Autocomplete
                                            placeholder="Aggiungi Mood"
                                            disabled={editing === "vintage" ? true : false}
                                            suggestions={wineMoods}
                                        />
                                    </fieldset>
                                </InputElement>
                                <InputElement title="Abbinamenti">
                                    <fieldset className="form-group position-relative">
                                        <Autocomplete
                                            placeholder="Aggiungi abbinamento"
                                            disabled={editing === "vintage" ? true : false}
                                            suggestions={wineSuggestions}
                                        />
                                    </fieldset>
                                </InputElement>
                                <InputElement title="Allergeni">
                                    <fieldset className="form-group input-group" disabled={editing === "vintage" ? true : false}>
                                        <div className="form-check form-check-inline mt-1">
                                            <input className="checkbox" type="checkbox" name="" id="allergene" value="allergene" />
                                            <label className="form-check-label" htmlFor="allergene">Solfiti</label>
                                        </div>
                                        <div className="form-check form-check-inline mt-1">
                                            <input className="checkbox" type="checkbox" name="" id="allergene2" value="allergene2" />
                                            <label className="form-check-label" htmlFor="allergene2">Albumina</label>
                                        </div>
                                        <div className="form-check form-check-inline mt-1">
                                            <input className="checkbox" type="checkbox" name="" id="allergene3" value="allergene3" />
                                            <label className="form-check-label" htmlFor="allergene3">Latticini</label>
                                        </div>
                                    </fieldset>
                                </InputElement>
                                <InputElement title="Regioni">
                                    <fieldset className="form-group position-relative">
                                        <Autocomplete
                                            placeholder="Aggiungi regione"
                                            disabled={editing === "vintage" ? true : false}
                                            suggestions={wineRegions}
                                        />
                                    </fieldset>
                                </InputElement>
                                <InputElement title="Description">
                                    <fieldset className="form-group position-relative">
                                        <MultiLanguageDescription
                                            label={'Descrizione Annata:'}
                                            placeholder={'Scrivi la tua descrizione'}
                                            languages={vintage.description}
                                            onChange={({ currentLanguage, value }) => setVintage({
                                                ...vintage,
                                                description: {
                                                    ...vintage.description,
                                                    [currentLanguage]: value
                                                }
                                            })}
                                            disabled={editing === "wine" ? true : false}
                                        />
                                    </fieldset>
                                </InputElement>
                            </div>
                        </Card>}
                </div>

                {/* Image column */}
                <div className={"col-12 col-lg-5 col-xl-4"}>
                    {editing === 'vintage' &&
                        <Card title="Wine Label">
                            <UploadFile
                                placeholder="Upload your wine label here (200x400px)"
                                disabled={editing === "wine" ? true : false}
                                onUpload={(files) => setUploadedLabel(files[0])}
                            />
                        </Card>
                    }
                    <button type="reset" onClick={() => history.goBack()} className="btn btn-block bg-primary text-white">
                        Cancel
                        </button>
                    <button type="submit" className="btn btn-block bg-danger text-white">
                        {isLoading ? "Saving..." : "Save"}
                    </button>
                </div>
            </div>
        </form>
    )
}

export default CreateWineCard

const InputElement = ({ title, ...props }) => {
    return (
        <div className="">
            <div className="font-small-5">{title}</div>
            <div>
                {props.children}
            </div>
        </div>
    )
}