import React, { useState } from 'react'

const Filter = ({ selection, onSelect }) => {
    const [opened, setOpened] = useState(false)
    const [selected, changeSelected] = useState('')
    const selectFilter = (value) => {
        onSelect(value);
        changeSelected(value)
        setOpened(false)
    }
    return (
        <div className={`dropdown${opened ? " show" : " hide"}`}>
            <button onClick={() => setOpened(!opened)} className="btn ">
                <span>{selected ? selected : selection[0]}</span>
                <i className={`las la-angle-${opened ? "up" : "down"}`} style={{ "marginLeft": "5px" }}></i>
            </button>
            {opened &&
                <div className="dropdown-menu">
                    {selection.map(item =>
                        <button className="btn btn-block dropdown-item" onClick={() => selectFilter(item)}>{item}</button>
                    )}
                </div>
            }
        </div>
    )
}

export default Filter