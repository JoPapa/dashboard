import React from 'react'
import "./VenueView.css"

const VenueView = ({ venue, selected, onSelected }) => {
    return (
        <div className={`font-medium-1 venue-view${selected ? " selected" : ""}`} onClick={onSelected}>
            <i className="las la-building la-3x venue-image"></i>
            <div className="venue-title font-weight-bold">{venue.toUpperCase()}</div>
        </div>
    )
}

export default VenueView