import React from 'react'
import { useTranslation } from 'react-i18next';

const SubscriptionCard = ({ title, subtitle, onRemove, onRenew, onActivate }) => {
    const { t } = useTranslation();
    return (
        <div className="card text-center">
            <i className={`las la-${title.toLowerCase() === "blockchain" ? "link" : "laptop"} la-5x mt-1`}></i>
            <div className="card-header h4 pb-1">{title}</div>
            <div className="card-body pt-0">
                {subtitle && <div className="font-medium-1">{subtitle}</div>}
                {onActivate &&
                    <button className="btn btn-outline-info round mt-1" onClick={onActivate}>
                        <span className="mx-1">{t('admin.activate subscription')}</span>
                    </button>
                }
                {onRenew &&
                    <button className="btn btn-outline-info round mt-1 mr-1" onClick={onRenew}>
                        <span className="mx-1">{t('admin.renew')}</span>
                        <i className="las la-redo-alt"></i>
                    </button>
                }
                {onRemove &&
                    <button className="btn btn-outline-danger round mt-1" onClick={onRemove}>
                        <span className="mx-1">{t('admin.remove subscription')}</span>
                        <i className="las la-times"></i>
                    </button>
                }
            </div>
        </div>
    )
}

export default SubscriptionCard