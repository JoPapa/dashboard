import React from "react";
import { Route, Redirect } from "react-router-dom";

export default function AuthenticatedRoute({ component: C, appProps, authorizedRole, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        !appProps.isAuthenticated
          ? <Redirect exact to={`/login?redirect=${props.location.pathname}${props.location.search}`} />
          : appProps.role
            ? <Redirect exact to={appProps.role === 'pro' ? '/homepro' : '/analytics'} />
            : <C {...props} {...appProps} />
      }
    />
  );
}