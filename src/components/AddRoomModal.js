import React, { useState } from 'react';
import { useTranslation } from 'react-i18next'
import Modal from 'react-modal'

const modalStyle = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        border: '0',
        borderRadius: '15px',
        boxShadow: '0px 0px 20px 0px #c5c5c5',
        marginRight: '-50%',
        padding: '50px',
        transform: 'translate(-50%, -50%)'
    }
}

const SubscribeModal = (props) => {
    const { t } = useTranslation()
    const { isOpen, onRequestClose, onSave, venue } = props
    const [roomName, setRoomName] = useState('')

    const onClose = () => {
        setRoomName('')
        onRequestClose()
    }

    const onConfirm = () => {
        onSave(roomName)
        setRoomName('')
        onRequestClose()
    }

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onClose}
            style={modalStyle}
            contentLabel="Add a new room"
        >
            <div className="text-center">
                <button
                    type="button"
                    onClick={onClose}
                    className="close"
                    aria-label="Close"
                    style={{
                        position: 'absolute',
                        top: '20px',
                        right: '25px',
                    }}>
                    <i className="la la-times"></i>
                </button>
                <h3>{t('dispensers.add room.title')}</h3>
                <p className="font-medium-2 mt-1">{t('dispensers.add room.subtitle', { venue })}</p>
                <fieldset className="form-group position-relative has-icon-left">
                    <input type="text" className="form-control" id="roomName" placeholder={t('dispensers.add room.placeholder')} value={roomName} onChange={(e) => setRoomName(e.target.value)} required />
                </fieldset>
                <button type="button" className="btn btn-outline-danger round" onClick={() => roomName.length > 0 && onConfirm()}>
                    <span className="text-uppercase mx-1">{t('settings.save')}</span>
                    <i className="las la-arrow-right"></i>
                </button>
            </div>
        </Modal >
    )
}

export default SubscribeModal