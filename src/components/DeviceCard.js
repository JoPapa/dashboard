import React from 'react'
import PropTypes from 'prop-types'
import Dispenser from '../images/dispenser-icon.png'

const DeviceCard = ({ title, model, onRemove, onRenew, app, subscription }) => {
    return (
        <div className="card text-center">
            {!subscription && !app
                ? <img className="card-img-top pt-1" src={Dispenser} alt="Dispenser Icon" style={{ "maxHeight": "85px", "objectFit": "contain" }} />
                : <i className={`las la-${subscription ? "laptop" : "mobile"} la-5x mt-1`}></i>
            }
            <div className="card-header h4 pb-1">
                {title}
            </div>
            <div className="card-body pt-0">
                <div className="font-medium-1">{model}</div>
                {subscription &&
                    <button class="btn btn-outline-info round mt-1 mr-1" onClick={onRenew}>
                        <span class="mx-1">Renew</span>
                        <i class="las la-redo-alt"></i>
                    </button>
                }
                <button class="btn btn-outline-danger round mt-1" onClick={onRemove}>
                    <span class="mx-1">Remove {app ? "App" : subscription ? "subscription" : "Device"}</span>
                    <i class="las la-times"></i>
                </button>
            </div>
        </div>
    )
}

DeviceCard.defaultProps = {
    title: "Dispenser",
    model: "Albi Home M"
}

DeviceCard.propTypes = {
    title: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired
}

export default DeviceCard