import React from 'react';
import PropTypes from 'prop-types';

export const Button = ({ primary, backgroundColor, hoverColor, size, label, ...props }) => {
  return (
    <button
      type="button"
      className={`inline-flex items-center justify-center px-4 py-2 ${backgroundColor} ${hoverColor} shadow rounded`}
      style={backgroundColor && { backgroundColor }}
      {...props}
    >
      <p className="flex-1 h-full text-sm font-medium leading-tight text-center text-white">{label}</p>
    </button>
  );
};

Button.propTypes = {
  /**
   * Is this the principal call to action on the page?
   */
  primary: PropTypes.bool,
  /**
   * What background color to use
   */
  backgroundColor: PropTypes.string,
  /**
   * How large should the button be?
   */
  size: PropTypes.oneOf(['small', 'medium', 'large']),
  /**
   * Button contents
   */
  label: PropTypes.string.isRequired,
  /**
   * Optional click handler
   */
  onClick: PropTypes.func,
};

Button.defaultProps = {
  backgroundColor: "bg-green-500",
  hoverColor: "hover:bg-green-400",
  primary: false,
  size: 'medium',
  onClick: undefined,
};
