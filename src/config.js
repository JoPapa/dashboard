const config = {
    s3: {
        REGION: "eu-central-1",
        BUCKET: "wines-on-albicchiere",
    },
    apiGateway: {
        REGION: "eu-central-1",
        URL: "https://yzapqv2ulf.execute-api.eu-central-1.amazonaws.com/Prod"
    },
    cognito: {
        REGION: "eu-central-1",
        USER_POOL_ID: "eu-central-1_ARlvjRiMd",
        APP_CLIENT_ID: "7rlpjima4rp7rvlrf1jmkh6at4",
        IDENTITY_POOL_ID: "eu-central-1:fd55cbb8-7624-4811-b99a-78ee416e097c"
    },
    social: {
        google: "183611016811-2rspi0jcmhpthjkum2fl5m8lurcnf4io.apps.googleusercontent.com",
        facebook: "420084055619846",
        amazon: "amzn1.application-oa2-client.57b79d794e604fb78be1206f404456d4"
    }
};
export default config