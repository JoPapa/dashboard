import { useState, useEffect } from 'react'

export default function useLocalStorageState(key, defaultValue = "") {
    const [value, setValue] = useState(() => {
        const currentValue = window.localStorage.getItem((key));
        if (currentValue) {
            return JSON.parse(currentValue)
        } else {
            return defaultValue
        }
    })
    useEffect(() => {
        window.localStorage.setItem(key, JSON.stringify(value))
    }, [key, value])
    return [value, setValue]
}