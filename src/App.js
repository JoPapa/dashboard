import React, { Suspense, useState, useEffect } from 'react'
import { AppContext } from './utils/contexts'
import { Auth } from 'aws-amplify'
import Routes from './Routes'
import useLocalStorageState from './hooks/useLocalStorageState'
// import AccountWarning from './components/AccountWarning'
import './App.css'
import Menu from './components/Header/Menu'

export const Header = React.lazy(() => import('./components/Header/Header'));
// TODO: Remove some local storage for authorization and role
const App = () => {
  // Toggles the left navbar menu
  const [menuOpen, setMenuOpen] = useState(true)
  // Loading state for authentication
  const [isAuthenticating, setIsAuthenticating] = useState(false)
  // User is authenticated whith Cognito
  const [isAuthenticated, setIsAuthenticated] = useState(true)
  // User is auhorized when associated to a company
  const [authorized, setAuthorized] = useLocalStorageState('authorized', true)
  // TODO: Do we need roles? Don't think so..
  const [role, setRole] = useLocalStorageState('role', 'winery')
  const [currentUser, setCurrentUser] = useState({
    id: "",
    firstName: "",
    lastName: "",
    email: "",
    profile: "",
    birthdate: "12/01/1978",
    gender: "male",
    nationality: "Italian",
    language: "IT",
    measureSystem: "metric",
    dateFormat: "dayFirst",
    hourFormat: "24h",
    firstDayOfWeek: "monday",
    newsletter: "no",
    newsletterDashboard: "no",
    notifications: "no",
    linkedCompanies: [
      {
        "id": "000001",
        "companyName": "Cantina Albi",
        "img": "https://...",
        "subscriptions": ["albi_pro", "albi_blockchain"]
      },
      {
        "id": "000002",
        "companyName": "Albi Resorts",
        "img": "https://...",
        "subscriptions": ["albi_restaurants_free"]
      },
      {
        "id": "000003",
        "companyName": "Albi Luxury Hotels",
        "img": "https://...",
        "subscriptions": ["albi_hotels_free"]
      }
    ]
  })
  const [currentCompany, setCurrentCompany] = useLocalStorageState('company', {
    id: "000001",
    "companyName": "Cantina Albi",
    "img": "https://...",
    "subscriptions": ["albi_pro", "albi_blockchain"]
  })

  // Switch company
  const setCompanyById = (id) => {
    const chosenCompany = currentUser.linkedCompanies.filter(company => company.id === id)[0]
    console.log("Switched company to: ", chosenCompany)
    setCurrentCompany(chosenCompany)
  }

  const state = {
    menuOpen,
    setMenuOpen,
    isAuthenticating,
    isAuthenticated,
    authorized,
    role
  }
  // TODO: Clean the app from the authenticate function
  // Authenticate the user
  const authenticate = (value) => {
    setIsAuthenticated(value)
    console.log("authenticated: ", isAuthenticated)
  }

  // Authorize the use of the application
  const authorize = (role) => {
    if (role === "all" || role === "winery" || role === "pro")
      setRole(role)
    setAuthorized(true)
  }

  useEffect(() => {
    // Authenticate user on first load
    (async function onLoad() {
      console.log("Albicchiere is running")
      try {
        // console.log("IDT: ", (await Auth.currentSession()))
        // console.log("Authenticated id token", (await Auth.currentSession()).getIdToken().getJwtToken())

        // Check if there is an authenticated user in locale
        const user = (await Auth.currentAuthenticatedUser())
        console.log("Authenticated user", user)
        // Set new user object
        setCurrentUser(prevUser => ({
          ...prevUser,
          firstName: user.payload ? user.payload.givenName : user.attributes ? user.attributes.given_name : user.givenName,
          lastName: user.payload ? user.payload.familyName : user.attributes ? user.attributes.family_name : user.familyName,
          email: user.payload ? user.payload.email : user.attributes ? user.attributes.email : user.email,
        }))
        // Authenticate
        setIsAuthenticated(true);
      }
      catch (e) {
        if (e !== 'No current user') { console.log(e); }
        console.log(e)
      }
      setIsAuthenticating(false);
    })()
  }, []);

  // LOG OUT the user
  async function handleLogout() {
    await Auth.signOut();
    setIsAuthenticated(false);
  }

  // const menuIsOpen = menuOpen ? " menu-open" : " menu-hide";
  return (
    !isAuthenticating && (
      <>
        {isAuthenticated
          ? <>
            <AppContext.Provider value={{ isAuthenticated, authorized, role, currentUser, setCurrentUser, currentCompany, setCurrentCompany, setCompanyById, authenticate, authorize, handleLogout }}>
              <Suspense fallback={<div>...</div>}>
                {/* <Header toggleMenu={() => setMenuOpen(!menuOpen)} menuOpen={menuOpen} userRole={role} /> */}
                <Header
                  // user={}
                  // company={}
                  // otherCompanies={}
                />
                {/* Side Nav */}
                <Menu />
                {/* <AccountWarning /> */}
                <main className="w-full min-h-screen h-full max-w-screen-2xl mx-auto py-20 pl-28 pr-4">
                    <Routes appProps={state} />
                </main>
              </Suspense>
            </AppContext.Provider>
          </>
          : <AppContext.Provider value={{ isAuthenticated, authorized, role, setCurrentUser, authenticate, authorize }}>
            <Routes
              appProps={state}
              authorize={authorize}
              authenticate={authenticate}
            />
          </AppContext.Provider>
        }
        {/* // Footer */}
      </>

    )
  );
}

export default App;
