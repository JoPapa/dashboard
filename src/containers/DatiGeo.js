import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import Map from '../components/Charts/Map'
import TopFive from '../components/Charts/TopFive'
import DoubleCard from '../components/Charts/DoubleCard'
import RosePie from '../components/Charts/RosePie'
import BasicPie from '../components/Charts/BasicPie'
import Histogram from '../components/Charts/Histogram';
import FilterButton from '../components/FilterButton';
import DataPicker from '../components/DataPicker';

const ChiSono = (props) => {
    window.scrollTo(0, 0);
    // dati mappa
    let mapData = {
        CN: 100000,
        IN: 9900,
        SA: 86,
        EG: 70,
        SE: 0,
        FI: 0,
        FR: 0,
        US: 20,
        ZA: 32
    }

    let topNations = [
        {
            id: 1,
            name: "Cina",
            percentage: "90"
        },
        {
            id: 2,
            name: "Stati Uniti",
            percentage: "60"
        },
        {
            id: 3,
            name: "Italia",
            percentage: "50"
        },
        {
            id: 4,
            name: "Francia",
            percentage: "30"
        },
        {
            id: 5,
            name: "Spagna",
            percentage: "20"
        }
    ];

    // grafico Fasce d'eta'
    let fasceEta = [
        { value: 335, name: '18-30' },
        { value: 310, name: '31-45' },
        { value: 234, name: '45-54' },
        { value: 135, name: '55-64' },
        { value: 1548, name: 'over 65' }
    ];

    let fasceOrarie = [
        { value: 13, name: '25:00 - 4:00' },
        { value: 14, name: '4:00 - 8:00' },
        { value: 15, name: '8:00 - 12:00' },
        { value: 18, name: '12:00 - 16:00' },
        { value: 20, name: '16:00 - 20:00' },
        { value: 16, name: '20:00 - 24:00' }
    ];

    let weekData = [
        { name: 'Lun', value: 10 },
        { name: 'Mar', value: 20 },
        { name: 'Mer', value: 35 },
        { name: 'Gio', value: 25 },
        { name: 'Ven', value: 12 },
        { name: 'Sab', value: 3 },
        { name: 'Dom', value: 6 }
    ];

    let stagioni = [
        { value: 335, name: 'Inverno' },
        { value: 310, name: 'Primavera' },
        { value: 234, name: 'Estate' },
        { value: 135, name: 'Autunno' }
    ]

    const { t } = useTranslation()
    return (
        <>
            {/* Intro Row */}
            <div className="row">
                <div className="col-md-8 col-12">
                    <Link className="text-left btn btn-primary m-1" to="/analytics"> {"< Back"}</Link>
                    <FilterButton />
                </div>
                <div className="col-md-4 col-12">
                    <DataPicker />
                </div>
            </div>

            {/* First Row */}
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <div className="row">
                            <div className="col-md-4 col-12">
                                <TopFive title={t('charts.top nations')}
                                    data={topNations} />
                            </div>
                            <div className="col-md-8 col-12">
                                <Map data={mapData} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* Second Row */}
            <div className="row">

                <div className="col-md-4">
                    <div className="card">
                        <div className="card-header">
                            <h4 className="card-title text-center">{t('charts.seasons.title')}</h4>
                        </div>
                        <div className="card-body">
                            <BasicPie data={stagioni} legend />
                        </div>
                    </div>
                </div>

                <div className="col-md-4">
                    <div className="card">
                        <div className="card-header">
                            <h4 className="card-title text-center">{t('charts.weekdays.title')}</h4>
                        </div>
                        <div className="card-body">
                            <Histogram data={weekData} />
                        </div>
                    </div>
                </div>

                <div className="col-md-4">
                    <div className="card">
                        <div className="card-header">
                            <h4 className="card-title text-center">{t('charts.time slots')}</h4>
                        </div>
                        <div className="card-body">
                            <RosePie data={fasceOrarie} />
                        </div>
                    </div>
                </div>

            </div>

            <hr />

            {/* Third Row */}
            <div className="row">

                <div className="col-md-4">
                    <div className="card">
                        <div className="card-header">
                            <h4 className="card-title text-center">{t('charts.age groups')}</h4>
                        </div>
                        <div className="card-body">
                            <BasicPie data={fasceEta} legend />
                        </div>
                    </div>
                </div>

                <div className="col-md-8">

                    <div className="row">

                        <div className="col-md-6">
                            <DoubleCard title={t('charts.total users.title')}
                                left={{
                                    name: t('charts.total users.male'),
                                    icon: "la la-mars",
                                    percentage: "40%"
                                }}
                                right={{
                                    name: t('charts.total users.female'),
                                    icon: "la la-venus",
                                    percentage: "60%"
                                }} />
                        </div>

                        <div className="col-md-6">
                            <DoubleCard title={t('charts.consumption.title')}
                                left={{
                                    name: t('charts.consumption.alone'),
                                    icon: "la la-user",
                                    percentage: "45%"
                                }}
                                right={{
                                    name: t('charts.consumption.with others'),
                                    icon: "la la-users",
                                    percentage: "55%"
                                }} />
                        </div>

                    </div>

                    <div className="row">

                        <div className="col-md-6">
                            <DoubleCard title={t('charts.pouring options.title')}
                                left={{
                                    name: t('charts.pouring options.taste'),
                                    icon: "la la-glass",
                                    percentage: "23%"
                                }}
                                right={{
                                    name: t('charts.pouring options.glass'),
                                    icon: "la la-glass",
                                    percentage: "77%"
                                }} />
                        </div>
                        <div className="col-md-6">
                            <DoubleCard title={t('charts.consumption period.title')}
                                left={{
                                    name: t('charts.consumption period.during meals'),
                                    icon: "la la-cutlery",
                                    percentage: "82%"
                                }}
                                right={{
                                    name: t('charts.consumption period.between meals'),
                                    icon: "la la-birthday-cake",
                                    percentage: "18%"
                                }} />
                        </div>

                    </div>

                </div>

            </div>
        </>
    )
}

export default ChiSono