import React from 'react'
import { Translation, useTranslation } from 'react-i18next';
// import WineInfoCard from '../components/Charts/WineInfoCard'
import Timeline from '../components/Charts/Timeline'
import ResponsiveTable from '../components/Charts/ResponsiveTable'
import BlockchainBoxScores from '../components/BlockchainBoxScores'
import WeatherMap from '../components/WeatherMap'
import { Link } from 'react-router-dom'
import { getBoxInfo } from '../utils/api'
import queryString from 'query-string'
import './BlockchainBox.css'
import Box from '../images/BlockchainBox.jpg'
import WineAttribute from '../components/WineAttribute'
import SmartBag from '../images/smartbag-icon.png'


class BlockchainBox extends React.Component {
    state = {
        isLoading: true,
        boxTag: "",
        packingDate: "",
        qualityScores: "",
        wineInside: [],
        boxCoordinates: {
            latitude: null,
            longitude: null
        },
        blockchainLogs: [],
        mainEvents: [
            {
                timestamp: '15 Gen',
                location: 'Corciano, PG',
                message: 'Rilevata temperatura eccessiva',
                description: "Una temperatura eccessiva è stata rilevata durante il trasporto a Corciano, PG"
            },
            {
                timestamp: '16 Gen',
                location: 'Corciano, PG',
                message: 'Trasporto Corriere',
                description: 'Il pacco è stato preso in carico dal corriere'
            },
            {
                timestamp: '18 Gen',
                location: 'Corciano, PG',
                message: 'Rilevata temperatura eccessiva',
                description: "Una temperatura eccessiva è stata rilevata durante il trasporto a Corciano, PG"
            },
            {
                timestamp: '20 Gen',
                location: 'Corciano, PG',
                message: 'Arrivo a destinazione',
                description: 'I vini sono arrivati a destinazione'
            },
        ]
    }

    componentDidMount() {
        const { id } = queryString.parse(this.props.location.search)
        getBoxInfo(id)
            .then(data => {
                this.setState({
                    isLoading: false,
                    boxTag: data.tag,
                    packingDate: data.packingDate,
                    wineInside: data.wineInside,
                    qualityScores: data.qualityScores,
                    boxCoordinates: data.coordinates,
                    blockchainLogs: data.blockchainLogs
                })
            })
            .catch(err => console.log("There was an error receiving data", err))
    }
    render() {
        window.scroll(0, 0);
        const { isLoading, wineInside, boxTag, packingDate, qualityScores, boxCoordinates, blockchainLogs, mainEvents } = this.state
        return (
            <Translation>
                {(t) =>
                    isLoading
                        ? <h3>{t('settings.loading')}...</h3>
                        : <>
                            <div className="row match-height">
                                <div className="col-12 col-xl-8">
                                    <BoxInfoCard
                                        boxTag={boxTag}
                                        packingDate={packingDate}
                                        logs={wineInside}
                                    />
                                </div>
                                {/* Blockchain Score */}
                                <div className="col-12 col-xl-4">
                                    <BlockchainBoxScores scores={qualityScores} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xl-8 col-lg-12">
                                    <Timeline data={mainEvents} />
                                </div>
                                <div className="col-xl-4 col-lg-12">
                                    <WeatherMap
                                        latitude={boxCoordinates.latitude}
                                        longitude={boxCoordinates.longitude} />
                                </div>
                            </div>
                            {isLoading
                                ? t('settings.loading') + "..."
                                : <ResponsiveTable
                                    title={t('blockchain.blockchain log')}
                                    link="/block"
                                    headers={[
                                        { title: t('table headers.log'), field: "log" },
                                        { title: t('table headers.user'), field: "user" },
                                        { title: t('table headers.user type'), field: "userType" },
                                        { title: t('table headers.description'), field: "description" },
                                        { title: t('table headers.date'), field: "timestamp" },
                                        { title: t('table headers.location'), field: "location" },
                                    ]}
                                    data={blockchainLogs} />
                            }
                        </>
                }
            </Translation>
        )
    }
}

export default BlockchainBox

const BoxInfoCard = ({ boxTag, packingDate, logs }) => {
    const { t } = useTranslation();
    return (
        <div className="card">
            <div className="row">
                <div className="col-md-3">
                    <div className="ml-2 mt-3 mb-1">
                        <img className="mb-1" src={Box} width="100%" height="100%" alt="wine label" />
                        <WineAttribute attribute={t('blockchain.box code')} value={boxTag} />
                        <WineAttribute attribute={t('blockchain.packaging date')} value={packingDate} />
                    </div>
                </div>
                <div className="col-md-9 p-3">
                    <Table
                        headers={[
                            { title: t('table headers.type'), field: "type" },
                            { title: t('table headers.tag'), field: "tag" },
                            { title: t('table headers.wine name'), field: "wine" },
                            { title: t('table headers.winery'), field: "winery" },
                            { title: t('table headers.score'), field: "score" },
                        ]}
                        data={logs}
                    />
                </div>
            </div>
        </div>
    )
}

const Table = ({ headers, data }) => {
    const tierIcons = {
        90: "grin-stars",
        80: "grin-wink",
        70: "laugh",
        60: "smile"
    }
    const colors = {
        90: "primary",
        80: "info",
        70: "success",
        60: "warning"
    }
    return (
        <table className="table table-hover mb-0">
            <thead>
                <tr>
                    {headers.map(header => (
                        <th key={header.field} className="border-top-0 text-center">{header.title}</th>
                    ))}
                </tr>
            </thead>
            <tbody>
                {data.map(row => (
                    <tr key={row.id}>
                        {headers.map(header => (
                            <td key={header.field} className="text-truncate text-center">
                                {header.field === "tag"
                                    ? <Link to={`/blockchain/wine?id=${row["tag"]}`}>{row[header.field]}</Link>
                                    : header.field === "type"
                                        ? row["type"] === "bag"
                                            ? <img src={SmartBag} width="29px" height="29px" style={{ "objectFit": "contain" }} alt="smart bag icon" />
                                            : <i className={`las la-${row["type"] === "bottle" ? "wine-bottle" : row["type"] === "box" ? "box" : "pallet"} la-2x`}></i>
                                        : header.field === "score"
                                            ? <i className={`las la-${tierIcons[row.score]} ${colors[row.score]} la-2x`}></i>
                                            : row[header.field]
                                }
                            </td>
                        ))}
                    </tr>
                ))}
            </tbody>
        </table>
    )
}