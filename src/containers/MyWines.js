import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom';
import Card from '../components/Card'
// import ResponsiveWineTable from '../components/Charts/ResponsiveWineTable'
// import { getWines } from '../utils/api'
// import { AppContext } from '../utils/contexts'
import NoWines from '../images/no-wines.png'
import { Button } from '../components/Button'

const MyWines = () => {
    const { t } = useTranslation()
    const history = useHistory()
    // static contextType = AppContext
    // state = {
    //     data: [],
    //     companyId: null,
    //     isLoading: true
    // }

    // componentDidMount() {
    //     this.loadWines()
    // }

    // componentDidUpdate() {
    //     if (this.companyId !== this.context.currentCompany.id) {
    //         this.loadWines()
    //     }
    // }

    // loadWines = () => {
    //     const companyId = this.context.currentCompany.id
    //     getWines(companyId)
    //         .then(data => {
    //             this.setState({
    //                 data,
    //                 companyId,
    //                 isLoading: false
    //             })
    //         })
    //         .catch(error => console.log("There was a problem receiving the data, please contact the customer care. Error: ", error));

    // }

    // Set variables for ease of use
    // let { data, isLoading } = this.state;
    return (
        <>
            <div className="inline-flex flex-col space-y-1 items-start justify-start mb-3">
                {/* Page Title */}
                <p className="w-full h-5 text-base font-bold">{t("wine.my wines")}</p>
                <p className="w-full h-3 text-xs font-light text-gray-600">{t("Wines")}</p>
            </div>
            <Card
                title={t('wine.my wines')}
                description={""}
            >
                <div className="flex flex-col md:flex-row w-full h-full space-y-3 md:space-x-6 lg:space-x-12 items-center justify-center">
                    <img className="max-w-96 max-h-96 rounded-tl-full rounded-tr-3xl rounded-bl-full" src={NoWines} alt="Illustration of hands cheering for wine" />
                    <div className="inline-flex flex-col space-y-2 lg:space-y-6 items-start justify-start">
                        <p className="text-2xl lg:text-4xl font-bold">Ops... non abbiamo trovato vini.</p>
                        <p className="text-base max-w-xs">E’ un ottimo momento per iniziare ad aggiungere vini nella piattaforma.<br />Crea il tuo primo vino per sfruttare al massimo le soluzioni di albicchiere e per farti conoscere dalla nostra wine community!</p>
                        <div className="w-64 h-8">
                            <Button
                                label={"CREA IL TUO PRIMO VINO"}
                                onClick={() => history.push('/wines/new-wine')}
                            />
                        </div>
                    </div>
                </div>
            </Card>
        </>
        // <ResponsiveWineTable
        //     title={t('wine.my wines')}
        //     headers={[
        //         { title: "", field: "year" },
        //         { title: "", field: "img" },
        //         { title: t('wine.package type'), field: "format" },
        //         { title: t('settings.reviews'), field: "review" },
        //         { title: "% Uso", field: "uso" },
        //         { title: t('wine.confirmation status'), field: "state" },
        //         { title: t('wine.sale status'), field: "on_sale" },
        //         { title: t('settings.price'), field: "price" },
        //     ]}
        //     data={data}
        //     isLoading={isLoading}
        //     createWine
        //     withSearch />
    )
}

export default MyWines