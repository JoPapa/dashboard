import React from 'react'
import { Translation } from 'react-i18next'
import { AppContext } from '../utils/contexts'
import DoubleCard from '../components/Charts/DoubleCard'
import Map from '../components/Charts/Map'
import TopFive from '../components/Charts/TopFive'
import BasicPie from '../components/Charts/BasicPie'
import DonutChart from '../components/Charts/DonutChart'
import RosePie from '../components/Charts/RosePie'
import Histogram from '../components/Charts/Histogram'
import TempMedia from '../components/Charts/TempMedia'
import SubscribeModal from '../components/SubscribeModal'
import { getHome } from '../utils/api'

import { Link } from 'react-router-dom'
import { getChartArrayFromObject } from '../utils/helpers'

class DashboardHome extends React.Component {
    static contextType = AppContext
    state = {
        companyId: "",
        isModalOpen: false,
        isLoading: true,
        mostSales: null,
        mapData: null,
        topNations: null,
        fasceEta: null,
        fasceOrarie: null,
        weekData: null,
        stagioni: null,
        selectedMapRegion: null
    }

    toggleModal = (value) => {
        this.setState({ isModalOpen: value })
    }

    setSelectedMapRegion = (countryCode) => {
        console.log("Selected Region", countryCode)
        this.setState({ selectedMapRegion: countryCode })
    }

    colorSeasons = (data) => {
        return data.map(season => {
            const seasonColor = {
                "Inverno": "#6ab0b8",
                "Primavera": "#ffc0cb",
                "Estate": "#c23531",
                "Autunno": "#562b1c"
            }
            return {
                ...season,
                itemStyle: { color: seasonColor[season.name] }
            }
        })
    }

    componentDidMount() {
        this.loadAnalytics()
    }

    componentDidUpdate(prevProps) {
        console.log("PrevProps", prevProps)
        if (this.state.companyId !== this.context.currentCompany.id) {
            this.loadAnalytics()
        }
    }

    loadAnalytics = () => {
        const companyId = this.context.currentCompany.id
        getHome(companyId)
            .then(data => {
                console.log("this home", data)
                this.setState({
                    companyId,
                    mostSales: data.most_sales,
                    mapData: data.map_data,
                    topNations: data.top_nations,
                    fasceEta: data.age_groups,
                    fasceOrarie: data.time_slots,
                    weekData: data.week_data,
                    stagioni: this.colorSeasons(data.seasons),
                    isLoading: false
                })
            })
            .catch(err => console.log("There was an error receiving data", err))
    }

    render() {
        const isUserPaying = false
        const moreDetailsButton = (link) => {
            return (
                <Translation>
                    {(t) =>
                        !isUserPaying
                            ? <button className="btn btn-outline-danger round" onClick={() => this.toggleModal(true)}>
                                <span className="mx-1">{t('settings.more details')}</span>
                                <i className="las la-arrow-right"></i>
                            </button>
                            : <Link to={link} className="btn btn-outline-danger round">
                                <span className="mx-1">{t('settings.more details')}</span>
                                <i className="las la-arrow-right"></i>
                            </Link>
                    }
                </Translation>
            )
        }

        const {
            isModalOpen,
            isLoading,
            mostSales,
            mapData,
            topNations,
            fasceEta,
            fasceOrarie,
            weekData,
            stagioni,
            selectedMapRegion
        } = this.state
        console.log("this state: ", this.state)

        return (
            <Translation>
                {(t) =>
                    isLoading
                        ? <h3>Loading...</h3>
                        : <>
                            {/* First row */}
                            <div className="row">

                                <div className="col-12 col-xl-8">
                                    <div className="d-flex justify-content-between mb-2">
                                        <h3>{t('home.consumers.where')}</h3>
                                        {moreDetailsButton("/dove-sono")}
                                    </div>
                                    <div className="card">
                                        <div className="row">
                                            <div className="col-12 col-md-8 py-4">
                                                <Map
                                                    data={mapData}
                                                    selectedRegion={selectedMapRegion}
                                                    height="410px"
                                                />
                                            </div>
                                            <div className="col-12 col-md-4 pt-3 h-100">
                                                <DonutChart
                                                    onClick={this.setSelectedMapRegion}
                                                    selectedRegion={selectedMapRegion}
                                                    data={[
                                                        {
                                                            "value": 335,
                                                            "name": "Cina"
                                                        },
                                                        {
                                                            "value": 310,
                                                            "name": "USA"
                                                        },
                                                        {
                                                            "value": 234,
                                                            "name": "Italy"
                                                        },
                                                        {
                                                            "value": 135,
                                                            "name": "Spain"
                                                        },
                                                        {
                                                            "value": 1548,
                                                            "name": "France"
                                                        }
                                                    ]} />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-12 col-xl-4">
                                    <TopFive
                                        title={t('charts.top wine sales')}
                                        data={mostSales}
                                    />
                                    <div className="card text-center">
                                        <div className="card-content align-items-center p-1">
                                            <button onClick={() => this.toggleModal(true)} className="btn round btn-danger mt-1 pl-2 btn-glow">{t('home.messages.promoButton').toUpperCase()}<i className="las la-chart-bar mx-1"></i></button>
                                            <p className="card-text h5 my-1">{t('home.messages.promoShort')}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />

                            {/* Second Row */}
                            <div className="d-flex justify-content-between mb-1">
                                <h3>{t('home.consumers.who')}</h3>
                                {moreDetailsButton("/chi-sono")}
                            </div>
                            <div className="row">
                                <div className="col-12 col-xl-3">
                                    <TopFive
                                        title={t('charts.nationalities')}
                                        data={topNations} />
                                </div>
                                <div className="col-12 col-xl-3">
                                    <div className="card">
                                        <div className="card-header">
                                            <h4 className="card-title text-center">{t('charts.age groups')}</h4>
                                        </div>
                                        <div className="card-body">
                                            <BasicPie data={fasceEta} legend />
                                        </div>
                                    </div>
                                </div>

                                <div className="col-12 col-xl-6">
                                    <div className="row">
                                        {/* Gender */}
                                        <div className="col-md-6 col-12">
                                            <DoubleCard
                                                title={t('charts.total users.title')}
                                                left={{
                                                    name: t('charts.total users.male'),
                                                    icon: "la la-mars",
                                                    percentage: "40%"
                                                }}
                                                right={{
                                                    name: t('charts.total users.female'),
                                                    icon: "la la-venus",
                                                    percentage: "60%"
                                                }} />
                                        </div>
                                        <div className="col-md-6 col-12">
                                            <DoubleCard
                                                title={t('charts.consumer type.title')}
                                                left={{
                                                    name: t('charts.consumer type.passion'),
                                                    icon: "la la-graduation-cap",
                                                    percentage: "40%"
                                                }}
                                                right={{
                                                    name: t('charts.consumer type.fun'),
                                                    icon: "la la-glass-cheers",
                                                    percentage: "60%"
                                                }} />
                                        </div>
                                    </div>

                                    <div className="row">
                                        <div className="col-12 my-auto">
                                            <div className="card">
                                                <div className="card-content text-center p-2">
                                                    <div className="h5 my-1 ml-1">{t('home.messages.promoShortest')}</div>
                                                    <button onClick={() => this.toggleModal(true)} className="btn round btn-danger mt-1 pl-2 btn-glow">
                                                        {t('home.messages.promoButton').toUpperCase()}<i className="las la-chart-bar mx-1"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr />

                            {/* Third row */}
                            <div className="d-flex justify-content-between mb-1">
                                <h3>{t('home.consumers.when')}</h3>
                                {moreDetailsButton("/quando")}
                            </div>
                            <div className="card">
                                <div className="row">
                                    <div className="col-lg-4 col-12 border-right-blue-grey border-right-lighten-5">
                                        <div className="card">
                                            <div className="card-header">
                                                <h4 className="card-title text-center">{t('charts.time slots')}</h4>
                                            </div>
                                            <div className="card-body">
                                                <RosePie data={fasceOrarie} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-4 col-12 border-right-blue-grey border-right-lighten-5">
                                        <div className="card">
                                            <div className="card-header">
                                                <h4 className="card-title text-center">{t('charts.weekdays.title')}</h4>
                                            </div>
                                            <div className="card-body">
                                                <Histogram data={getChartArrayFromObject(weekData)} />
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-lg-4 col-12">
                                        <div className="card">
                                            <div className="card-header">
                                                <h4 className="card-title text-center">{t('charts.seasons.title')}</h4>
                                            </div>
                                            <div className="card-body">
                                                <BasicPie data={stagioni} legend />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />

                            {/* Fourth row */}
                            <div className="d-flex justify-content-between mb-1">
                                <h3>{t('home.consumers.how')}</h3>
                            </div>
                            <div className="row">
                                <div className="col-12 col-xl-3">
                                    <div className="row">
                                        {/* Consumption period | Meals, between meals*/}
                                        <div className="col-lg-6 col-xl-12">
                                            <DoubleCard title={t('charts.consumption period.title')}
                                                left={{
                                                    name: t('charts.consumption period.during meals'),
                                                    icon: "la la-cutlery",
                                                    percentage: "82%"
                                                }}
                                                right={{
                                                    name: t('charts.consumption period.between meals'),
                                                    icon: "la la-birthday-cake",
                                                    percentage: "18%"
                                                }} />
                                        </div>

                                        {/* Pouring options */}
                                        <div className="col-lg-6 col-xl-12">
                                            <DoubleCard title={t('charts.pouring options.title')}
                                                left={{
                                                    name: t('charts.pouring options.taste'),
                                                    icon: "la la-wine-glass",
                                                    percentage: "23%"
                                                }}
                                                right={{
                                                    name: t('charts.pouring options.glass'),
                                                    icon: "la la-wine-glass-alt",
                                                    percentage: "77%"
                                                }} />
                                        </div>
                                    </div>
                                </div>

                                {/* Avg. Serving Temperature */}
                                <div className="col-12 col-lg-6 col-xl-3">
                                    <TempMedia
                                        title={t('charts.avarage temperature')}
                                        large={true}
                                        temperatures={{
                                            red: {
                                                average: "18",
                                                maximum: "24",
                                                minimum: "12",
                                            },
                                            white: {
                                                average: "8",
                                                maximum: "18",
                                                minimum: "6",
                                            },
                                            rose: {
                                                average: "12",
                                                maximum: "16",
                                                minimum: "8",
                                            },
                                        }}
                                    />
                                </div>

                                <div className="col-lg-6 col-xl-3">
                                    <div className="row">
                                        {/* Drinking Temperature */}
                                        <div className="col-12">
                                            <DoubleCard title={t('charts.drinking temperature.title')}
                                                left={{
                                                    name: t('charts.drinking temperature.suggested'),
                                                    icon: "la la-thermometer-half",
                                                    percentage: "82%"
                                                }}
                                                right={{
                                                    name: t('charts.drinking temperature.custom'),
                                                    icon: "la la-temperature-high",
                                                    percentage: "18%"
                                                }} />
                                        </div>

                                        {/* Consumption | Alone, with others */}
                                        <div className="col-12">
                                            <DoubleCard
                                                title={t('charts.consumption.title')}
                                                left={{
                                                    name: t('charts.consumption.alone'),
                                                    icon: "la la-user",
                                                    percentage: "40%"
                                                }}
                                                right={{
                                                    name: t('charts.consumption.with others'),
                                                    icon: "la la-users",
                                                    percentage: "60%"
                                                }} />
                                        </div>
                                    </div>
                                </div>

                                {/* Promo section */}
                                <div className="col-12 col-lg-6 col-xl-3">
                                    <div className="card">
                                        <div className="card-body my-3 py-2 text-center align-items-center">
                                            <div className="mx-3 py-3">
                                                <i className="las la-chalkboard la-8x mb-1"></i>
                                                <p className="font-medium-2">{t('home.messages.promoLong')}</p>
                                                <button
                                                    onClick={() => this.toggleModal(true)}
                                                    className="btn round btn-danger mt-1 pl-2 p-1 btn-glow"
                                                >
                                                    {t('home.messages.promoButton').toUpperCase()}
                                                    <i className="las la-chart-bar mx-1"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <SubscribeModal
                                isOpen={isModalOpen}
                                onRequestClose={() => this.toggleModal(false)}
                            />
                        </>
                }
            </Translation>
        )
    }
}

export default DashboardHome