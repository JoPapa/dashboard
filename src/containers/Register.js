import React, { useState } from 'react'
import { Auth } from "aws-amplify"
import { useFormFields } from '../utils/helpers'
import { Link } from 'react-router-dom'
import logo from '../images/logo/logo-dark.png'
import { useAppContext } from "../utils/contexts"
import { setLimitDate } from "../utils/helpers"

// TODO: Add Social login

const Register = (props) => {
    const { currentUser, setCurrentUser } = useAppContext();
    const [signedUp, hasSignedUp] = useState(false)
    const [isLoading, setIsLoading] = useState(false)
    const [showPassword, setShowPassword] = useState(false)

    const [fields, handleFieldChange] = useFormFields({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        birthDate: "",
        confirmationCode: "",
    });

    async function handleSignUp(e) {
        e.preventDefault()
        setIsLoading(true);
        try {
            await Auth.signUp({
                username: fields.email,
                password: fields.password,
                attributes: {
                    given_name: fields.firstName,
                    family_name: fields.lastName,
                    birthdate: fields.birthDate,
                }
            });
            setIsLoading(false);
            hasSignedUp(true)
        } catch (e) {
            alert(e.message);
            console.log("Error on signup: ", e)
            setIsLoading(false);
        }
    }

    async function handleConfirmationSubmit(e) {
        e.preventDefault();
        setIsLoading(true);
        try {
            await Auth.confirmSignUp(fields.email, fields.confirmationCode);
            await Auth.signIn(fields.email, fields.password)
                .then(res =>
                    // set username
                    setCurrentUser({
                        ...currentUser,
                        firstName: res.signInUserSession.idToken.payload.given_name
                    })
                )

            setIsLoading(false);
            props.authenticate(true)
        } catch (e) {
            alert(e.message);
            console.log(e);
            console.log(e.message);
            setIsLoading(false);
        }
    }

    function renderRegisterForm() {
        return (
            <>
                <form className="form-horizontal" onSubmit={handleSignUp}>

                    <p className="card-subtitle line-on-side text-muted text-center font-small-3 m-2"><span>Register with your social accounts</span></p>
                    <div className="text-center">
                        <a href="##" className="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"><span className="la la-facebook"></span></a>
                        <a href="##" className="btn btn-social-icon mr-1 mb-1 btn-outline-google"><span className="la la-google"></span></a>
                        {/* <a href="##" className="btn btn-social-icon mr-1 mb-1 btn-outline-linkedin"><span className="la la-linkedin font-medium-4"></span></a> */}
                        <a href="##" className="btn btn-social-icon mr-1 mb-1 btn-outline-facebook"><span className="la la-amazon font-medium-4"></span></a>
                    </div>

                    <p className="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 mb-2"><span>Or use your account credentials</span></p>
                    <fieldset className="form-group position-relative has-icon-left">
                        <input type="text" className="form-control" placeholder="First name" id="firstName" value={fields.firstName} onChange={handleFieldChange} required />
                        <div className="form-control-position">
                            <i className="las la-user"></i>
                        </div>
                    </fieldset>
                    <fieldset className="form-group position-relative has-icon-left">
                        <input type="text" className="form-control" placeholder="Last name" id="lastName" value={fields.lastName} onChange={handleFieldChange} required />
                        <div className="form-control-position">
                            <i className="las la-user"></i>
                        </div>
                    </fieldset>
                    <fieldset className="form-group position-relative has-icon-left">
                        <input type="text" className="form-control" placeholder="Your email" id="email" value={fields.email} onChange={handleFieldChange} required />
                        <div className="form-control-position">
                            <i className="las la-at"></i>
                        </div>
                    </fieldset>
                    <fieldset className="form-group position-relative">
                        <input type={showPassword ? "text" : "password"} className="form-control pl-3" placeholder="Enter your password" id="password" value={fields.password} onChange={handleFieldChange} required />
                        <div className="has-icon-left">
                            <div className="form-control-position">
                                <i className="las la-key"></i>
                            </div>
                        </div>
                        <div className="form-control-position" onClick={() => setShowPassword(showPassword ? false : true)}>
                            <i className={`las la-${showPassword ? "eye" : "eye-slash"}`}></i>
                        </div>
                    </fieldset>
                    <fieldset className="form-group position-relative">
                        {/* TODO: Validate age based on country, now is 18yrs old */}
                        <p className="font-medium-1 text-center">Confirm that you are older than the legal drinking age of your country</p>
                        <input type="date" className="form-control pl-3" id="birthDate" min={setLimitDate(125)} max={setLimitDate(18)} value={fields.birthDate} onChange={handleFieldChange} required />
                    </fieldset>
                    <button type="submit" className={`btn btn-outline-danger btn-block`} tabIndex="0" autoFocus>{isLoading ? <i className="las la-circle-notch spinner"></i> : <i className="las la-unlock"></i>} Register</button>
                    <p className="font-small-3 text-center mt-1">
                        By clicking "Register", you agree to the <Link to="#">Terms and Conditions</Link> set out by this site.
                        {/* , including our cookie use. */}
                    </p>
                </form >
                <p className="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 mt-3 mb-2"><span>Already have an Albicchiere account?</span></p>
                <Link to={'/login'} className="btn btn-outline-info btn-block"><i className="la la-user"></i> Login</Link>
            </>
        )
    }

    function renderConfirmationForm() {
        return (
            <form className="form-check mt-1" onSubmit={handleConfirmationSubmit}>
                <p className="h5 text-center">We just sent you an email with a confirmation code, type it below</p>
                <fieldset className="form-group position-relative has-icon-left">
                    <input
                        type="tel"
                        className="form-control"
                        placeholder="Enter the code"
                        id="confirmationCode"
                        value={fields.confirmationCode}
                        onChange={handleFieldChange}
                        required
                    />
                    <div className="form-control-position">
                        <i className="las la-lock"></i>
                    </div>
                </fieldset>
                <button type="submit" className="btn btn-outline-danger btn-block" tabIndex="0" autoFocus>{isLoading && <i className="las la-circle-notch spinner"></i>} Confirm Account</button>
            </form>
        )
    }

    return (
        <section className="flexbox-container">
            <div className="col-12 d-flex align-items-center justify-content-center">
                <div className="col-12 col-md-6 col-lg-6 col-xl-4 box-shadow-2 p-0">
                    <div className="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div className="card-header border-0">
                            <div className="card-title text-center">
                                <img src={logo} alt="Albicchiere logo" />
                            </div>
                        </div>
                        <div className="card-content">
                            <div className="card-body">
                                {!signedUp ? renderRegisterForm() : renderConfirmationForm()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section >

    )
}

export default Register