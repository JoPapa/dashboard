import React, { useState } from 'react'
import { Auth } from "aws-amplify"
import { useFormFields } from '../utils/helpers'
import { Link } from 'react-router-dom'
import logo from '../images/logo/logo-dark.png'
import { useAppContext } from "../utils/contexts"
import SignInWithGoogle from '../components/Login/SignInWithGoogle'
import SignInWithFacebook from '../components/Login/SignInWithFacebook'
import SignInWithAmazon from '../components/Login/SignInWithAmazon'
// TODO: Handle Forgot Password situation

function Login(props) {
    const { authenticate, currentUser, setCurrentUser } = useAppContext();
    const [showPassword, setShowPassword] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [fields, handleFieldChange] = useFormFields({
        email: "",
        password: ""
    });

    // SIGN IN
    async function handleSubmit(event) {
        event.preventDefault();
        setIsLoading(true);
        try {
            const res = await Auth.signIn(fields.email, fields.password)
            const payload = res.signInUserSession.idToken.payload
            setCurrentUser({
                ...currentUser,
                firstName: payload.given_name,
                lastName: payload.family_name
            })
            authenticate(true)
        } catch (e) {
            alert("Sign in error: ", e.message);
            console.log("Sign in error: ", e.message);
            setIsLoading(false);
        }
    }

    return (
        <section className="flexbox-container" onSubmit={handleSubmit}>
            <div className="col-12 d-flex align-items-center justify-content-center">
                <div className="col-12 col-md-6 col-lg-6 col-xl-4 box-shadow-2 p-0">
                    <div className="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div className="card-header border-0">
                            <div className="card-title text-center">
                                <img src={logo} alt="Albicchiere logo" />
                            </div>
                            <h6 className="card-subtitle line-on-side text-muted text-center font-small-3 pt-2"><span>Login with your social accounts</span></h6>
                        </div>
                        <div className="card-content">

                            <div className="text-center">
                                <SignInWithFacebook currentUser={currentUser} setCurrentUser={setCurrentUser} authenticate={authenticate} />
                                <SignInWithGoogle currentUser={currentUser} setCurrentUser={setCurrentUser} authenticate={authenticate} />
                                <SignInWithAmazon currentUser={currentUser} setCurrentUser={setCurrentUser} authenticate={authenticate} />
                            </div>

                            <p className="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>Or use your account credentials</span></p>
                            <div className="card-body">
                                <form className="form-horizontal" onSubmit={props.authorize}>
                                    <fieldset className="form-group position-relative has-icon-left">
                                        <input type="email" className="form-control" id="email" placeholder="Your Username" value={fields.email} onChange={handleFieldChange} required />
                                        <div className="form-control-position">
                                            <i className="la la-user"></i>
                                        </div>
                                    </fieldset>
                                    <fieldset className="form-group position-relative">
                                        <input type={showPassword ? "text" : "password"} className="form-control pl-3" id="password" placeholder="Enter Password" value={fields.password} onChange={handleFieldChange} required />
                                        <div className="has-icon-left">
                                            <div className="form-control-position">
                                                <i className="la la-key"></i>
                                            </div>
                                        </div>
                                        <div className="form-control-position" onClick={() => setShowPassword(showPassword ? false : true)}>
                                            <i className={`las la-${showPassword ? "eye" : "eye-slash"}`}></i>
                                        </div>
                                    </fieldset>
                                    <div className="form-group row">
                                        <div className="col-md-6 col-12 text-center text-sm-left">
                                            <fieldset>
                                                <input type="checkbox" id="remember-me" className="chk-remember mr-1" checked />
                                                <label htmlFor="remember-me"> Remember Me</label>
                                            </fieldset>
                                        </div>
                                        <div className="col-md-6 col-12 float-sm-left text-center text-sm-right">
                                            <Link to="/reset" className="card-link">Forgot Password?</Link>
                                        </div>
                                    </div>
                                    <button type="submit" className="btn btn-outline-info btn-block" tabIndex="0" autoFocus>
                                        {isLoading ? <i className="las la-circle-notch spinner"></i> : <i className="las la-unlock"></i>} Login
                                    </button>
                                </form>
                            </div>
                            <p className="card-subtitle line-on-side text-muted text-center font-small-3 mx-2 my-1"><span>New to Albicchiere ?</span></p>
                            <div className="card-body">
                                <Link to={'/register'} className="btn btn-outline-danger btn-block"><i className="la la-user"></i> Register</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Login