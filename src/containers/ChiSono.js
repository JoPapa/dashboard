import React from 'react'
import { useTranslation } from 'react-i18next'
import { withRouter } from 'react-router-dom'
import Map from '../components/Charts/Map'
import DoubleCard from '../components/Charts/DoubleCard'
import TempMedia from '../components/Charts/TempMedia'
import RosePie from '../components/Charts/RosePie'
import BasicPie from '../components/Charts/BasicPie';
import FilterButton from '../components/FilterButton';
import DataPicker from '../components/DataPicker';

const ChiSono = (props) => {
    window.scrollTo(0, 0);
    // grafico Fasce d'eta'
    let fasceEta = [
        { value: 335, name: '18-30' },
        { value: 310, name: '31-45' },
        { value: 234, name: '45-54' },
        { value: 135, name: '55-64' },
        { value: 1548, name: '65+' }
    ];

    const { t } = useTranslation();
    return (
        <>
            {/* Intro Row */}
            <div className="row">
                <div className="col-md-8 col-12">
                    <button className="text-left btn btn-primary m-1" onClick={() => props.history.goBack()}> {"< Back"}</button>
                    <FilterButton />
                </div>
                <div className="col-md-4 col-12">
                    <DataPicker />
                </div>
            </div>

            {/* First Row */}
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <div className="row">
                            <div className="col-md-8 col-12">
                                <Map />
                            </div>
                            <div className="col-md-4 col-12">
                                <div className="row">

                                    <div class="col-md-12">
                                        <DoubleCard title={t('charts.total users.title')}
                                            left={{
                                                name: t('charts.total users.male'),
                                                icon: "la la-mars",
                                                percentage: "40%"
                                            }}
                                            right={{
                                                name: t('charts.total users.female'),
                                                icon: "la la-venus",
                                                percentage: "60%"
                                            }} />
                                    </div>

                                </div>
                                <div className="row">
                                    <div className="col-12">
                                        <h3 className="card-title text-center">{t('charts.age groups')}</h3>
                                        <BasicPie data={fasceEta} legend />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />

            {/* Third Row */}
            <div className="row">

                <div className="col-md-4">
                    <div className="card">
                        <div className="card-header">
                            <h4 className="card-title text-center">{t('charts.time slots')}</h4>
                        </div>
                        <div className="card-body">
                            <RosePie />
                        </div>
                    </div>
                </div>

                <div className="col-md-8">

                    <div className="row">

                        <div className="col-md-6">
                            <DoubleCard title={t('charts.pouring options.title')}
                                left={{
                                    name: t('charts.pouring options.taste'),
                                    icon: "la la-glass",
                                    percentage: "23%"
                                }}
                                right={{
                                    name: t('charts.pouring options.glass'),
                                    icon: "la la-glass",
                                    percentage: "77%"
                                }} />
                        </div>

                        <div className="col-md-6">
                            <DoubleCard title={t('charts.consumption period.title')}
                                left={{
                                    name: t('charts.consumption period.during meals'),
                                    icon: "la la-cutlery",
                                    percentage: "82%"
                                }}
                                right={{
                                    name: t('charts.consumption period.between meals'),
                                    icon: "la la-birthday-cake",
                                    percentage: "18%"
                                }} />
                        </div>

                    </div>

                    <div className="row">

                        <div className="col-md-6">
                            <DoubleCard title={t('charts.consumption.title')}
                                left={{
                                    name: t('charts.consumption.alone'),
                                    icon: "la la-user",
                                    percentage: "45%"
                                }}
                                right={{
                                    name: t('charts.consumption.with others'),
                                    icon: "la la-users",
                                    percentage: "55%"
                                }} />
                        </div>

                        <div className="col-md-6">
                            {/* <TempMedia title="Ciao" value="34" /> */}
                        </div>
                    </div>

                </div>

            </div>
        </>
    )
}

export default withRouter(ChiSono)