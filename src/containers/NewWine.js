import { useTranslation } from "react-i18next"
import Card from "../components/Card"
import IconSelectionItem from "../components/IconSelectionItem"
import { Button } from '../components/Button'
import { useHistory } from "react-router-dom"

import ShoppingBag from '../images/shopping-bag.png'

const TextInput = ({ placeholder, ...props }) => {
    return (
        <input type="text" placeholder={placeholder} {...props}></input>
    )
}

const NumberControlButton = ({ value, color, ...props }) => {
    return (
        <div className={`w-9 h-9 bg-gray-300 hover:bg-gray-400 rounded-xl cursor-pointer select-none ${props.className}`} {...props}>
            <p className="w-full text-2xl font-bold text-center text-white">{value}</p>
        </div>
    )
}

const NumberInput = ({ ...props }) => {
    return (
        <div className="inline-flex space-x-2 items-center justify-start w-full">
            {/* Number Input */}
            <input
                type="number"
                className="flex-1 h-full text-6xl font-bold px-2 py-1"
                {...props}
            />
            {/* Input buttons */}
            <div className="inline-flex flex-col space-y-1 items-center justify-end w-8 h-full pb-0.5">
                <NumberControlButton value={"+"} />
                <NumberControlButton value={"-"} />
            </div>
        </div>
    )
}

const TemperatureInput = ({ value = 18, ...props }) => {
    return (
        <div className="inline-flex flex-col space-y-3 items-center justify-end pb-5">
            <p className="w-28 text-6xl font-bold text-center select-none cursor-pointer">{value}°</p>
            <div className="inline-flex space-x-2 items-center justify-center">
                <div className={`w-9 h-9 bg-blue-500 hover:bg-blue-400 rounded-xl cursor-pointer select-none`} {...props}>
                    <p className="w-full text-2xl font-bold text-center text-white">{"-"}</p>
                </div>
                {/* Temperature Slider */}
                <div className="inline-flex flex-row items-center justify-center space-x-2 h-full">
                    <div className="w-2 h-3 hover:bg-blue-500 bg-black rounded" />
                    <div className="w-2 h-4 hover:bg-blue-500 bg-black rounded" />
                    <div className="w-2 h-5 hover:bg-blue-500 bg-black rounded" />
                    <div className="w-2 h-9 hover:bg-blue-500 bg-black rounded" />
                    <div className="w-2 h-12 hover:bg-blue-500  bg-black rounded" />
                    <div className="w-2 h-16 hover:h-20  bg-black rounded" />
                    <div className="w-2 h-12 hover:bg-pink-700 bg-black rounded" />
                    <div className="w-2 h-9 hover:bg-pink-700 bg-black rounded" />
                    <div className="w-2 h-5 hover:bg-pink-700 bg-black rounded" />
                    <div className="w-2 h-4 hover:bg-pink-700 bg-black rounded" />
                    <div className="w-2 h-3 hover:bg-pink-700 bg-black rounded" />
                </div>
                <div className={`w-9 h-9 bg-pink-700 hover:bg-pink-600 rounded-xl cursor-pointer select-none`}>
                    <p className="w-full text-2xl font-bold text-center text-white">{"+"}</p>
                </div>
            </div>
        </div>
    )
}

// TODO: Think about using compound components
const FormDescriptions = (props) => {
    return (
        <div className={"flex flex-col space-y-3"}>
            <div className="flex flex-col space-y-0.5 items-start justify-start">
                <p className="text-sm">{props.title}</p>
                <p className="text-xs leading-tight text-gray-500">{props.description}</p>
            </div>
            {props.children}
        </div>
    )
}

// const IconSelectionItem = ({ label }) => {
//     return (
//         <button className="inline-flex flex-col space-y-1.5 items-center justify-start w-20 h-28 p-1 pt-5 bg-gray-100 hover:bg-gray-300 rounded-lg cursor-pointer select-none">
//             <div className="w-12 h-12 bg-white rounded-2xl" />
//             <p className="text-xs text-center">{label}</p>
//         </button>
//     )
// }

const philosophies = ["Bio", "Biodynamic", "Vegan", "Natural fermentation"]
const allergens = ["Sulphites", "Eggs", "Milk", "Nickel"]
const moods = ["Party with friends", "Romantic dinner", "Reading with glasses", "Family meal", "Daily glass", "Aperitif", "Special event", "Relaxing evening"]
const foodPairings = ["White meat", "Fish", "Red meat", "Vegetables", "Pasta", "Cheese", "Dessert & fruit", "Pizza"]

const NewWine = () => {
    const { t } = useTranslation()
    const history = useHistory()
    return (
        <>
            <div className="flex flex-col sm:flex-row w-full justify-between md:items-center space-y-3 md:space-y-0 mb-3">
                <div className="inline-flex flex-col space-y-1 items-start justify-start">
                    {/* Page Title */}
                    <p className="w-full h-5 text-base font-bold">{t("Crea nuovo vino")}</p>
                    <p className="w-full h-3 text-xs font-light text-gray-600">{t("Wines > New wine")}</p>
                </div>
                {/* // Actions */}
                <div className="inline-flex space-x-4 items-start justify-start">
                    <button onClick={() => history.push('/wines')} className="flex items-center justify-center w-40 px-4 py-2 bg-pink-700 hover:bg-pink-800 shadow rounded-md">
                        <p className="flex-1 h-full text-sm leading-tight text-center text-gray-100">CANCEL</p>
                    </button>
                    <button disabled={true} className="flex items-center justify-center w-48 px-4 py-2 bg-green-500 hover:bg-green-400 shadow rounded-md">
                        <p className="flex-1 h-full text-sm leading-tight text-center text-gray-100">SAVE CHANGES</p>
                    </button>
                </div>
                {/* <Button label="Salva" /> */}
            </div>

            {/* // General Wine Info */}
            <Card title={t("Wine Information")}>
                <div className="lg:flex items-start justify-center max-w-max mx-auto space-y-6 lg:space-y-0 lg:space-x-12 w-full">
                    {/* Wine info */}
                    <div className="w-full lg:w-1/3 inline-flex flex-col space-y-8">
                        {/* Wine Name */}
                        <FormDescriptions
                            title={t("Choose a name for your wine.")}
                            description={t("Type in your wine’s name, don’t include neither your winery’s name nor the vintage.")}
                        >
                            <TextInput placeholder={t("BIANCO DI TORGIANO")} value={null} />
                        </FormDescriptions>

                        {/* Wine Description */}
                        <FormDescriptions
                            title={t("Can you tell us more about it?")}
                            description={t("We want to know more about this wine. Tell us about its story, what it tastes like, the proper way to drink it, and everything that comes to your mind.")}
                        >
                            <textarea
                                placeholder={t("Your wine description goes here...")}
                                className="text-sm h-32 bg-white border rounded border-gray-300"
                            >
                            </textarea>
                        </FormDescriptions>

                    </div>
                    <div className="sm:flex lg:w-2/3 sm:space-x-12 space-y-6 sm:space-y-0 justify-start">
                        {/* Wine label */}
                        <FormDescriptions
                            title={t("Wine label")}
                            description={"This label will be displayed all across our platform, be sure to upload an high quality picture and with the right size"}
                            className="w-1/2"
                        >
                            <p className="text-xs leading-tight text-gray-800">Accepted file types: jpeg, png, pdf, ai ecc.. <br />Accepted dimensions: 448x272px</p>
                            {/* TODO: Create dropdown-zone */}
                            <div className="flex flex-col space-y-3 items-center justify-center w-40 h-64 bg-gray-300 rounded-md border-gray-200 border-2 border-dashed cursor-pointer">
                                <div className="inline-flex items-center justify-center">
                                    <div className="w-12 h-12 bg-gray-400 rounded-xl"></div>
                                </div>
                                {/* Drag n drop message */}
                                <div className="flex flex-col space-y-2 items-center justify-center w-full h-10">
                                    <p className="w-full h-1/5 text-xs text-center text-gray-800">Drag n Drop here or</p>
                                    <p className="w-full h-1/5 text-xs text-center text-pink-700">Browse</p>
                                </div>
                            </div>
                        </FormDescriptions>
                        {/* Other Images */}
                        <FormDescriptions
                            title={t("Other images")}
                            description={t("Show off your wine with additional photos")}
                        >
                            <p className="text-xs leading-tight text-gray-800">Accepted file types: jpeg, png</p>
                            <div className="inline-flex space-x-2.5 items-start justify-start">
                                <div className="w-20 h-20 bg-gray-300 hover:bg-gray-500 cursor-pointer rounded-xl border-4 border-gray-300" />
                                <div className="w-20 h-20 bg-gray-300 hover:bg-gray-500 cursor-pointer rounded-xl border-4 border-gray-300" />
                            </div>
                            <div className="inline-flex space-x-2.5 items-start justify-start ">
                                <div className="w-20 h-20 bg-gray-300 hover:bg-gray-500 cursor-pointer rounded-xl border-4 border-gray-300" />
                                <div className="w-20 h-20 ">
                                    <div className="inline-flex flex-col items-center justify-center text-center border rounded border-gray-300 hover:bg-gray-400 cursor-pointer" style={{ width: 76, height: 76, }}>
                                        <p className="w-6 text-4xl font-bold text-gray-200" style={{ left: 26, top: 9, }}>+</p>
                                        <p className="w-7 text-sm text-gray-200" style={{ left: 23, top: 47, }}>New</p>
                                    </div>
                                </div>
                            </div>
                        </FormDescriptions>
                    </div>
                </div>
            </Card >

            {/* // Wine details */}
            <Card title={t("Wine Details")}>
                <div className="flex flex-col md:flex-wrap xl:flex-nowrap lg:flex-row justify-between space-y-6 xl:space-y-0 xl:space-x-12 py-4">
                    <div className="flex flex-col md:flex-row lg:flex-col justify-between space-y-3 md:space-y-0 lg:space-y-5">
                        {/* Wine Color */}
                        <FormDescriptions
                            title={t("What color of wine is this?")}
                            description={t("Select the type of your wine")}
                        >
                            <div className="flex flex-row space-x-1">
                                <IconSelectionItem label="Red" />
                                <IconSelectionItem label="White" />
                                <IconSelectionItem label="Rose" />
                            </div>
                            {/* Wine characteristics */}
                            <FormDescriptions
                                title={null}
                                description={t("Does it have more characteristics worth mentioning?")}
                            >
                                <TextInput placeholder={t("Add new types")} value={null} />
                            </FormDescriptions>
                        </FormDescriptions>


                        {/* Phylosophies */}
                        <FormDescriptions title={t("Philosophy")} description={t("Select the ones that apply")}>
                            <div className="inline-flex flex-wrap">
                                {philosophies.map((philosophy, idx) =>
                                    <IconSelectionItem
                                        key={idx}
                                        label={philosophy}
                                    // icon={""}
                                    />
                                )}
                            </div>
                        </FormDescriptions>
                    </div>
                    <div className="flex flex-col xl:flex-col justify-between xl:justify-start space-y-3 md:space-y-0 xl:space-y-5 lg:mx-auto">
                        {/* Grapes */}
                        <FormDescriptions
                            title={t("Grapes")}
                            description={t("Type everything that applies")}
                        >
                            <TextInput placeholder={t("Add new grapes")} value={null} />
                        </FormDescriptions>

                        {/* Region */}
                        <FormDescriptions
                            title={t("Region")}
                            description={t("Type in the name of the region")}
                        >
                            <TextInput placeholder={t("Add new region")} value={null} />
                        </FormDescriptions>

                        {/* Allergens */}
                        <FormDescriptions title={t("Is there any allergen?")} description={t("Select the ones that apply")}>
                            <div className="inline-flex flex-wrap">
                                {allergens.map((allergen, idx) =>
                                    <IconSelectionItem key={idx} label={allergen} />
                                )}
                            </div>
                        </FormDescriptions>
                    </div>

                    <div className="flex flex-col justify-between space-y-5">
                        <h3 className="text-xl font-bold">{t("User Suggestions")}</h3>
                        {/* Moods */}
                        <FormDescriptions title={t("What’s the wine mood?")} description={t("Wine may be best suited for specific situations, whether it is a party with friends or a dinner at home. Select the ones that sound appropriate for this specific wine.")}>
                            <div className="inline-flex flex-wrap">
                                {moods.map((moods, idx) =>
                                    <IconSelectionItem key={idx} label={moods} />
                                )}
                            </div>
                        </FormDescriptions>

                        {/* foodPairings */}
                        <FormDescriptions title={t("Which food goes best with it?")} description={t("People love drinking wine while eating food. Why not help them enjoy this wine at its best with recommendations on pairings? Select the ones that applies.")}>
                            <div className="inline-flex flex-wrap">
                                {foodPairings.map((food, idx) =>
                                    <IconSelectionItem key={idx} label={food} />
                                )}
                            </div>
                        </FormDescriptions>
                    </div>
                </div>
            </Card>

            {/* // Vintage details */}
            <Card title={t("Vintage Details")}>
                <div className="inline-flex flex-col lg:flex-row space-y-8 lg:space-y-0 lg:space-x-12 lg:justify-between lg:items-start py-5">
                    <div className="inline-flex flex-col space-y-6">
                        <div className="inline-flex flex-col lg:flex-row space-y-3 lg:space-y-0 lg:space-x-12">
                            {/* Vintage Year */}
                            <FormDescriptions title={"What is the vintage?"} description={"Type in the wine’s year"}>
                                <NumberInput
                                    placeholder={2019}
                                    min={1900}
                                    max={new Date(Date.now()).getFullYear()}
                                />
                            </FormDescriptions>
                            {/* Alcohol Content */}
                            <FormDescriptions title={"Alcohol content"} description={"Select the alcohol content"}>
                                <NumberInput
                                    placeholder={18}
                                    min={0}
                                    max={100}
                                />
                            </FormDescriptions>
                        </div>

                        {/* Temperature Input */}
                        <div className="inline-flex flex-col-reverse md:flex-row items-center md:items-start justify-center md:space-x-4 bg-gray-100 rounded-lg py-6 px-5 mx-auto">
                            <FormDescriptions title={"Do you have a recommended temperature?"} description={"Drinking wine at the right temperature will make it more likeable and increase your brand perception. Suggest the best serving temperature to fully enjoy all the flavors and aromas of this wine."} />
                            <TemperatureInput value={18} />
                        </div>
                    </div>
                    <div className="flex flex-col justify-between w-full">

                        {/* Special Vintage */}
                        <FormDescriptions
                            title={t("Unique details, what makes this wine different? (Optional)")}
                            description={t("This field make this wine different from others of the same vintage, e.g. “Christmas Edition”")}
                        >
                            <TextInput placeholder={t("e.g. “Special Edition”")} value={null} />
                        </FormDescriptions>
                    </div>
                </div>

            </Card>

            <Card
                title={t('wine.my wines')}
                description={""}
            >
                <div className="flex flex-col md:flex-row w-full h-full space-y-3 md:space-x-6 lg:space-x-12 items-center justify-center">
                    <img className="max-w-96 max-h-96" src={ShoppingBag} alt="Illustration of hands cheering for wine" />
                    <div className="inline-flex flex-col space-y-4 lg:space-y-6 items-center md:items-start justify-start px-1 text-center md:text-left">
                        <p className="text-2xl lg:text-4xl font-bold">Do you want to add your wine in the Albicchiere marketplace?</p>
                        <p className="text-base max-w-lg">Add a variant to promote your wine in the Albicchiere wine lover community and acquire new customers.</p>
                        <div className="w-64 h-8">
                            <Button
                                label={"CREA LA PRIMA VARIANTE"}
                                disabled
                            />
                        </div>
                    </div>
                </div>
            </Card>

        </>
    )
}

export default NewWine