import React, { useState } from 'react'
import { useAppContext } from "../utils/contexts"
import { useFormFields, setLimitDate } from '../utils/helpers'
import { useTranslation } from 'react-i18next';
import UploadLogo from '../components/UploadLogo';
import Avatar from '../images/user-avatar.png'
import ConfirmModal from '../components/ConfirmModal'
import AlbiBlack from '../images/logo/Albi-Logo-Black.png'
import AlbiWhite from '../images/logo/Albi-Logo-White.png'

const Settings = () => {
    const { t, i18n } = useTranslation();
    // Navigation
    const [activeLink, setActiveLink] = useState('user')
    // Modals
    const [isDeletingAccount, setIsDeletingAccount] = useState(false)
    // Form
    const { currentUser, setCurrentUser } = useAppContext();
    console.log("user on settings => ", currentUser)
    const [isRemovingCompany, setIsRemovingCompany] = useState()
    const [avatar, changeAvatar] = useState(true)
    const [fields, handleFieldChange] = useFormFields({ ...currentUser });
    // TODO Change language based on currentUser data not from the following variable
    const [selectedLanguage, setSelectedLanguage] = useState('it-IT' || fields.language.toLowerCase())

    React.useEffect(() => {
        console.log("Sion", fields)
    })
    const deleteAccount = () => {
        // TODO: Delete account
        alert("You are not allowed to delete this account.\nContact us for more information")
    }

    const removeCompany = (id) => {
        setCurrentUser({
            ...currentUser,
            linkedCompanies: currentUser.linkedCompanies.filter(company => company.id !== id)
        })
        // TODO: Remove company from this account
        alert("You are not allowed to remove this company from this account.\nContact us for more information")
    }

    const userForm = () => {
        return (
            <form onSubmit={(e) => {
                e.preventDefault()
                alert("Your are not allowed to save your account data yet!")
            }}>
                <div className="row">
                    <div className="col-sm-4 text-center">
                        {!avatar
                            ? <>
                                <UploadLogo circular />
                                <div className="mt-1">
                                    <button type="button" onClick={() => changeAvatar(true)} className="info mx-1">{t('settings.cancel')}</button>
                                    <button type="button" onClick={() => changeAvatar(true)} className="info mx-1">{t('settings.upload')}</button>
                                </div>
                            </>
                            : <div className="mt-1">
                                <img src={fields.picture || Avatar} alt="avatar" />
                                <div className="mt-1">
                                    <button type="button" onClick={() => changeAvatar(false)} className="info">{t('settings.change avatar')}</button>
                                </div>
                            </div>
                        }
                    </div>
                    <div className="col-sm-8">
                        <fieldset className="form-group position-relative">
                            <label htmlFor="name">{t('settings.first name')}</label>
                            <input type="text" className="form-control" id="name" placeholder={t('settings.first name')} value={fields.firstName} onChange={handleFieldChange} required />
                        </fieldset>
                        <fieldset className="form-group position-relative">
                            <label htmlFor="lastname">{t('settings.surname')}</label>
                            <input type="text" className="form-control" id="lastname" placeholder={t('settings.surname')} value={fields.lastName} onChange={handleFieldChange} required />
                        </fieldset>
                    </div>

                </div>
                <fieldset className="form-group position-relative">
                    <div className="my-1">{t('account settings.gender')}</div>
                    <input type="radio" id="gender" className="chk-remember mr-1" value="male" checked={fields.gender === "male"} onChange={handleFieldChange} />
                    <label htmlFor="gender"> Male</label>
                    <input type="radio" id="gender" className="chk-remember mx-1" value="female" checked={fields.gender === "female"} onChange={handleFieldChange} />
                    <label htmlFor="gender"> Female</label>
                </fieldset>
                <fieldset className="form-group position-relative">
                    <label htmlFor="nationality">{t('account settings.nationality')}</label>
                    <input type="text" className="form-control" id="nationality" placeholder={t('account settings.nationality')} value={fields.nationality} onChange={handleFieldChange} required />
                </fieldset>
                <fieldset className="form-group position-relative">
                    <label htmlFor="email">{t('settings.email')}</label>
                    <input type="email" className="form-control" id="email" placeholder={t('settings.email')} value={fields.email} onChange={handleFieldChange} required />
                </fieldset>
                <fieldset className="form-group position-relative">
                    <label htmlFor="date">{t('settings.age')}</label>
                    <input type="date" className="form-control" id="birthdate"
                        onChange={handleFieldChange}
                        min={setLimitDate(125)}
                        max={setLimitDate(18)}
                        required
                    />
                </fieldset>
                <button type="submit" className="btn btn-outline-info btn-block" tabIndex="0" autoFocus>{t('settings.save')}</button>
            </form>
        )
    }

    const languageForm = () => {
        return (
            <form onSubmit={(e) => {
                e.preventDefault()
                setCurrentUser({
                    ...currentUser,
                    language: selectedLanguage.toLowerCase()
                })
                i18n.changeLanguage(selectedLanguage.toLowerCase())
                setCurrentUser({
                    ...currentUser,
                    ...fields
                })
                alert("Your settings have been changed!")
            }}>
                <fieldset className="form-group position-relative mb-0">
                    <div className="mb-1">Language</div>
                    <button type="button" className={`btn btn-${selectedLanguage === 'en' && 'info'} border border-light mr-1`} onClick={() => setSelectedLanguage('en')}>
                        <i className="flag-icon flag-icon-gb"></i>
                        <span className="pl-1">UK</span>
                    </button>
                    <button type="button" className={`btn btn-${selectedLanguage === 'it' && 'info'} border border-light mr-1`} onClick={() => setSelectedLanguage('it')}>
                        <i className="flag-icon flag-icon-it"></i>
                        <span className="pl-1">IT</span>
                    </button>
                    <button type="button" className={`btn btn-${selectedLanguage === 'es' && 'info'} border border-light mr-1`} onClick={() => setSelectedLanguage('es')}>
                        <i className="flag-icon flag-icon-es"></i>
                        <span className="pl-1">ES</span>
                    </button>
                    <button type="button" className={`btn btn-${selectedLanguage === 'fr' && 'info'} border border-light mr-1`} onClick={() => setSelectedLanguage('fr')}>
                        <i className="flag-icon flag-icon-fr"></i>
                        <span className="pl-1">FR</span>
                    </button>
                </fieldset>
                <fieldset className="form-group position-relative mb-0">
                    <div className="my-1">{t('account settings.measure system')}</div>
                    <input type="radio" id="measureSystem" className="chk-remember mr-1" value="metric" checked={fields.measureSystem === "metric"} onChange={handleFieldChange} />
                    <label htmlFor="measureSystem">{t('account settings.metric')} ( ml / °C )</label>
                    <input type="radio" id="measureSystem" className="chk-remember mx-1" value="imperial" checked={fields.measureSystem === "imperial"} onChange={handleFieldChange} />
                    <label htmlFor="measureSystem">{t('account settings.imperial')} ( oz / °F)</label>
                </fieldset>
                <fieldset className="form-group position-relative mb-0">
                    <div className="my-1">{t('account settings.date format')}</div>
                    <input type="radio" id="dateFormat" className="chk-remember mr-1" value="dayFirst" checked={fields.dateFormat === "dayFirst"} onChange={handleFieldChange} />
                    <label htmlFor="dateFormat"> DD/MM/YYYY</label>
                    <input type="radio" id="dateFormat" className="chk-remember mx-1" value="monthFirst" checked={fields.dateFormat === "monthFirst"} onChange={handleFieldChange} />
                    <label htmlFor="dateFormat"> MM/DD/YYYY</label>
                </fieldset>
                <fieldset className="form-group position-relative mb-0">
                    <div className="my-1">{t('account settings.hour format')}</div>
                    <input type="radio" id="hourFormat" className="chk-remember mr-1" value="12h" checked={fields.hourFormat === "12h"} onChange={handleFieldChange} />
                    <label htmlFor="hourFormat"> 12h</label>
                    <input type="radio" id="hourFormat" className="chk-remember mx-1" value="24h" checked={fields.hourFormat === "24h"} onChange={handleFieldChange} />
                    <label htmlFor="hourFormat"> 24h</label>
                </fieldset>
                <fieldset className="form-group position-relative">
                    <div className="my-1">{t('account settings.week.first day of the week')}</div>
                    <select className="form-control col-12 col-lg-4" id="firstDayOfWeek" value={fields.firstDayOfWeek} onChange={handleFieldChange}>
                        <option value="monday">{t('account settings.week.monday')}</option>
                        <option value="saturday">{t('account settings.week.saturday')}</option>
                        <option value="sunday">{t('account settings.week.sunday')}</option>
                    </select>
                </fieldset>
                <button type="submit" className="btn btn-outline-info btn-block" tabIndex="0" autoFocus>{t('settings.save')}</button>
            </form>
        )
    }

    const privacyForm = () => {
        return (
            <form onSubmit={(e) => {
                e.preventDefault()
                alert("Your are not allowed to save your account data yet!")
            }}>
                <fieldset>
                    <div className="my-1">{t('account settings.subscribe to newsletter')}</div>
                    <input type="radio" id="newsletter" className="chk-remember mr-1" value="yes" checked={fields.newsletter === "yes"} onChange={handleFieldChange} />
                    <label htmlFor="newsletter">{t('account settings.allow')}</label>
                    <input type="radio" id="newsletter" className="chk-remember mx-1" value="no" checked={fields.newsletter === "no"} onChange={handleFieldChange} />
                    <label htmlFor="newsletter">{t('account settings.reject')}</label>
                </fieldset>
                <fieldset>
                    <div className="my-1">{t('account settings.subscribe to dashboard newsletter')}</div>
                    <input type="radio" id="newsletterDashboard" className="chk-remember mr-1" value="yes" checked={fields.newsletterDashboard === "yes"} onChange={handleFieldChange} />
                    <label htmlFor="newsletterDashboard">{t('account settings.allow')}</label>
                    <input type="radio" id="newsletterDashboard" className="chk-remember mx-1" value="no" checked={fields.newsletterDashboard === "no"} onChange={handleFieldChange} />
                    <label htmlFor="newsletterDashboard">{t('account settings.reject')}</label>
                </fieldset>
                <fieldset>
                    <div className="my-1">{t('account settings.email notification')}</div>
                    <input type="radio" id="notifications" className="chk-remember mr-1" value="yes" checked={fields.notifications === "yes"} onChange={handleFieldChange} />
                    <label htmlFor="notifications">{t('account settings.allow')}</label>
                    <input type="radio" id="notifications" className="chk-remember mx-1" value="no" checked={fields.notifications === "no"} onChange={handleFieldChange} />
                    <label htmlFor="notifications">{t('account settings.reject')}</label>
                </fieldset>
                <button type="submit" className="btn btn-outline-info btn-block mt-1" tabIndex="0" autoFocus>{t('settings.save')}</button>
            </form>
        )
    }

    const accountSettingsForm = () => {
        return (
            <form onSubmit={(e) => {
                e.preventDefault()
                alert("Your are not allowed to save your account data yet!")
            }}>
                <h5>{t('account settings.paired companies')}</h5>
                <button type="button" className="btn btn-outline-info round mt-1 mr-1" onClick={() => alert("This action is not allowed yet")}>
                    <i className="las la-plus"></i>
                    {t('account settings.add new winery')}
                </button>
                {currentUser.linkedCompanies.map((company, i) => (
                    <div key={i} className="d-flex px-1 pt-1">
                        {/* TODO: Delete mock images */}
                        <img className="d-block img-thumbnail p-0 mr-1" src={i % 2 ? AlbiWhite : AlbiBlack} alt={company.companyName} width="40px" style={{ "borderRadius": "25px" }} />
                        <div>
                            <div className="h5 align-middle mb-0">{company.companyName}</div>
                            <button type="button" className="btn danger p-0" onClick={() => setIsRemovingCompany(company.id)}>{t('settings.remove')}</button>
                        </div>
                        <hr />
                    </div>
                ))}
                {/* <button type="submit" className="btn btn-outline-info btn-block mt-1" tabIndex="0" autoFocus>{t('settings.save')}</button> */}
                <ConfirmModal
                    isOpen={isRemovingCompany}
                    onRequestClose={() => setIsRemovingCompany('')}
                    onSave={() => removeCompany(isRemovingCompany)}
                    message={t('account settings.confirm remove company')}
                />
            </form>
        )
    }

    const securityForm = () => {
        return (
            <form onSubmit={(e) => {
                e.preventDefault()
                alert("Your are not allowed to save your account data yet!")
            }}>
                <fieldset className="form-group position-relative">
                    <label htmlFor="password">{t('account settings.old password')}</label>
                    <input type="password" className="form-control" id="password" placeholder="Your old password" value={fields.oldPassword} onChange={handleFieldChange} required />
                </fieldset>
                <fieldset className="form-group position-relative">
                    <label htmlFor="password">{t('account settings.new password')}</label>
                    <input type="password" className="form-control" id="password" placeholder="Your new password" value={fields.newPassword} onChange={handleFieldChange} required />
                </fieldset>
                <fieldset className="form-group position-relative">
                    <label htmlFor="password">{t('account settings.confirm password')}</label>
                    <input type="password" className="form-control" id="password" placeholder="Confirm your new password" value={fields.confirmNewPassword} onChange={handleFieldChange} required />
                </fieldset>
                <button type="submit" className="btn btn-outline-info btn-block mt-1" tabIndex="0" autoFocus>{t('settings.save')}</button>
                <button type="button" className="btn btn-outline-danger btn-block mt-1" tabIndex="0" autoFocus onClick={() => setIsDeletingAccount(true)}>{t('account settings.delete account')}</button>
                <ConfirmModal
                    isOpen={isDeletingAccount}
                    onRequestClose={() => setIsDeletingAccount(false)}
                    onSave={() => deleteAccount()}
                    message={t('account settings.confirm delete account')}
                />
            </form>
        )
    }

    return (
        <div className="">
            <h2>{t('account settings.title')}</h2>
            <div className="row">
                <div className="col-12 col-lg-4">
                    <SettingsNavigation onClick={setActiveLink} active={activeLink} />
                </div>
                <div className="col-12 col-lg-8">
                    <div className="card">
                        <div className="card-header">
                            <div className="card-title text-uppercase font-weight-bold">
                                {activeLink === "user" && t('account settings.user')}
                                {activeLink === "language" && t('account settings.language')}
                                {activeLink === "privacy" && t('account settings.privacy')}
                                {activeLink === "account" && t('account settings.account')}
                                {activeLink === "security" && t('account settings.security')}
                            </div>
                        </div>
                        <div className="card-body pt-0">
                            {activeLink === "user" && userForm()}
                            {activeLink === "language" && languageForm()}
                            {activeLink === "privacy" && privacyForm()}
                            {activeLink === "account" && accountSettingsForm()}
                            {activeLink === "security" && securityForm()}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default Settings

const SettingsNavigation = ({ onClick, active }) => {
    const { t } = useTranslation();
    return (
        <nav className="my-2 font-medium-2">
            <div>
                <button onClick={() => onClick('user')} className={`d-flex btn ${active === 'user' && 'bg-white'}  p-1 mb-1`}>
                    <i className="la la-user mr-1"></i>
                    {t('account settings.user')}
                </button>
            </div>
            <div>
                <button onClick={() => onClick('language')} className={`d-flex btn ${active === 'language' && 'bg-white'} rounded p-1 mb-1`}>
                    <i className="la la-sms mr-1"></i>
                    {t('account settings.language')}
                </button>
            </div>
            <div>
                <button onClick={() => onClick('privacy')} className={`d-flex btn ${active === 'privacy' && 'bg-white'} rounded p-1 mb-1`}>
                    <i className="la la-universal-access mr-1"></i>
                    {t('account settings.privacy')}
                </button>
            </div>
            <div>
                <button onClick={() => onClick('account')} className={`d-flex btn ${active === 'account' && 'bg-white'} rounded p-1 mb-1`}>
                    <i className="la la-exclamation-circle mr-1"></i>
                    {t('account settings.account')}
                </button>
            </div>
            <div>
                <button onClick={() => onClick('security')} className={`d-flex btn ${active === 'security' && 'bg-white'} rounded p-1 mb-1`}>
                    <i className="la la-lock mr-1"></i>
                    {t('account settings.security')}
                </button>
            </div>
        </nav>
    )
}