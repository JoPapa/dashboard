import React, { Component } from 'react'
import { Translation, useTranslation } from 'react-i18next';
import ResponsiveTable from '../components/Charts/ResponsiveTable'
import CompanyInfo from '../components/CompanyInfo';
import DeviceCard from '../components/DeviceCard';
import OrderDetailsModal from '../components/OrderDetailsModal';
import SubscriptionCard from '../components/SubscriptionCard';
import UserTable from '../components/Charts/UserTable';
import { getCompanyInfo, getCompanyLogs, getCompanyOrders } from '../utils/api';
import { AppContext } from '../utils/contexts'


class MyWines extends Component {
    static contextType = AppContext
    state = {
        // Per avere i field all'interno dell'array 'headers' bisogna conoscere l'oggetto JSON o JS
        isLoading: true,
        activeLink: 'organization',
        orderDetailsOpen: false,
        headers: [
            { userId: "0001", name: "Mario", surname: "Rossi", field: "Proprietario" },
            { userId: "0002", name: "Tizi", surname: "ana", field: "Gestore" }
        ],
        companyInfo: {

        },
        data: [
            {
                userId: "0001",
                user: "email@email.com",
                field: true,
                role_1: true,
                role_2: true,
                role_3: true,
                role_4: true,
                role_5: true
            },
            {
                userId: "0002",
                user: "email@email.com",
                field: false,
                role_1: true,
                role_2: true,
                role_3: false,
                role_4: true,
                role_5: false
            }
        ],
        logData: [],
        dispenserData: [],
        orderData: [],
        devices: [
            { "title": "My Dispenser", "model": "Albi Home M" },
            // { "title": "Room123", "model": "Albi Home M" },
            // { "title": "Room145", "model": "Albi Home M" },
            // { "title": "Sala Rossa", "model": "Albi Home M" },
            // { "title": "Restaurant 1", "model": "Albi Home M" },
            // { "title": "Room122", "model": "Albi Home M" },
            { "title": "Room132", "model": "Albi Home M" }
        ],
        apps: [
            { "title": "Albi Pro App", "model": "Free" }
        ],
        subscriptions: []
    }

    componentDidMount() {
        this.loadCompanyInfo()
    }

    componentDidUpdate() {
        if (this.state.companyInfo.id !== this.context.currentCompany.id) {
            this.loadCompanyInfo()
        }
    }

    loadCompanyInfo = () => {
        const companyId = this.context.currentCompany.id
        getCompanyInfo(companyId)
            .then(data => {
                console.log("this company", data)
                this.setState({
                    ...this.state,
                    companyInfo: {
                        ...this.state.companyInfo,
                        "id": data.id,
                        "createdAt": data.createdAt,
                        "brandName": data.brandName,
                        "companyName": data.companyName,
                        "logo": data.logo,
                        "phoneNumber": data.phoneNumber,
                        "vatNumber": data.vatNumber,
                        "website": data.website,
                        "email": data.email,
                        "subscriptions": data.subscriptions,
                        "headquarters": data.headquarters,
                        "billingAddress": data.billingAddress,
                        "shippingAddress": data.shippingAddress,
                        "addresses": data.addresses
                    },
                    isLoading: false,
                })
                console.log("this company now", this.state.companyInfo)
            })
            .catch(err => console.log("There was an error receiving data", err))
        getCompanyLogs(this.context.currentCompany.id)
            .then(data => {
                console.log("£ERF", data);
                this.setState({
                    ...this.state,
                    logData: data.operationalLog,
                    dispenserData: data.accessLog
                })
            })
            .catch(err => console.log("There was an error receiving data", err))
        getCompanyOrders(this.context.currentCompany.id)
            .then(data => {
                console.log("£ERF", data);
                this.setState({
                    ...this.state,
                    subscriptions: data.subscriptions,
                    orderData: data.orders
                })
            }
            )
            .catch(err => console.log("There was an error receiving data", err))
    }

    setActiveLink = (link) => {
        this.setState({ activeLink: link })
    }

    toggleOrderDetails = () => {
        this.setState({ orderDetailsOpen: !this.state.orderDetailsOpen })
    }

    removeDevice = (title) => {
        // TODO: We should change this to use ID instead of the title
        const newDeviceList = this.state.devices.filter(device => device.title !== title)
        this.setState({
            devices: newDeviceList
        })
        console.log(`Device ${title} removed from your account`)
        alert(`Device ${title} removed from your account`)
    }
    removeApp = (title) => {
        // TODO: We should change this to use ID instead of the title
        const newDeviceList = this.state.apps.filter(app => app.title !== title)
        this.setState({
            apps: newDeviceList
        })
        console.log(`App ${title} removed from your account`)
        alert(`App ${title} removed from your account`)
    }
    removesubscription = (title) => {
        // TODO: We should change this to use ID instead of the title
        const newDeviceList = this.state.subscriptions.filter(subscription => subscription.subscriptionCode !== title)
        this.setState({
            subscriptions: newDeviceList
        })
        console.log(`App ${title} removed from your account`)
        alert(`App ${title} removed from your account`)
    }

    renderDevices(devices) {
        return (
            <>
                <h4 className="my-1 ml-1">Your Devices</h4>
                {devices.length > 0
                    ? <div className="row">
                        {devices.map((device, i) => (
                            <div className="col-12 col-sm-6 col-lg-3">
                                <DeviceCard title={device.title} model={device.model} onRemove={() => this.removeDevice(device.title)} />
                            </div>
                        ))}
                    </div>
                    : <div className="card text-center m-3 font-medium-4 p-4">There are no devices connected to this account</div>
                }
            </>
        )
    }
    renderApps(apps) {
        return (
            <>
                <h4 className="my-1 ml-1">Your Apps</h4>
                {
                    apps.length > 0
                        ? <div className="row">
                            {apps.map((app, i) => (
                                <div className="col-12 col-sm-6 col-lg-3">
                                    <DeviceCard app title={app.title} model={app.model} onRemove={() => this.removeApp(app.title)} />
                                </div>
                            ))}
                        </div>
                        : <div className="card text-center m-3 font-medium-4 p-4">There are no apps connected to this account</div>
                }
            </>
        )
    }

    renderSubscriptions(subscriptions) {
        const notSubscribed = ["albi_pro", "albi_blockchain"].filter(element => subscriptions.length < 1 ? element : subscriptions.some(subscription => subscription.subscriptionCode !== element))
        console.log(notSubscribed)
        return (
            <Translation>
                {(t) =>
                    <>
                        <h4 className="my-1 ml-1">{t('admin.your subscriptions')}</h4>
                        <div className="row">
                            {/* Active subscriptions */}
                            {subscriptions &&
                                subscriptions.map(subscription =>
                                    <div key={subscription.subscriptionCode} className="col-12 col-sm-6 col-lg-3">
                                        <SubscriptionCard
                                            title={subscription.subscriptionCode === "albi_pro" ? "Albi Dashboard PRO" : subscription.subscriptionCode === "albi_blockchain" ? "Albi Blockchain" : ''}
                                            subtitle={`${t('admin.expires')} ${subscription.expiringDate}`}
                                            onRemove={() => this.removesubscription(subscription.subscriptionCode)}
                                            onRenew={() => alert("This function is not available at this moment")}
                                        />
                                    </div>
                                )
                            }
                            {/* Not active subscriptions */}
                            {notSubscribed &&
                                notSubscribed.map(subscription =>
                                    <div key={subscription} className="col-12 col-sm-6 col-lg-3">
                                        <SubscriptionCard
                                            title={subscription === "albi_pro" ? "Albi Dashboard PRO" : subscription === "albi_blockchain" ? "Albi Blockchain" : ''}
                                            subtitle={subscription.toLowerCase() === "blockchain" ? t('admin.blockchain description') : t('admin.pro description')}
                                            onActivate={() => alert("This function is not available at this moment")}
                                        />
                                    </div>
                                )
                            }
                        </div>
                    </>
                }
            </Translation>
        )
    }

    render() {
        // Set variables for ease of use
        let { isLoading, activeLink, companyInfo, headers, data, logData, dispenserData, orderData, subscriptions, orderDetailsOpen } = this.state;

        return (
            <Translation>
                {(t) =>
                    isLoading
                        ? <h3>{t('settings.loading')}...</h3>
                        : <>
                            <AdminNavigation onClick={this.setActiveLink} active={activeLink} />
                            {activeLink === 'organization' &&
                                <>
                                    <CompanyInfo company={companyInfo} />
                                    <UserTable
                                        title={t('admin.manage your users')}
                                        headers={headers}
                                        data={data} />
                                </>
                            }
                            {activeLink === 'logs' &&
                                <>
                                    {/* {this.renderApps(apps)}
                                        {this.renderDevices(devices)} */}
                                    <ResponsiveTable
                                        filter={true}
                                        title={t('admin.operations log')}
                                        headers={[
                                            { title: t('table headers.log'), field: "type" },
                                            { title: t('table headers.user'), field: "user" },
                                            { title: t('table headers.description'), field: "action" },
                                            { title: t('table headers.location'), field: "location" },
                                            { title: t('table headers.date'), field: "timestamp" },
                                            { title: t('table headers.time'), field: "hour" }
                                        ]}
                                        data={logData} />
                                    <ResponsiveTable
                                        filter={true}
                                        title={t('admin.access log')}
                                        headers={[
                                            { title: t('table headers.user'), field: "user" },
                                            { title: t('table headers.device'), field: "device" },
                                            { title: t('table headers.description'), field: "action" },
                                            { title: t('table headers.location'), field: "location" },
                                            { title: t('table headers.date'), field: "timestamp" },
                                            { title: t('table headers.time'), field: "hour" }
                                        ]}
                                        data={dispenserData} />
                                </>
                            }
                            {activeLink === 'subscriptions' &&
                                <>
                                    {this.renderSubscriptions(subscriptions)}
                                    <ResponsiveTable
                                        filter={true}
                                        title={t('admin.shop orders')}
                                        headers={[
                                            { title: t('table headers.user'), field: "user" },
                                            { title: t('table headers.quantity'), field: "quantity" },
                                            { title: t('table headers.item'), field: "item" },
                                            { title: t('settings.price'), field: "price" },
                                            { title: t('table headers.status'), field: "status" },
                                            { title: t('table headers.address'), field: "address" },
                                            { title: t('table headers.date'), field: "timestamp" },
                                            { title: t('table headers.time'), field: "hour" }
                                        ]}
                                        data={orderData}
                                        details={() => this.toggleOrderDetails()}
                                    />
                                    <OrderDetailsModal isOpen={orderDetailsOpen} onRequestClose={() => this.toggleOrderDetails(false)} />
                                </>
                            }
                        </>
                }
            </Translation>
        )
    }
}

export default MyWines

const AdminNavigation = ({ onClick, active }) => {
    const { t } = useTranslation();
    return (
        <nav className="d-flex mb-1 font-medium-2">
            <button onClick={() => onClick('organization')} className={`${active === 'organization' && 'd-flex btn-float bg-white rounded'} p-1`}>
                <i className="la la-campground mr-1"></i>
                {t('admin.organization')}
            </button>
            <button onClick={() => onClick('logs')} className={`${active === 'logs' && 'd-flex btn-float bg-white rounded'} p-1`}>
                <i className="la la-sms mr-1"></i>
                {t('admin.access and activity control')}
            </button>
            <button onClick={() => onClick('subscriptions')} className={`${active === 'subscriptions' && 'd-flex btn-float bg-white rounded'} p-1`}>
                <i className="la la-universal-access mr-1"></i>
                {t('admin.orders and subscriptions')}
            </button>
        </nav>
    )
}