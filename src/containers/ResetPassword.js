import React, { useState } from "react";
import { Auth } from "aws-amplify";
import { Link } from "react-router-dom";
import { useFormFields } from "../utils/helpers";
import logo from '../images/logo/logo-dark.png';

export default function ResetPassword() {
    const [fields, handleFieldChange] = useFormFields({
        code: "",
        email: "",
        password: "",
        confirmPassword: "",
    });
    const [codeSent, setCodeSent] = useState(false);
    const [confirmed, setConfirmed] = useState(false);
    const [isConfirming, setIsConfirming] = useState(false);
    const [isSendingCode, setIsSendingCode] = useState(false);
    const [showPassword, setShowPassword] = useState();
    const [showPasswordTwo, setShowPasswordTwo] = useState();

    async function handleSendCodeClick(event) {
        event.preventDefault();

        setIsSendingCode(true);

        try {
            await Auth.forgotPassword(fields.email);
            setCodeSent(true);
        } catch (error) {
            // onError(error);
            console.log("An error while requesting confirmation code", error)
            setIsSendingCode(false);
        }
    }

    async function handleConfirmClick(event) {
        event.preventDefault();

        setIsConfirming(true);

        try {
            await Auth.forgotPasswordSubmit(
                fields.email,
                fields.code,
                fields.password
            );
            setConfirmed(true);
        } catch (error) {
            // onError(error);
            console.log("An error occured while confirming your request for new credential", error)
            setIsConfirming(false);
        }
    }

    function renderRequestCodeForm() {
        return (
            <form onSubmit={handleSendCodeClick}>
                <h6 className="card-subtitle line-on-side text-muted text-center font-small-3"><span>Type in your account email</span></h6>
                <fieldset className="form-group position-relative has-icon-left">
                    <input type="email" className="form-control" id="email" placeholder="Your email" value={fields.email} onChange={handleFieldChange} required/>
                    <div className="form-control-position">
                        <i className="la la-user"></i>
                    </div>
                </fieldset>
                <button type="submit" className="btn btn-outline-info btn-block" tabIndex="0" autoFocus>
                    {isSendingCode && <i className="las la-circle-notch spinner"></i>} Send Confirmation
                </button>
            </form>
        );
    }

    function renderConfirmationForm() {
        return (
            <form onSubmit={handleConfirmClick}>
                <h6 className="card-subtitle line-on-side text-muted text-center font-small-3"><span>Type in your new password</span></h6>
                <fieldset className="form-group position-relative has-icon-left">
                    <input type="tel" className="form-control" id="code" placeholder="Confirmation code" value={fields.code} onChange={handleFieldChange} required/>
                    <div className="form-control-position">
                        <i className="la la-user"></i>
                    </div>
                </fieldset>
                <p>Please check your email ({fields.email}) for the confirmation code.</p>
                <hr />
                <fieldset className="form-group position-relative">
                    <input type={showPassword ? "text" : "password"} className="form-control pl-3" id="password" placeholder="Enter Password" value={fields.password} onChange={handleFieldChange} required/>
                    <div className="has-icon-left">
                        <div className="form-control-position">
                            <i className="la la-key"></i>
                        </div>
                    </div>
                    <div className="form-control-position" onClick={() => setShowPassword(showPassword ? false : true)}>
                        <i className={`las la-${showPassword ? "eye" : "eye-slash"}`}></i>
                    </div>
                </fieldset>
                <fieldset className="form-group position-relative">
                    <input type={showPasswordTwo ? "text" : "password"} className="form-control pl-3" id="confirmPassword" placeholder="Enter Password" value={fields.confirmPassword} onChange={handleFieldChange} required/>
                    <div className="has-icon-left">
                        <div className="form-control-position">
                            <i className="la la-key"></i>
                        </div>
                    </div>
                    <div className="form-control-position" onClick={() => setShowPasswordTwo(showPasswordTwo ? false : true)}>
                        <i className={`las la-${showPasswordTwo ? "eye" : "eye-slash"}`}></i>
                    </div>
                </fieldset>
                <button type="submit" className="btn btn-outline-info btn-block" tabIndex="0" autoFocus>
                    {isConfirming && <i className="las la-circle-notch spinner"></i>} Send confirmation
                </button>
            </form>
        );
    }

    function renderSuccessMessage() {
        return (
            <div class="text-center font-medium-2">
                <i class="las la-check-circle la-2x my-1 success"></i>
                <p>Your password has been reset.</p>
                <Link to="/login">
                    Click here to login with your new credentials.
                    </Link>
            </div>
        );
    }

    return (
        <section className="flexbox-container">
            <div className="col-12 d-flex align-items-center justify-content-center">
                <div className="col-12 col-md-6 col-lg-6 col-xl-4 box-shadow-2 p-0">
                    <div className="card border-grey border-lighten-3 px-1 py-1 m-0">
                        <div className="card-header border-0 pb-0">
                            <div className="card-title text-center">
                                <img src={logo} alt="Albicchiere logo" />
                            </div>
                        </div>
                        <div className="card-content">
                            <div className="card-body">
                                {!codeSent
                                    ? renderRequestCodeForm()
                                    : !confirmed
                                        ? renderConfirmationForm()
                                        : renderSuccessMessage()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}