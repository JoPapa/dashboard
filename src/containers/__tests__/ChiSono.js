import React from 'react';
import { shallow } from 'enzyme';
import ChiSono from '../ChiSono';

describe('<ChiSono/>', () => {
  it('should render correctly', () => {
    const component = shallow(<ChiSono/>);
  
    expect(component).toMatchSnapshot();
  });
});