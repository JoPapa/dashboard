import React from 'react';
import { shallow } from 'enzyme';
import Auth from '../Auth';

describe('<Auth/>', () => {
  it('should render correctly', () => {
    const component = shallow(<Auth/>);
  
    expect(component).toMatchSnapshot();
  });
});