import React, { Component } from 'react'
import { Translation } from 'react-i18next'
import Histogram from '../components/Charts/Histogram'
import RosePie from '../components/Charts/RosePie'
import { getDispensers } from '../utils/api'
// import { Link } from 'react-router-dom'
// import PropTypes from 'prop-types'
import loader from '../images/loader_75_fast.gif'
import LazyLine from '../components/Lazy/LazyLine'
import Collapsible from '../components/Collapsible'
import DispenserSummary from '../components/DispenserSummary'
import AddRoomModal from '../components/AddRoomModal'
import VenueView from '../components/VenueView'
import WineLevel from '../components/WineLevel'
import Label from '../images/albi-wine-label.jpg'
import { AppContext } from '../utils/contexts'
import { getChartArrayFromObject } from '../utils/helpers'

class Dispensers extends Component {
    static contextType = AppContext
    state = {
        // Per avere i field all'interno dell'array 'headers' bisogna conoscere l'oggetto JSON o JS
        headers: [
            { title: "Nome", field: "img" },
            { title: "Vino", field: "format" },
            { title: "Temperatura vino", field: "wineTemperature" },
            { title: "Temperatura esterna", field: "roomTemperature" },
            { title: "Ultimo servizio", field: "lastService" }
        ],
        venueName: '',
        venues: [],
        data: [],
        isAddingRoom: false,
        companyId: null,
        isLoading: true,
        searchQuery: '',
        roomFilters: [],
        showFilters: false,
        filter: false,
        filters: ["red", "white", "rose", "bag", "bottle", "onSale", "pending", "confirmed", "notOnSale"],
        yearRange: {
            min: 2012,
            max: 2018
        },
        monthData: [],
        weekData: [],
        timeSlots: []
    }

    componentDidMount() {
        this.loadRooms()
    }

    componentDidUpdate() {
        if (this.state.companyId !== this.context.currentCompany.id) {
            this.loadRooms()
        }
    }

    loadRooms = () => {
        const companyId = this.context.currentCompany.id
        getDispensers(companyId)
            .then(data => {
                this.setState({
                    companyId,
                    venues: data.venues.map((venue, index) => ({
                        venueId: venue.venueId,
                        venueName: venue.venueName,
                        selected: index === 0 ? true : false
                    })),
                    data: [...data.venues],
                    venueName: data.venues[0].venueName,
                    roomFilters: data.venues[0].rooms,
                    monthData: data.venues[0].monthData,
                    weekData: data.venues[0].weekData,
                    timeSlots: data.venues[0].timeSlots,
                    isLoading: false
                })
                console.log("venues: ", this.state.venues)
            })
            .catch(error => console.log("There was a problem receiving the data, please contact the customer care. Error: ", error));
    }

    updateSearchQuery = (searchQuery) => {
        this.setState({ searchQuery })
    }

    toggleFilter = () => {
        this.setState({ showFilters: !this.state.showFilters })
    }

    handleInputChange = (e) => {
        const value = e.target.checked;
        const name = e.target.name;
        this.setState({
            filter: true,
            filters: value
                ? [...this.state.filters, name]
                : this.state.filters.filter(el => el !== name)
        });
        // [name]: value
    }

    handleYearRangeChange = (values) => {
        this.setState({ ...this.setState, yearRange: { min: values[0], max: values[1] } })
    }

    toggleModal = (value) => {
        this.setState({ isAddingRoom: value })
    }

    selectVenue = (venueId) => {
        const newVenue = this.state.data.filter(venue => venue.venueId === venueId)[0]
        this.setState({
            venues: this.state.venues.map(venue => ({
                ...venue,
                selected: venue.venueId === venueId ? true : false
            })),
            venueName: newVenue.venueName,
            roomFilters: newVenue.rooms,
            monthData: newVenue.monthData,
            weekData: newVenue.weekData,
            timeSlots: newVenue.timeSlots
        })
        console.log("Selected", this.state.roomFilters)
    }

    createRoom = (roomName) => {
        // TODO: Use randomly generated id for this
        this.setState({
            roomFilters: [...this.state.roomFilters, {
                "id": 456324,
                "room": roomName,
                "dispensers": []
            }]
        })
    }

    render() {
        // Set variables for ease of use
        let { headers, venueName, venues, roomFilters, isLoading, isAddingRoom, monthData, weekData, timeSlots } = this.state;
        const withSearch = true;
        const title = venueName;
        const { filters } = this.state;

        const filteredResults = roomFilters.filter(el =>
            el.room.toLowerCase().indexOf(this.state.searchQuery.toLowerCase()) > -1
            // && filters.includes(el.type)
        )

        return (
            <Translation>
                {(t) =>
                    <>
                        <div className="text-left pt-1 ml-1">
                            <h5>{t('dispensers.select location')}</h5>
                        </div>
                        <div className="d-flex">
                            {venues.map(venue =>
                                <div key={venue.venueId}>
                                    <VenueView
                                        venue={venue.venueName}
                                        selected={venue.selected}
                                        onSelected={() => this.selectVenue(venue.venueId)}
                                    />
                                </div>
                            )}
                        </div>
                        <div className="row">
                            <div className="col-12">
                                <div className="card">

                                    {/* Card Header */}
                                    <div className="card-header d-flex justify-content-between">
                                        <h4 className="card-title">
                                            <strong>{title.toUpperCase()}</strong>
                                        </h4>

                                        {/* Heading Elements */}
                                        <div className="d-flex align-items-center">
                                            {/* Inserisci qui gli elementi a destra del titolo */}
                                            {withSearch &&
                                                <fieldset className="position-relative has-icon-left">
                                                    <input
                                                        type="text"
                                                        className="form-control round"
                                                        placeholder={`${t('settings.search')}...`}
                                                        value={this.state.searchQuery}
                                                        onChange={(e) => this.updateSearchQuery(e.target.value)}
                                                    />
                                                    <div className="form-control-position">
                                                        <i className="la la-search"></i>
                                                    </div>
                                                </fieldset>
                                            }

                                            <button className="btn btn-outline-danger round ml-1" onClick={() => this.toggleModal(true)}>
                                                <span className="mx-1">{t('dispensers.add room.button')}</span>
                                                <i className="las la-plus"></i>
                                            </button>
                                        </div>
                                    </div>

                                    {/* Card content, AKA 'Table' */}
                                    <div className="card-content">
                                        {filteredResults
                                            ? isLoading
                                                ? <>
                                                    <LazyWineElement />
                                                    <LazyWineElement />
                                                </>
                                                : filteredResults.map(room => (
                                                    <Collapsible
                                                        key={room.id}
                                                        title={<span>{room.dispensers.some(dispenser => dispenser.status === "orange") && <i className="la la-exclamation-circle warning mr-1"></i>}{room.room}</span>}
                                                        wine={room}
                                                        filters={filters}
                                                        headers={headers}
                                                        yearRange={this.state.yearRange}
                                                    >
                                                        {console.log("room", room)}
                                                        <div className="table-responsive">
                                                            <table className="table table-hover table-lg text-center mb-0">
                                                                {/* Table Header */}
                                                                <thead>
                                                                    <tr>
                                                                        {headers.map(header => (
                                                                            <th key={header.field} className="border-top-0">{header.title}</th>
                                                                        ))}
                                                                        <th className="border-top-0"></th>
                                                                    </tr>
                                                                </thead>
                                                                {/* Table Body */}
                                                                <tbody style={{ "width": "100%" }}>
                                                                    {room.dispensers.map(dispenser => (
                                                                        <tr key={dispenser.id} className="text-center">
                                                                            {headers.map(header => (
                                                                                <td key={header.field} className="align-middle">
                                                                                    {header.field === 'img' &&
                                                                                        <DispenserSummary
                                                                                            name={dispenser.name}
                                                                                            status={dispenser.status}
                                                                                        />
                                                                                    }
                                                                                    {header.field === 'format' &&
                                                                                        // TODO: Crea component per questo elemento se viene ripetuto
                                                                                        <div className="d-flex justify-content-center">
                                                                                            <div>
                                                                                                <img className="img-fluid rounded" src={Label} width={"55px"} height={"75px"} alt={`${dispenser.wine}'s label`} />
                                                                                            </div>
                                                                                            <div className="font-medium-2 text-left px-1">
                                                                                                <div className="font-weight-bold">{dispenser.wine}</div>
                                                                                                <div>{dispenser.winery}</div>
                                                                                                <WineLevel
                                                                                                    totalGlasses={dispenser.bagCapacity}
                                                                                                    glassesLeft={dispenser.quantityLeft}

                                                                                                />
                                                                                            </div>
                                                                                        </div>
                                                                                    }
                                                                                    {(header.field === 'roomTemperature' || header.field === 'wineTemperature') &&
                                                                                        <span className="font-medium-4">{dispenser[header.field]}°C</span>
                                                                                    }
                                                                                    {header.field === 'lastService' &&
                                                                                        <span className="font-medium-4">{dispenser[header.field]}</span>
                                                                                    }
                                                                                </td>
                                                                            ))}
                                                                            <td className="pt-4 text-center">
                                                                                <div className="d-flex justify-content-around">
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    ))}
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </Collapsible>
                                                ))
                                            : t('dispensers.no dispensers')
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* Consumption Analytics */}
                        <div className="card mt-2">
                            <h5 className="m-2">{t('dispensers.charts description')} {venueName}</h5>
                            <div className="row">
                                {/* Per Mese */}
                                <div className="col-md-4 col-12 border-right-blue-grey border-right-lighten-5">
                                    <div className="card">
                                        <div className="card-header">
                                            <h4 className="card-title text-center">{t('charts.month')}</h4>
                                        </div>
                                        <div className="card-body">
                                            <Histogram
                                                data={monthData} />
                                        </div>
                                    </div>
                                </div>
                                {/* Giorno della settimana */}
                                <div className="col-md-4 col-12 border-right-blue-grey border-right-lighten-5">
                                    <div className="card">
                                        <div className="card-header">
                                            <h4 className="card-title text-center">{t('charts.weekdays.title')}</h4>
                                        </div>
                                        <div className="card-body">
                                            <Histogram data={getChartArrayFromObject(weekData)} />
                                        </div>
                                    </div>
                                </div>
                                {/* Fasce Orarie */}
                                <div className="col-md-4 col-12">
                                    <div className="card">
                                        <div className="card-header">
                                            <h4 className="card-title text-center">{t('charts.time slots')}</h4>
                                        </div>
                                        <div className="card-body">
                                            <RosePie data={timeSlots} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <AddRoomModal
                            isOpen={isAddingRoom}
                            onRequestClose={() => this.toggleModal(false)}
                            onSave={(roomName) => this.createRoom(roomName)}
                            venue={venueName}
                        />
                    </>
                }
            </Translation>
        )
    }
}

export default Dispensers


const LazyWineElement = () => {
    return (
        <>
            <div className="pl-3 py-2">
                <LazyLine width="350px" />
            </div>
            <div className="pl-3 pt-2 d-flex align-items-center">
                <div className="pr-3">
                    <LazyLine width="70px" />
                </div>
                <img src={loader} alt="Loading glass.." style={{
                    "width": "75px",
                    "height": "75px",
                }}></img>
                <div className="px-3">
                    <LazyLine width="120px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="80px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="240px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="140px" />
                </div>
            </div>
            <div className="pl-3 py-2 d-flex align-items-center">
                <div className="pr-3">
                    <LazyLine width="70px" />
                </div>
                <img src={loader} alt="Loading glass.." style={{
                    "width": "75px",
                    "height": "75px",
                }}></img>
                <div className="px-3">
                    <LazyLine width="120px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="80px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="240px" />
                </div>
                <div className="pr-3">
                    <LazyLine width="140px" />
                </div>
            </div>
        </>
    )
}