import React, { useState, useEffect } from 'react'
import { listBlockchainBoxes } from '../utils/api'
import ResponsiveTable from '../components/Charts/ResponsiveTable'

const BlockchainTags = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [tags, setTags] = useState([]);

    useEffect(() => {
        listBlockchainBoxes()
            .then(data => {
                setTags(data.boxes)
                setIsLoading(false)
            })
            .catch(error => console.log("There was a problem with the receiving data, please contact the customer care.", error));
    }, [])

    return (
        isLoading
            ? <h3>Loading...</h3>
            : <ResponsiveTable
                title="I tuoi tag"
                headers={[
                    { title: "Tag", field: "tag" }
                ]}
                data={tags}
            />
    )
}

export default BlockchainTags