import React, { Component } from 'react'
import { Translation } from 'react-i18next'
import ResponsiveAlertTable from '../components/Charts/ResponsiveTable'
import RosePie from '../components/Charts/RosePie'
import Histogram from '../components/Charts/Histogram'
import { getFinance } from '../utils/api'
import { AppContext } from '../utils/contexts'
import SmoothLineChart from '../components/Charts/SmoothLineChart'
import SmartBag from '../images/smartbag-icon.png'
import Dispenser from '../images/dispenser-icon.png'
import WineFinanceTable from '../components/Charts/WineFinanceTable'
import ArchiveModal from '../components/ArchiveModal'
import Scrollable from '../components/Scrollable'
import { getChartArrayFromObject } from '../utils/helpers'

// import DataPicker from '../components/DataPicker'

const WallOfFameElement = ({ title, value, icon, img, winner, winnerSubtitle }) => {
    return (
        <div className="d-block">
            <strong className="font-medium-5 text-uppercase">{title}</strong>
            <div className="d-flex my-1">
                <span className="text-center">
                    {icon && <li className={`las la-${icon ? icon : "medal"} la-4x`}></li>}
                    {img && img}
                    <div className="h3 font-weight-bold">{value}</div>
                </span>
                <span className="px-1">
                    <div className="font-medium-3">{winner}</div>
                    {winnerSubtitle &&
                        <strong className="font-medium-2">{winnerSubtitle}</strong>
                    }
                </span>
            </div>
        </div>
    )
}

const ConsumptionsTable = ({ headers, data }) => {
    return (
        <table className="table table-hover table-sm mb-0">
            <thead>
                <tr>
                    <th className="border-top-0 text-center text-uppercase">
                        <input type="checkbox" className="" />
                    </th>
                    <th className="border-top-0 text-center text-uppercase">Time</th>
                    <th className="border-top-0 text-center text-uppercase">Where</th>
                    <th className="border-top-0 text-center text-uppercase">Wine</th>
                    <th className="border-top-0 text-center text-uppercase">Quantity</th>
                    <th className="border-top-0 text-center text-uppercase">Price</th>
                </tr>
            </thead>
            <tbody>
                <tr className="px-1">
                    <td className="text-center align-middle m-0 p-0">
                        <input type="checkbox" className="" />
                    </td>
                    <td className="text-center align-middle p-0">
                        <div>
                            12:30
                            </div>
                        <div>
                            12/10/2020
                            </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <strong>Sala pranzo</strong>
                        <div>Cantina rossa</div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div>Sangiovese 2019</div>
                        <strong>Cantina Albi</strong>
                    </td>
                    <td className="text-center align-middle p-0">Taste</td>
                    <td className="text-center align-middle p-0">$9.45</td>
                </tr>
                <tr className="px-1">
                    <td className="text-center align-middle p-0">
                        <input type="checkbox" className="" />
                    </td>
                    <td className="text-center align-middle p-0">
                        <div>
                            12:30
                            </div>
                        <div>
                            12/10/2020
                            </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <strong>Sala pranzo</strong>
                        <div>Cantina rossa</div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div>Sangiovese 2019</div>
                        <strong>Cantina Albi</strong>
                    </td>
                    <td className="text-center align-middle p-0">Taste</td>
                    <td className="text-center align-middle p-0">$9.45</td>
                </tr>
                <tr className="px-1">
                    <td className="text-center align-middle p-0">
                        <input type="checkbox" className="" />
                    </td>
                    <td className="text-center align-middle p-0">
                        <div>
                            12:30
                            </div>
                        <div>
                            12/10/2020
                            </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <strong>Sala pranzo</strong>
                        <div>Cantina rossa</div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div>Sangiovese 2019</div>
                        <strong>Cantina Albi</strong>
                    </td>
                    <td className="text-center align-middle p-0">Taste</td>
                    <td className="text-center align-middle p-0">$9.45</td>
                </tr>
                <tr className="px-1">
                    <td className="text-center align-middle p-0">
                        <input type="checkbox" className="" />
                    </td>
                    <td className="text-center align-middle p-0">
                        <div>
                            12:30
                            </div>
                        <div>
                            12/10/2020
                            </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <strong>Sala pranzo</strong>
                        <div>Cantina rossa</div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div>Sangiovese 2019</div>
                        <strong>Cantina Albi</strong>
                    </td>
                    <td className="text-center align-middle p-0">Taste</td>
                    <td className="text-center align-middle p-0">$9.45</td>
                </tr>
                <tr className="px-1">
                    <td className="text-center align-middle p-0">
                        <input type="checkbox" className="" />
                    </td>
                    <td className="text-center align-middle p-0">
                        <div>
                            12:30
                            </div>
                        <div>
                            12/10/2020
                            </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <strong>Sala pranzo</strong>
                        <div>Cantina rossa</div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div>Sangiovese 2019</div>
                        <strong>Cantina Albi</strong>
                    </td>
                    <td className="text-center align-middle p-0">Taste</td>
                    <td className="text-center align-middle p-0">$9.45</td>
                </tr>
            </tbody>
        </table>
    )
}

class DashboardPro extends Component {
    static contextType = AppContext
    state = {
        isLoading: true,
        openArchiveModal: false,
        companyId: null,
        financePerios: [],
        wineAlerts: [],
        dispenserAlerts: [],
        weekFinance: [],
        monthFinance: [],
        mostSoldWines: [],
        monthData: [],
        weekData: [],
        timeSlots: [],
        topSales: {
            value: 0,
        },
        bestRoi: {
            value: 0,
        },
        bestRoom: {
            value: 0,
        },
        worstRoom: {
            value: 0,
        },
        bestDispenser: {
            value: 0,
        },
        worstDispenser: {
            value: 0,
        },
    }

    componentDidMount() {
        this.loadData()
    }

    componentDidUpdate() {
        if (this.state.companyId !== this.context.currentCompany.id) {
            this.loadData()
        }
    }

    loadData = () => {
        const companyId = this.context.currentCompany.id
        getFinance(companyId)
            .then(data => {
                console.log("dashboard", data)
                this.setState({
                    ...this.state,
                    companyId,
                    wineAlerts: data.wineAlerts,
                    dispenserAlerts: data.dispenserAlerts,
                    weekFinance: data.finance.week,
                    monthFinance: data.finance.month,
                    mostSoldWines: data.mostSoldWines,
                    monthData: data.monthData,
                    weekData: data.weekData,
                    timeSlots: data.timeSlots,
                    topSales: data.topSales,
                    bestRoi: data.bestRoi,
                    bestRoom: data.bestRoom,
                    worstRoom: data.worstRoom,
                    bestDispenser: data.bestDispenser,
                    worstDispenser: data.worstDispenser,
                    isLoading: false,
                })
                console.log("This company now", this.state.companyInfo)
            })
            .catch(err => console.log("There was an error receiving data", err))
    }
    render() {
        window.scroll(0, 0);
        let {
            isLoading,
            wineAlerts,
            dispenserAlerts,
            weekFinance,
            mostSoldWines,
            monthData,
            weekData,
            timeSlots,
            topSales, bestRoi, bestRoom, bestDispenser, worstDispenser, worstRoom
        } = this.state;
        return (
            <Translation>
                {(t) =>
                    isLoading
                        ? <h3>{t('settings.loading')}...</h3>
                        : <>
                            {/* Alerts */}
                            <div className="card">
                                <div className="row">

                                    {/* Alert tables */}
                                    <div className="col-9 row">
                                        {/* Alert Wine */}
                                        <div className="col-lg-6 col-12">
                                            <ResponsiveAlertTable
                                                title="Alert Vino"
                                                link="/analytics"
                                                headers={[
                                                    { title: "Stato", field: "alertLevel" },
                                                    { title: "TagID", field: "tagID" },
                                                    { title: "Nome Vino", field: "wine" },
                                                    { title: "Descrizione", field: "description" },
                                                    { title: "Stanza", field: "room" }
                                                ]}
                                                data={wineAlerts}
                                                onDeleteAll={() => alert("Function not available yet")}
                                            />
                                        </div>

                                        {/* Alert Dispenser */}
                                        <div className="col-lg-6 col-12">
                                            <ResponsiveAlertTable
                                                title="Alert Dispenser"
                                                link="/analytics"
                                                headers={[
                                                    { title: "Stato", field: "alertLevel" },
                                                    { title: "TagID", field: "tagID" },
                                                    { title: "Nome Vino", field: "wine" },
                                                    { title: "Descrizione", field: "description" },
                                                    { title: "Stanza", field: "room" }
                                                ]}
                                                data={dispenserAlerts}
                                                onDeleteAll={() => alert("Function not available yet")}
                                            />
                                        </div>
                                    </div>

                                    {/* Alert info */}
                                    <div className="col-3 p-2 d-flex justify-content-around align-items-center">
                                        {/* Dispenser alert info */}
                                        <div className="px-1">
                                            <img className="img-fluid" src={Dispenser} width={"90px"} height={"135px"} alt="Dispenser Icon" />
                                            <div className=""></div>
                                            <div className="d-flex align-items-center text-center warning">
                                                <li className="las la-exclamation-circle la-2x"></li>
                                                <div className="font-medium-2"> {"3"} Warnings</div>
                                            </div>
                                            <div className="d-flex align-items-center text-center danger">
                                                <li className="las la-exclamation-triangle la-2x"></li>
                                                <div className="font-medium-2"> {"1"} Critical</div>
                                            </div>
                                        </div>
                                        {/* Wine alert info */}
                                        <div className="">
                                            <div className="px-1">
                                                <img className="img-fluid" src={SmartBag} width={"95px"} height={"145px"} alt="Dispenser Icon" />
                                                <div className="d-flex align-items-center text-center warning">
                                                    <li className="las la-exclamation-circle la-2x"></li>
                                                    <div className="font-medium-2"> {"3"} Warnings</div>
                                                </div>
                                                <div className="d-flex align-items-center text-center">
                                                    <li className="las la-exclamation-triangle la-2x"></li>
                                                    <div className="font-medium-2"> {"0"} Critical</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <hr />
                            {/* Finance data */}
                            <div className="d-flex justify-content-between my-2">
                                <h3 className="text-uppercase">Dati finanziari</h3>
                                <div className="">
                                    <select className="btn mr-1">
                                        <option value={"none"}>Tutte le sedi</option>
                                        <option value={"sede"}>Sede</option>
                                        <option value={"altraSede"}>Altra sede</option>
                                    </select>
                                    <button>Periodo dal __ al __</button>
                                </div>
                            </div>
                            <div className="row">
                                {/* Finance chart */}
                                <div className="col-lg-7 col-12">
                                    <div className="card">
                                        <div className="card-header text-uppercase">
                                            <div className="card-title">Grafico Fatturato</div>
                                            <div className="heading-elements d-flex">
                                                <button className="btn btn-info">Settimana</button><button className="btn">Mese</button>
                                            </div>
                                        </div>
                                        <div className="card-body">
                                            <SmoothLineChart data={weekFinance} />
                                            {/* <Scrollable> */}
                                            <WeekTableCost></WeekTableCost>
                                            {/* <WeekTableTypeWine></WeekTableTypeWine> */}
                                            {/* </Scrollable> */}
                                        </div>
                                    </div>
                                </div>

                                {/* Consumption info */}
                                <div className="col-lg-5 col-12">
                                    <div className="card">
                                        <div className="card-header pb-1">
                                            <div className="card-title text-uppercase">Consumazioni per <strong>Sala Pranzo</strong></div>
                                        </div>
                                        <div className="card-body px-0 pt-0">
                                            <div className="input-group text-center px-2 mb-1">
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    placeholder="Type in the room name"
                                                />
                                                {/* <select className="btn btn-success">
                                                    <option value={"none"}>INVIA A</option>
                                                    <option value={"print"}>Print</option>
                                                    <option value={"software"}>Software</option>
                                                </select> */}
                                            </div>
                                            <div className="d-flex justify-content-between px-2 mb-1">
                                                <button className="btn">Periodo dal __ al __</button>
                                                <div>
                                                    <button className="btn btn-info">Tutto</button><button className="btn">Non contabilizzati</button>
                                                </div>
                                            </div>
                                            <ConsumptionsTable
                                            // headers
                                            // data
                                            />
                                            <div className="d-flex justify-content-around text-center font-medium-1 py-1 mt-1 mx-2 round bg-info white">
                                                <div>
                                                    <div className="text-uppercase">Prima erogazione</div>
                                                    <div className="font-small-3">12:30 AM</div>
                                                    <div className="font-small-3">11/10/2020</div>
                                                    <div className="text-uppercase">Ultima erogazione</div>
                                                    <div className="font-small-3">12:30 AM</div>
                                                    <div className="font-small-3">12/10/2020</div>
                                                </div>
                                                <div>
                                                    <div className="text-uppercase">Erogazioni Taste</div>
                                                    <div className="font-medium-3">20</div>
                                                    <div className="text-uppercase">Erogazioni Drink</div>
                                                    <div className="font-medium-3">5</div>
                                                </div>
                                                <div>
                                                    <div className="text-uppercase">Totale da contabilizzare</div>
                                                    <strong className="font-medium-4">$246.20</strong>
                                                    <div className="mt-1">
                                                        <button type="button" className="btn btn-secondary round mr-1" onClick={() => this.setState({ openArchiveModal: true })}>
                                                            Archivia
                                                        </button>
                                                        <button type="button" className="btn btn-dark round" onClick={() => alert("Questa funzione non è ancora disponibile.")}>
                                                            Contabilizza
                                                        </button>
                                                    </div>
                                                </div>
                                                <ArchiveModal
                                                    isOpen={this.state.openArchiveModal}
                                                    onRequestClose={() => (this.setState({ openArchiveModal: false }))}
                                                    onSave={() => this.setState({ openArchiveModal: false })}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>

                            {/* Cunsumptions + Wall of fame */}
                            <h3 className="text-uppercase my-2">Dati d'uso</h3>
                            <div className="row">
                                {/* Most sold wines */}
                                <div className="col-lg-8 col-12">
                                    <WineFinanceTable
                                        title="Vini più venduti"
                                        headers={[
                                            { title: "Vino", field: "wine" },
                                            { title: "Bicchieri venduti", field: "sold" },
                                            { title: "Performance (7 giorni)", field: "weekPerformance" },
                                            { title: "Fatturato", field: "revenue" },
                                            { title: "ROI", field: "roi" }
                                        ]}
                                        data={mostSoldWines} />
                                </div>

                                {/* Wall of fame */}
                                <div className="col-lg-4 col-12">
                                    <div className="card">
                                        <div className="card-header">
                                            <h4 className="card-title text-center text-uppercase">Wall of fame</h4>
                                        </div>
                                        <div className="card-body">
                                            <div className="d-flex justify-content-around px-1">
                                                <div className="">
                                                    <WallOfFameElement
                                                        title={"Top sales"}
                                                        icon={"trophy"}
                                                        value={topSales.value}
                                                        winner={topSales.wine}
                                                        winnerSubtitle={topSales.winery}
                                                    />
                                                    <WallOfFameElement
                                                        title={"Best room"}
                                                        icon={"home"}
                                                        value={bestRoom.value}
                                                        winner={bestRoom.room}
                                                    />
                                                    <WallOfFameElement
                                                        title={"Best dispenser"}
                                                        icon={"thumbs-up"}
                                                        value={bestDispenser.value}
                                                        winner={bestDispenser.dispenser}
                                                        winnerSubtitle={bestDispenser.room}
                                                    />
                                                </div>
                                                <div className="">
                                                    <WallOfFameElement
                                                        title={"Best roi"}
                                                        icon={"medal"}
                                                        value={bestRoi.value + "%"}
                                                        winner={bestRoi.wine}
                                                        winnerSubtitle={bestRoi.winery}
                                                    />
                                                    <WallOfFameElement
                                                        title={"Worst room"}
                                                        icon={"broom"}
                                                        value={worstRoom.value}
                                                        winner={worstRoom.room}
                                                    />
                                                    <WallOfFameElement
                                                        title={"Worst dispenser"}
                                                        icon={"thumbs-down"}
                                                        value={worstDispenser.value}
                                                        winner={worstDispenser.dispenser}
                                                        winnerSubtitle={worstDispenser.room}
                                                    />
                                                </div>
                                            </div>
                                        </div >
                                    </div >
                                </div >

                            </div >

                            {/* Analytics */}
                            <div className="card" >
                                <div className="row">
                                    {/* Months */}
                                    <div className="col-lg-4 col-12 border-right-blue-grey border-right-lighten-5">
                                        <div className="card">
                                            <div className="card-header">
                                                <h4 className="card-title text-center">{t('charts.month')}</h4>
                                            </div>
                                            <div className="card-body">
                                                <Histogram data={monthData} />
                                            </div>
                                        </div>
                                    </div>
                                    {/* days of the week */}
                                    <div className="col-lg-4 col-12 border-right-blue-grey border-right-lighten-5">
                                        <div className="card">
                                            <div className="card-header">
                                                <h4 className="card-title text-center">{t('charts.weekdays.title')}</h4>
                                            </div>
                                            <div className="card-body">
                                                <Histogram data={getChartArrayFromObject(weekData)} />
                                            </div>
                                        </div>
                                    </div>
                                    {/* Daily time slots */}
                                    <div className="col-lg-4 col-12">
                                        <div className="card">
                                            <div className="card-header">
                                                <h4 className="card-title text-center">{t('charts.time slots')}</h4>
                                            </div>
                                            <div className="card-body">
                                                <RosePie data={timeSlots} />
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </>
                }
            </Translation>
        )

    }
}

export default DashboardPro

const WeekTableCost = () => {
    return (
        <table className="table table-hover table-md">
            <thead>
                <tr className="border-top-0 text-center text-uppercase">
                    <th></th>
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
                    <th>Sunday</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <th scope="row">
                        <div>Ricavi</div>
                    </th>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$35.76</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$35.76</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$35.76</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$35.76</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$35.76</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$35.76</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$35.76</div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <div>Costi</div>
                    </th>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$15.22</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$15.22</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$15.22</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$15.22</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$15.22</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$15.22</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$15.22</div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">
                        <div>Guadagno</div>
                    </th>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$20.54</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$20.54</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$20.54</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$20.54</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$20.54</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$20.54</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>$20.54</div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

    )
}
const WeekTableTypeWine = () => {
    return (
        <table className="table table-hover table-sm">
            <thead>
                <tr className="border-top-0 text-center text-uppercase">
                    <th>Monday</th>
                    <th>Tuesday</th>
                    <th>Wednesday</th>
                    <th>Thursday</th>
                    <th>Friday</th>
                    <th>Saturday</th>
                    <th>Sunday</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>Red</div>
                                <div>White</div>
                                <div>Rose</div>
                            </div>
                            <div>
                                <div>24</div>
                                <div>8</div>
                                <div>12</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>Red</div>
                                <div>White</div>
                                <div>Rose</div>
                            </div>
                            <div>
                                <div>24</div>
                                <div>8</div>
                                <div>12</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>Red</div>
                                <div>White</div>
                                <div>Rose</div>
                            </div>
                            <div>
                                <div>24</div>
                                <div>8</div>
                                <div>12</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>Red</div>
                                <div>White</div>
                                <div>Rose</div>
                            </div>
                            <div>
                                <div>24</div>
                                <div>8</div>
                                <div>12</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>Red</div>
                                <div>White</div>
                                <div>Rose</div>
                            </div>
                            <div>
                                <div>24</div>
                                <div>8</div>
                                <div>12</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>Red</div>
                                <div>White</div>
                                <div>Rose</div>
                            </div>
                            <div>
                                <div>24</div>
                                <div>8</div>
                                <div>12</div>
                            </div>
                        </div>
                    </td>
                    <td className="text-center align-middle p-0">
                        <div className="d-flex justify-content-around">
                            <div>
                                <div>Red</div>
                                <div>White</div>
                                <div>Rose</div>
                            </div>
                            <div>
                                <div>24</div>
                                <div>8</div>
                                <div>12</div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>

    )
}