import React, { Component } from 'react'
import { Translation, useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import ResponsiveTable from '../components/Charts/ResponsiveTable'
import Map from '../components/Charts/Map'
import TopFive from '../components/Charts/TopFive'
import SingleDataCard from '../components/Charts/SingleDataCard'
import { getBlockchainData } from '../utils/api'
import Modal from 'react-modal'
import ConfirmModal from '../components/ConfirmModal'

const searchDropdown = {
    "position": "absolute",
    "top": "43px",
    "width": "100%",
    "backgroundColor": "#fff",
    "borderRadius": "3px",
    "zIndex": "1"
}

class BlockchainHome extends Component {
    state = {
        isLoading: true,
        searchQuery: "",
        searchModal: false,
        isRemovingAlerts: false,
        alerts: [],
        blockchainLocations: [],
        regionsByAlerts: [],
        regionsByConsumption: [],
        regionsByScans: []
    }

    componentDidMount() {
        getBlockchainData()
            .then(data => {
                this.setState({
                    isLoading: false,
                    alerts: data.alert_blockchain,
                    blockchainLocations: data.blockchainLocations,
                    regionsByScans: data.paesi_scansionatori,
                    regionsByConsumption: data.paesi_consumatori,
                    regionsByAlerts: data.paesi_alert
                })
            })
            .catch(error => console.log("There was a problem with the receiving data, please contact the customer care.", error));
    }

    updateSearchQuery = (value) => {
        this.setState({ searchQuery: value })
    }

    toggleSearchModal = () => {
        this.setState({ searchModal: !this.state.searchModal })
    }

    setIsRemovingAlerts = (value) => {
        this.setState({ isRemovingAlerts: value })

    }

    deleteAlerts = () => {
        const empty = []
        this.setState({ alerts: empty })
    }

    render() {
        window.scroll(0, 0);
        let { isLoading, alerts, blockchainLocations, isRemovingAlerts,
            regionsByScans, regionsByConsumption, regionsByAlerts, searchModal, searchQuery } = this.state;
        console.log("Alerts", alerts)
        return (
            <Translation>
                {(t) =>
                    isLoading
                        ? <h3>{t('settings.loading')}...</h3>
                        : <>
                            <div className="d-flex mb-1">
                                {/* <Link to={'/blockchain/tags'} className="btn btn-outline-danger round mr-1">
                                    <span className="mx-1">Vedi tutte le scatole</span>
                                    <i className="las la-arrow-right"></i>
                                </Link>
                                <Link to={'/blockchain/tags'} className="btn btn-outline-danger round mr-1">
                                    <span className="mx-1">Vedi tutti i vini</span>
                                    <i className="las la-arrow-right"></i>
                                </Link> */}
                                <fieldset className="position-relative has-icon-left">
                                    <input
                                        type="text"
                                        className="form-control round"
                                        placeholder="Search..."
                                        value={this.state.searchQuery}
                                        onChange={(e) => this.updateSearchQuery(e.target.value)}
                                    />
                                    <div className="form-control-position">
                                        <i className="la la-search"></i>
                                    </div>
                                    {searchQuery.length > 0 &&
                                        <div className="border border-light" style={searchDropdown}>
                                            <div className="text-center my-1"></div>
                                            <button className="btn round" onClick={() => this.toggleSearchModal()}>Vedi tutti i tag <i className="las la-arrow-right"></i></button>
                                        </div>
                                    }
                                </fieldset>
                                <TagsModal
                                    isOpen={searchModal}
                                    onRequestClose={() => this.toggleSearchModal()}
                                    tags={[
                                        {
                                            type: "box",
                                            tagId: "XX45678"
                                        },
                                        {
                                            type: "box",
                                            tagId: "XX45678"
                                        },
                                        {
                                            type: "bag",
                                            tagId: "XX45678"
                                        },
                                        {
                                            type: "bag",
                                            tagId: "XX45678"
                                        },
                                        {
                                            type: "bag",
                                            tagId: "XX45678"
                                        },
                                        {
                                            type: "bag",
                                            tagId: "XX45678"
                                        },
                                        {
                                            type: "bag",
                                            tagId: "XX45678"
                                        },
                                        {
                                            type: "bag",
                                            tagId: "XX45678"
                                        },
                                    ]}
                                />
                            </div>
                            {/* First Row */}
                            <ResponsiveTable
                                title="Alert Blockchain"
                                link="/blockchain/box"
                                headers={[
                                    { title: t('table headers.status'), field: "alertLevel" },
                                    { title: t('table headers.tag'), field: "tag" },
                                    { title: t('table headers.description'), field: "description" },
                                    { title: t('table headers.date'), field: "data" },
                                    { title: t('table headers.last location'), field: "local" }
                                ]}
                                data={this.state.alerts}
                                onDelete
                                onDeleteAll={() => this.setIsRemovingAlerts(true)}
                            />

                            {/* Second row */}
                            <div className="row">
                                <div className="col-lg-2 col-md-6 col-12">
                                    <SingleDataCard
                                        iconName="circle"
                                        title={t('charts.finished bags')}
                                        value="56%"
                                    />
                                </div>
                                <div className="col-lg-2 col-md-6 col-12">
                                    <SingleDataCard
                                        iconName="barcode"
                                        title={t('charts.scans')}
                                        value="45%"
                                    />
                                </div>
                                <div className="col-lg-3 col-md-6 col-12">
                                    <DoubleDataCard mild={"3.45"} serious={"0.45"} />
                                </div>
                                <div className="col-lg-5 col-md-6 col-12">
                                    <div className="">
                                        <div className="row">
                                            <div className="col-md-6 col-12">
                                                <SingleDataCard
                                                    iconName="qrcode"
                                                    title={t('charts.scanned bags')}
                                                    value="32%"
                                                />
                                            </div>
                                            <div className="col-md-6 col-12">
                                                <SingleDataCard
                                                    iconName="shield-alt"
                                                    title={t('charts.quality score')}
                                                    value="95%"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/* Map */}
                            <div className="card">
                                <div className="row">
                                    <div className="col-md-8 col-12">
                                        <Map markers={blockchainLocations} />
                                    </div>
                                    <div className="col-md-4 col-12">
                                        <TopFive top={3} title={t('charts.regions with most scans')} data={regionsByScans} />
                                        <TopFive top={3} title={t('charts.most consumer countries')} data={regionsByConsumption} />
                                        <TopFive top={3} title={t('charts.countries most alerts')} data={regionsByAlerts} />
                                    </div>
                                </div>
                            </div>
                            <ConfirmModal
                                isOpen={isRemovingAlerts}
                                onRequestClose={() => this.setIsRemovingAlerts(false)}
                                onSave={() => this.deleteAlerts()}
                                message={t('settings.confirm delete alerts')}
                            />
                        </>

                }
            </Translation>
        )
    }
}

export default BlockchainHome

const DoubleDataCard = ({ mild, serious }) => {
    const { t } = useTranslation()
    return (
        <div className="card">
            <div className="card-content">
                <div className="card-body">
                    <h4 className="text-muted pb-1">{t('charts.altered bottles.title')}</h4>
                    <div className="d-flex justify-content-around align-items-center">
                        <div className="text-center">
                            <div className="font-large-2">{mild}%</div>
                            <div className="d-flex align-items-center">
                                <i className={`la la-exclamation-circle warning font-large-1 mr-1`}></i>
                                <div className="font-medium-4">{t('charts.altered bottles.mild')}</div>
                            </div>
                        </div>
                        <div className="text-center">
                            <div className="font-large-2">{serious}%</div>
                            <div className="d-flex align-items-center">
                                <i className={`la la-exclamation-circle danger font-large-1 mr-1`}></i>
                                <div className="font-medium-4">{t('charts.altered bottles.serious')}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

const modalStyle = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        border: '0',
        borderRadius: '15px',
        boxShadow: '0px 0px 20px 0px #c5c5c5',
        marginRight: '-50%',
        padding: '50px',
        transform: 'translate(-50%, -50%)',
        width: '500px',
        zIndex: 2
    }
}

const TagsModal = ({ isOpen, onRequestClose, tags }) => {
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            style={modalStyle}
            contentLabel=""
        >
            <div className="text-center">
                <button
                    type="button"
                    onClick={onRequestClose}
                    className="close"
                    aria-label="Close"
                    style={{
                        position: 'absolute',
                        top: '20px',
                        right: '25px',
                    }}>
                    <i className="la la-times"></i>
                </button>
                {/* <h3>{title}</h3> */}
                <p className="font-medium-2 mt-1">Tutti i tag scatola e bag</p>
                {/* TODO: Aggiungi possibilità di scegliere icona insieme all'indirizzo */}
                <div className="text-center p-1 m-1">
                    {tags.map(tag =>
                        <div key={tag.tagId} className="font-medium-3 d-flex align-items-center my-1">
                            <i className={`font-large-1 la la-${tag.type === 'box' ? "box" : "wine-bottle"} mr-1`}></i>
                            <Link to={`/blockchain/${tag.type === 'bag' ? 'bag' : 'box'}`}>{tag.tagId}</Link>
                        </div>
                    )}
                </div>
            </div>
        </Modal>
    )
}