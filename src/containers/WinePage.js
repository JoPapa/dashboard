import React, { Component } from 'react'
import { Translation } from 'react-i18next'
import WineInfoCard from '../components/WineInfoCard'
import Map from '../components/Charts/Map'
import TopFive from '../components/Charts/TopFive'
import DoubleCard from '../components/Charts/DoubleCard'
import BasicPie from '../components/Charts/BasicPie'
import RosePie from '../components/Charts/RosePie'
import Histogram from '../components/Charts/Histogram'
import ReviewCard from '../components/ReviewCard'
import { getWineInfo } from '../utils/api'
import queryString from 'query-string'
import { getChartArrayFromObject } from '../utils/helpers'

class WinePage extends Component {
    state = {
        wine: {},
        isLoading: true,
        mapData: null,
        topNations: null,
        fasceOrarie: null,
        fasceEta: null,
        weekData: null,
        stagioni: null
    }

    componentDidMount() {
        const { id, year } = queryString.parse(this.props.location.search)
        // TODO: handle year change that comes form the select input in WineInfoCard
        getWineInfo(id)
            .then(data => {
                // instead of using index 0, change the api to search for the year of the wine
                this.setState({
                    wine: data,
                    wineYear: year,
                    mapData: data.wines[0].map_data,
                    topNations: data.wines[0].nationality,
                    fasceOrarie: data.wines[0].time_slots,
                    fasceEta: data.wines[0].age_groups,
                    weekData: data.wines[0].week_data,
                    stagioni: data.wines[0].seasons,
                    isLoading: false
                })
            })
            .catch(err => console.log("There was an error receiving data", err))
    }

    render() {
        window.scrollTo(0, 0);
        console.log("wine", this.state.wineYear)
        // TODO: change state variables
        const { mapData, topNations, fasceEta, fasceOrarie, weekData, stagioni } = this.state
        return (
            <Translation>
                {(t) =>
                    this.state.isLoading
                        ? <h3>Loading...</h3>
                        : <>
                            {/* Firt row: Wine Info */}
                            <div className="row">
                                <div className="col-12">
                                    <WineInfoCard
                                        withActions
                                        wine={this.state.wine}
                                        wineYear={this.state.wineYear}
                                        isLoading={this.state.isLoading}
                                    />
                                </div>
                            </div>

                            {/* Second row */}
                            <div className="row">

                                <div className="col-lg-4 col-12">
                                    <ReviewCard />
                                </div>

                                <div className="col-lg-8 col-12">
                                    <div className="card">
                                        <div className="row">

                                            <div className="col-md-8 col-12">
                                                <Map data={mapData} />
                                            </div>

                                            <div className="col-md-4 col-12">
                                                <TopFive
                                                    title={t('charts.top nations')}
                                                    data={topNations}
                                                />
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            {/* Third row */}
                            <div className="row">

                                <div className="col-lg-3 col-md-6 col-12">
                                    {/* TODO: Change this data with real one */}
                                    <DoubleCard title={t('charts.total users.title')}
                                        left={{
                                            name: t('charts.total users.male'),
                                            icon: "la la-mars",
                                            percentage: "40%"
                                        }}
                                        right={{
                                            name: t('charts.total users.female'),
                                            icon: "la la-venus",
                                            percentage: "60%"
                                        }} />
                                </div>

                                <div className="col-lg-3 col-md-6 col-12">
                                    <div className="card">
                                        <div className="card-header">
                                            <h4 className="card-title text-center">{t('charts.age groups')}</h4>
                                        </div>
                                        <div className="card-body">
                                            <BasicPie data={fasceEta} legend />
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-3 col-md-6 col-12">
                                    <DoubleCard title={t('charts.consumption.title')}
                                        left={{
                                            name: t('charts.consumption.alone'),
                                            icon: "la la-user",
                                            percentage: "45%"
                                        }}
                                        right={{
                                            name: t('charts.consumption.with others'),
                                            icon: "la la-users",
                                            percentage: "55%"
                                        }} />
                                </div>

                                <div className="col-lg-3 col-md-6 col-12">
                                    <TopFive title={t('charts.nationalities')} data={topNations} />
                                </div>
                            </div>

                            {/* Fourth row */}
                            <div className="row">
                                <div className="card col-12">
                                    <div className="row">

                                        <div className="col-lg-4 col-12 border-right-blue-grey border-right-lighten-5">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h4 className="card-title text-center">{t('charts.time slots')}</h4>
                                                </div>
                                                <div className="card-body">
                                                    <RosePie data={fasceOrarie} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-lg-4 col-12 border-right-blue-grey border-right-lighten-5">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h4 className="card-title text-center">{t('charts.weekdays.title')}</h4>
                                                </div>
                                                <div className="card-body">
                                                    <Histogram data={getChartArrayFromObject(weekData)} />
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-lg-4 col-12">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h4 className="card-title text-center">{t('charts.seasons.title')}</h4>
                                                </div>
                                                <div className="card-body">
                                                    <BasicPie data={stagioni} legend />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </>
                }
            </Translation>
        )
    }
}

export default WinePage