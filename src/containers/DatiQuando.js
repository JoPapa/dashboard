import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
// import TopFive from '../components/Charts/TopFive'
import DoubleCard from '../components/Charts/DoubleCard'
import TempMedia from '../components/Charts/TempMedia'
import RosePie from '../components/Charts/RosePie'
// import BasicPie from '../components/Charts/BasicPie'
import CircleChart from '../components/Charts/CircleChart'
import Histogram from '../components/Charts/Histogram'
import FilterButton from '../components/FilterButton'
import DataPicker from '../components/DataPicker'

const ChiSono = (props) => {
    window.scrollTo(0, 0);
    const { t } = useTranslation();
    return (
        <>
            {/* Intro Row */}
            <div className="row">
                <div className="col-md-8 col-12">
                    <Link className="text-left btn btn-primary m-1" to="/analytics"> {"< Back"}</Link>
                    <FilterButton />
                </div>
                <div className="col-md-4 col-12">
                    <DataPicker />
                </div>
            </div>

            {/* First Row */}
            <div className="row">
                <div className="col-12">
                    <div className="card">
                        <div className="row">
                            <div className="col-md-3 col-12 border-right-blue-grey border-right-lighten-5">
                                <div className="card">
                                    <div className="card-header">
                                        <h4 className="card-title text-center">{t('charts.seasons.winter')}</h4>
                                    </div>
                                    <div className="card-body">
                                        <CircleChart />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 col-12 border-right-blue-grey border-right-lighten-5">
                                <div className="card">
                                    <div className="card-header">
                                        <h4 className="card-title text-center">{t('charts.seasons.spring')}</h4>
                                    </div>
                                    <div className="card-body">
                                        <CircleChart />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 col-12 border-right-blue-grey border-right-lighten-5">
                                <div className="card">
                                    <div className="card-header">
                                        <h4 className="card-title text-center">{t('charts.seasons.summer')}</h4>
                                    </div>
                                    <div className="card-body">
                                        <CircleChart />
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-3 col-12">
                                <div className="card">
                                    <div className="card-header">
                                        <h4 className="card-title text-center">{t('charts.seasons.autumn')}</h4>
                                    </div>
                                    <div className="card-body">
                                        <CircleChart />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {/* Second Row */}
            <div className="row">
                <div className="col-md-4">
                    <div className="card">
                        <div className="card-header">
                            <h4 className="card-title text-center">{t('charts.consumption hemisphere')}</h4>
                        </div>
                        <div className="card-body">
                            <Histogram data={[
                                { name: 'Gen', value: 10 },
                                { name: 'Feb', value: 2 },
                                { name: 'Mar', value: 10 },
                                { name: 'Apr', value: 5 },
                                { name: 'Mag', value: 12 },
                                { name: 'Giu', value: 3 },
                                { name: 'Lug', value: 6 },
                                { name: 'Ago', value: 6 },
                                { name: 'Set', value: 6 },
                                { name: 'Ott', value: 6 },
                                { name: 'Nov', value: 6 },
                                { name: 'Dic', value: 6 }
                            ]}
                                data2={[
                                    { name: 'Gen', value: 20 },
                                    { name: 'Feb', value: 3 },
                                    { name: 'Mar', value: 10 },
                                    { name: 'Apr', value: 8 },
                                    { name: 'Mag', value: 12 },
                                    { name: 'Giu', value: 3 },
                                    { name: 'Lug', value: 16 },
                                    { name: 'Ago', value: 6 },
                                    { name: 'Set', value: 12 },
                                    { name: 'Ott', value: 6 },
                                    { name: 'Nov', value: 21 },
                                    { name: 'Dic', value: 17 }
                                ]}
                                legend />
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="card">
                        <div className="card-header">
                            <h4 className="card-title text-center">{t('charts.weekdays.title')}</h4>
                        </div>
                        <div className="card-body">
                            <Histogram data={[
                                { name: 'Lun', value: 10 },
                                { name: 'Mar', value: 20 },
                                { name: 'Mer', value: 30 },
                                { name: 'Gio', value: 25 },
                                { name: 'Ven', value: 12 },
                                { name: 'Sab', value: 3 },
                                { name: 'Dom', value: 6 }
                            ]} />
                        </div>
                    </div>
                </div>
                <div className="col-md-4">
                    <div className="card">
                        <div className="card-header">
                            <h4 className="card-title text-center">{t('charts.time slots')}</h4>
                        </div>
                        <div className="card-body">
                            <RosePie />
                        </div>
                    </div>
                </div>
            </div>
            {/* Third Row */}
            <div className="row">
                <div className="col-md-3">
                    <DoubleCard title={t('charts.pouring options.title')}
                        left={{
                            name: t('charts.pouring options.taste'),
                            icon: "la la-glass",
                            percentage: "23%"
                        }}
                        right={{
                            name: t('charts.pouring options.glass'),
                            icon: "la la-glass",
                            percentage: "77%"
                        }} />
                </div>
                <div className="col-md-3">
                    <DoubleCard title={t('charts.consumption period.title')}
                        left={{
                            name: t('charts.consumption period.during meals'),
                            icon: "la la-cutlery",
                            percentage: "82%"
                        }}
                        right={{
                            name: t('charts.consumption period.between meals'),
                            icon: "la la-birthday-cake",
                            percentage: "18%"
                        }} />
                </div>
                <div className="col-md-3">
                    <DoubleCard title={t('charts.consumption.title')}
                        left={{
                            name: t('charts.consumption.alone'),
                            icon: "la la-user",
                            percentage: "45%"
                        }}
                        right={{
                            name: t('charts.consumption.with others'),
                            icon: "la la-users",
                            percentage: "55%"
                        }} />
                </div>
                <div className="col-md-3">
                    {/* <TempMedia /> */}
                </div>
            </div>
        </>
    )
}

export default ChiSono