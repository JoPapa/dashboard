import React, { useState } from 'react'
import { useAppContext } from "../utils/contexts"
import SupportFooter from '../components/SupportFooter'
import logo from '../images/logo/logo-dark.png'
// import { createCompany } from '../utils/api'

const Home = () => {
    const { authorize, setCurrentCompany } = useAppContext();
    const [businessType, setBusinessType] = useState('');
    const [isBusinessSelected, setIsBusinessSelected] = useState(false);
    const [error, setError] = useState('');
    const [fields, setFields] = useState({
        brandName: "",
        companyName: "",
        address: "",
        phoneNumber: "",
        vatNumber: "",
    });

    const handleFieldChange = (event) => {
        setFields({
            ...fields,
            [event.target.id]: event.target.value
        });
    }

    async function handleCompanyInfoSubmit(e) {
        e.preventDefault()
        try {
            // Create Company
            // const res = await createCompany(fields)
            // console.log("Company retrieved => ", res)
            // Set company state
            setCurrentCompany({
                ...fields,
                id: "000001",
            })
            // Authorize access
            authorize('winery')
            // console.log(res)
        } catch (error) {
            console.log(error)
            setError("There was an error: " + error.message)
        }
    }

    function renderCompanyTypeSelection() {
        return (
            <>
                <div className="row my-1">
                    <div className="col"></div>
                    <div
                        className={`col border${businessType === "winery" && '-3'} border-info box-shadow-1 rounded p-1 mx-2 m-1`}
                        onClick={() => setBusinessType('winery')}
                        style={{ "cursor": "pointer" }}
                    >
                        <h4>{"Winery".toUpperCase()}</h4>
                        <p>Descrizione testuale e vantaggi:</p>
                        <ul className="px-3">
                            <li>Vantaggio 1</li>
                            <li>Vantaggio 2</li>
                            <li>Vantaggio 3</li>
                        </ul>
                    </div>
                    <div
                        className={`col border${businessType === "professional" && '-3'} border-info box-shadow-1 rounded p-1 m-1`}
                        // onClick={() => setBusinessType('professional')}
                        style={{ "cursor": "pointer" }}
                    >
                        <h4>{"Professional".toUpperCase()}</h4>
                        <div className="my-2">
                            <li className="las la-play-circle la-4x"></li>
                            <p className="font-medium-3">Coming soon</p>
                        </div>
                        {/* When ready */}
                        {/* <p>Descrizione testuale e vantaggi:</p>
                        <ul className="px-3">
                            <li>Vantaggio 1</li>
                            <li>Vantaggio 2</li>
                            <li>Vantaggio 3</li>
                        </ul> */}
                    </div>
                    <div className="col"></div>
                </div>
                <button type="button" disabled={!businessType} onClick={() => setIsBusinessSelected(true)} className="btn btn-outline-danger round">
                    <span className="mx-1">START</span>
                    <i className="las la-arrow-right"></i>
                </button>
            </>
        )
    }

    function renderCompanyInfoForm() {
        return (
            <form onSubmit={handleCompanyInfoSubmit}>
                <fieldset className="form-group position-relative has-icon-left">
                    <input type="text" className="form-control" placeholder="Your brand name" id="brandName" value={fields.brandName} onChange={handleFieldChange} required />
                    <div className="form-control-position">
                        <i className="las la-copyright"></i>
                    </div>
                </fieldset>
                <fieldset className="form-group position-relative has-icon-left">
                    <input type="text" className="form-control" placeholder="Your company name" id="companyName" value={fields.companyName} onChange={handleFieldChange} required />
                    <div className="form-control-position">
                        <i className="las la-copyright"></i>
                    </div>
                </fieldset>
                <fieldset className="form-group position-relative has-icon-left">
                    <input type="text" className="form-control" placeholder="VAT number" id="vatNumber" value={fields.vatNumber} onChange={handleFieldChange} required />
                    <div className="form-control-position">
                        <i className="las la-percent"></i>
                    </div>
                </fieldset>
                <fieldset className="form-group position-relative has-icon-left">
                    <input type="text" className="form-control" placeholder="Your business address" id="address" value={fields.address} onChange={handleFieldChange} required />
                    <div className="form-control-position">
                        <i className="las la-building"></i>
                    </div>
                </fieldset>
                <fieldset className="form-group position-relative has-icon-left">
                    <input type="tel" className="form-control" placeholder="Business phone number" id="phoneNumber" value={fields.phoneNumber} onChange={handleFieldChange} required />
                    <div className="form-control-position">
                        <i className="las la-phone"></i>
                    </div>
                </fieldset>
                <button type="submit" className="btn btn-outline-danger btn-block" tabIndex="0" autoFocus>Continue</button>
                <div style={{ color: 'red' }}>{error}</div>
            </form>
        )
    }

    const WinerySearch = () => {
        // TODO: get wineries data from db
        const style = {
            "maxHeight": "250px",
            "overflowY": "auto",
            "cursor": "pointer",
        }
        const wineries = businessType === "winery" ? ['Cantina Albicchiere', 'Cantina Albi', 'Cantina Albi Home', 'Cantina Albiani', 'Cantina Monterolo', 'Tenuta Albi Home M'] : ["Ristorante Romolo", "Hotel Albi", "Best Albi Hotels", "Albi 5 stars", "Red Albister"];
        const [searchQuery, setSearchQuery] = useState();
        const [filteredList, setFilteredList] = useState(wineries);

        const handleSearch = (e) => {
            let currentList = []
            let filteredList = []

            if (e.target.value !== "") {
                currentList = wineries;

                filteredList = currentList.filter(item => {
                    const lc = item.toLowerCase()
                    const filter = e.target.value.toLowerCase()
                    return lc.includes(filter)
                })
            } else {
                filteredList = wineries;
            }
            setFilteredList(filteredList)
            console.log("target", e.target.value)
        }

        const setCompanyName = (e, winery) => {
            e.target.value = winery;
            setFields({
                brandName: winery,
                companyName: `${winery} s.r.l`,
                vatNumber: '004583989283',
                address: 'Via cosmo 87',
                phoneNumber: '+39 067788948'
            });
            setSearchQuery(winery)
        }

        return (
            <>
                {/* <legend className="font-medium-1">Search for an existing winery or create a new one</legend> */}
                <input className="form-control" type="text" placeholder={`Search for your ${businessType === "winery" ? "winery" : "business"}`} aria-label="Search" value={searchQuery} onChange={(e) => handleSearch(e)} />
                <div style={style} className="border border-1 border-light rounded my-1 py-1">
                    {filteredList.length > 0
                        ? filteredList.map((winery) =>
                            <div key={winery} className="p-1" id="companyName" onClick={(e) => { setCompanyName(e, winery) }}>
                                {winery}
                            </div>
                        ) : <p>There is no winery matchinng your search, create a new one!</p>
                    }
                </div>
            </>
        )
    }

    return (
        <>
            <div className="card">
                <div className="text-center p-4">
                    <img className="brand-logo" alt="albicchiere's logo" src={logo} />
                    <h3>Ti dà il benvenuto</h3>
                    <h4 className="mt-3">Cerca {businessType === "winery" ? "una cantina" : "un'azienda"} esistente o creane una nuova.</h4>
                    {!isBusinessSelected
                        ? renderCompanyTypeSelection()
                        : <div className="row mt-2">
                            <div className="col"></div>
                            <div className="col">
                                <WinerySearch />
                            </div>
                            <div className="col">
                                {renderCompanyInfoForm()}
                            </div>
                            <div className="col"></div>
                        </div>
                    }
                </div>
            </div>
            <SupportFooter />
        </>
    )
}

export default Home



