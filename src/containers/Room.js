import React, { Component } from 'react'
import { Translation } from 'react-i18next'
import ResponsiveTable from '../components/Charts/ResponsiveTable'
import ConsumptionsTable from '../components/Charts/ConsumptionsTable'
import Histogram from '../components/Charts/Histogram'
import RosePie from '../components/Charts/RosePie'
import DispenserInfo from '../components/DispenserInfo'
import { getRoom } from '../utils/api'
import { AppContext } from '../utils/contexts'
import { getChartArrayFromObject } from '../utils/helpers'

class Room extends Component {
    static contextType = AppContext
    state = {
        companyId: null,
        isLoading: true,
        alertDispenser: {
            data: []
        },
        dispensers: [],
        consumptionLogs: {
            data: []
        },
        filteredConsumptionData: [],
        monthData: [],
        weekData: [],
        timeSlots: []
    }

    componentDidMount() {
        this.loadRoom()
    }

    componentDidUpdate() {
        if (this.state.companyId !== this.context.currentCompany.id) {
            this.loadRooms()
        }
    }

    loadRoom = () => {
        console.log("chagein")
        const companyId = this.context.currentCompany.id
        // fetching Alert data
        getRoom(companyId)
            .then(data => {
                this.setState({
                    ...this.state,
                    companyId,
                    alertDispenser: { ...this.state.alertDispenser, data: data.alert },
                    dispensers: data.dispensers.map(dispenser => ({ ...dispenser, checked: true })),
                    consumptionLogs: { ...this.state.consumptionLogs, data: data.roomConsumptions },
                    filteredConsumptionData: data.roomConsumptions,
                    monthData: data.monthData,
                    weekData: data.weekData,
                    timeSlots: data.timeSlots,
                    isLoading: false
                })
                console.log("The data:", data)
            })
            .catch(error => console.log("There was a problem with the receiving data, please contact the customer care." + error));
    }

    checkAllDispensers = (value) => {
        const newDispensers = this.state.dispensers.map(dispenser => ({ ...dispenser, checked: value }))
        this.setState({
            dispensers: newDispensers,
            filteredConsumptionData: this.state.consumptionLogs.data.filter(log =>
                newDispensers.some(dispenser => dispenser.id === log.dispenserId && dispenser.checked === true)
            )
        })
    }

    toggleDispenser = (dispenserId) => {
        const newDispensers = this.state.dispensers.map(dispenser => ({ ...dispenser, checked: dispenser.id === dispenserId ? !dispenser.checked : dispenser.checked }))
        const filterDispenser = newDispensers.map(dispenser => dispenser.checked === true && dispenser.id)
        const consumptionData = this.state.consumptionLogs.data.filter(log => filterDispenser.includes(log.dispenserId))

        this.setState({
            dispensers: newDispensers,
            filteredConsumptionData: consumptionData
        })

    }

    render() {
        // window.scroll(0, 0);
        let { isLoading, alertDispenser, dispensers, filteredConsumptionData, monthData, weekData, timeSlots } = this.state;
        const DataCard = ({ title, value }) => {
            return (
                <div className="card">
                    <div className="card-content">
                        <div className="card-body">
                            <h4 className="text-muted pb-2">{title}</h4>
                            <div className="font-large-1 pb-2">{value}</div>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <Translation>
                {(t) =>
                    isLoading
                        ? <h3>{t('settings.loading')}...</h3>
                        : <>
                            {/* First Row */}
                            <div className="row">
                                <div className="col-lg-5 col-12">
                                    <div className="card">
                                        <div className="card-header my-1">
                                            <h1>SEDE - Room 123</h1>
                                        </div>
                                        <div className="row mt-1">
                                            <div className="col-4">
                                                <DataCard title={t('room.total pourings')} value="3589" />
                                            </div>
                                            <div className="col-4">
                                                <DataCard title={t('room.last service')} value="Oggi 16:29" />
                                            </div>
                                            <div className="col-4">
                                                <DataCard title={t('room.total dispensers')} value="5" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {/* Alerts */}
                                <div className="col-lg-7 col-12">
                                    <ResponsiveTable
                                        title="Alert"
                                        link="/analytics"
                                        headers={[
                                            { title: t('table headers.status'), field: "alertLevel" },
                                            { title: t('table headers.tag id'), field: "tagID" },
                                            { title: t('table headers.wine name'), field: "wine" },
                                            { title: t('table headers.description'), field: "description" },
                                            { title: t('table headers.room'), field: "room" }
                                        ]}
                                        data={alertDispenser.data}
                                        onDelete />
                                </div>
                            </div>
                            {/* Dispensers */}
                            <div className="">
                                <div className="row my-2">
                                    <div className="col-lg-12 col-12">
                                        <button className="btn btn-round" onClick={() => this.checkAllDispensers(true)}>{t('room.select all')}</button>
                                        <button className="btn btn-round" onClick={() => this.checkAllDispensers(false)}>{t('room.deselect all')}</button>
                                        <div className="row">
                                            {dispensers.map(dispenser =>
                                                <div key={dispenser.id} className="col-12 col-md-6 col-lg-4">
                                                    <DispenserInfo
                                                        dispenser={dispenser}
                                                        checked={dispenser.checked}
                                                        onCheck={() => this.toggleDispenser(dispenser.id)}
                                                    />
                                                </div>
                                            )}
                                        </div>
                                    </div>
                                </div>
                                {/* Consumption Logs */}
                                <ConsumptionsTable
                                    title={t('room.last consumptions')}
                                    headers={[
                                        { title: t('table headers.date'), field: "data" },
                                        { title: t('table headers.time'), field: "ora" },
                                        { title: t('table headers.device'), field: "dispenserName" },
                                        { title: t('table headers.wine'), field: "wine" },
                                        { title: t('table headers.serving temperature'), field: "servingTemperature" },
                                        { title: t('table headers.bag code'), field: "bagId" },
                                        { title: t('table headers.pouring type'), field: "pouringType" }
                                    ]}
                                    data={filteredConsumptionData} />
                                {/* Consumption Analytics */}
                                <div className="card">
                                    <div className="text-center my-1 ml-1">
                                        <h3>{t('room.chart title').toUpperCase()}</h3>
                                    </div>
                                    <div className="row">
                                        {/* Per Mese */}
                                        <div className="col-md-4 col-12 border-right-blue-grey border-right-lighten-5">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h4 className="card-title text-center">{t('charts.month')}</h4>
                                                </div>
                                                <div className="card-body">
                                                    <Histogram
                                                        data={monthData} />
                                                </div>
                                            </div>
                                        </div>
                                        {/* Giorno della settimana */}
                                        <div className="col-md-4 col-12 border-right-blue-grey border-right-lighten-5">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h4 className="card-title text-center">{t('charts.weekdays.title')}</h4>
                                                </div>
                                                <div className="card-body">
                                                    <Histogram data={getChartArrayFromObject(weekData)} />
                                                </div>
                                            </div>
                                        </div>
                                        {/* Fasce Orarie */}
                                        <div className="col-md-4 col-12">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h4 className="card-title text-center">{t('charts.time slots')}</h4>
                                                </div>
                                                <div className="card-body">
                                                    <RosePie data={timeSlots} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </>
                }
            </Translation>
        )
    }
}

export default Room