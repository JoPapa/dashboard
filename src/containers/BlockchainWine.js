import React from 'react'
import { Translation } from 'react-i18next'
import BagInfoCard from '../components/BagInfoCard'
import Timeline from '../components/Charts/Timeline'
import ResponsiveTable from '../components/Charts/ResponsiveTable'
import BlockchainScoreCard from '../components/BlockchainScoreCard'
import WeatherMap from '../components/WeatherMap'
import { getBagInfo } from '../utils/api'
import queryString from 'query-string'
import BlockchainChart from '../components/Charts/BlockchainChart'

class BlockchainWine extends React.Component {
    state = {
        isLoading: true,
        wineData: "",
        qualityScore: "",
        wineCoordinates: {
            latitude: "",
            longitude: ""
        },
        blockchainLogs: [],
        mainEvents: [
            {
                timestamp: '15 Gen',
                status: 'danger',
                message: 'Rilevata temperatura eccessiva'
            },
            {
                timestamp: '16 Gen',
                status: 'success',
                message: 'Trasporto Corriere'
            },
            {
                timestamp: '18 Gen',
                status: 'danger',
                message: 'Rilevata temperatura eccessiva'
            },
            {
                timestamp: '20 Gen',
                status: 'success',
                message: 'Arrivo a destinazione'
            },
            {
                timestamp: '23 Gen',
                status: 'success',
                message: 'Arrivo a destinazione'
            },
        ]
    }

    componentDidMount() {
        const { id } = queryString.parse(this.props.location.search)
        getBagInfo(id)
            .then(data => {
                this.setState({
                    isLoading: false,
                    wineData: data.wine,
                    qualityScore: data.qualityScore,
                    wineCoordinates: data.coordinates,
                    blockchainLogs: data.blockchainLogs
                })
            })
            .catch(err => console.log("There was an error receiving data", err))
    }

    render() {
        const { isLoading, wineData, qualityScore, wineCoordinates, blockchainLogs, mainEvents } = this.state
        return (
            <Translation>
                {(t) =>
                    isLoading
                        ? <h3>{t('settings.loading')}...</h3>
                        : <>
                            < div className="row" >
                                {/* Firt row: Wine Info */}

                                <div className="col-lg-8 col-12">
                                    <BagInfoCard wine={wineData} />
                                </div>

                                <div className="col-lg-4 col-12">
                                    <BlockchainScoreCard score={qualityScore} />
                                </div>

                            </ div>

                            <div className="row">
                                <div className="col-xl-8 col-lg-12">
                                    <Timeline data={mainEvents} />
                                </div>
                                <div className="col-xl-4 col-lg-12">
                                    <WeatherMap
                                        latitude={wineCoordinates.latitude}
                                        longitude={wineCoordinates.longitude} />
                                </div>
                            </div>

                            <ResponsiveTable
                                title={t('blockchain.blockchain log')}
                                headers={[
                                    { title: t('table headers.log'), field: "log" },
                                    { title: t('table headers.user'), field: "user" },
                                    { title: t('table headers.user type'), field: "userType" },
                                    { title: t('table headers.description'), field: "description" },
                                    { title: t('table headers.date'), field: "date" },
                                    { title: t('table headers.time'), field: "hour" },
                                    { title: t('table headers.location'), field: "location" },
                                    { title: t('table headers.paired tags'), field: "parentTag" },
                                ]}
                                data={blockchainLogs} />

                            <div className="card">
                                <div className="card-header">{t('blockchain.temperature and acceleration')}</div>
                                <div className="card-body" style={{ "height": "600px" }}>
                                    <BlockchainChart />
                                </div>
                            </div>
                        </>
                }
            </Translation>
        )
    }
}

export default BlockchainWine