import React, { Component } from 'react'
import { Translation } from 'react-i18next'
// import ResponsiveTable from '../components/Charts/ResponsiveTable'
import ResponsiveAlertTable from '../components/Charts/ResponsiveTable'
import Modal from 'react-modal'
import StockTable from '../components/StockTable'
import SmartBag from '../images/smartbag-icon.png'
import Label from '../images/albi-wine-label.jpg'
import ConfirmModal from '../components/ConfirmModal'

const stockData = [{
    "id": 1111,
    "wine": "Sangiovese",
    "vintage": "2018",
    "img": "https://www.stoneycreekwinepress.com/assets/images/labels/categories/classic-vineyard-wine-label1.jpg",
    "winery": "Cantina Albi",
    "purchasePrice": "32.00",
    "available": "921",
    "opened": "237",
    "expiring": "45",
    "expired": "21",
    "bags": [
        {
            "bagCode": "AC5490",
            "status": "opened",
            "dispenser": "Alby09",
            "room": "Room 4",
            "openingDate": "07/20",
            "expiration": "09/20",
            "wineLeft": "3",
            "lastConsumption": "8/20 4:02",
            "alert": ""
        }
    ]
},
{
    "id": 1112,
    "wine": "Grechetto",
    "vintage": "2017",
    "img": "https://www.stoneycreekwinepress.com/assets/images/labels/categories/classic-vineyard-wine-label1.jpg",
    "winery": "Cantina Albi",
    "purchasePrice": "54.00",
    "available": "1290",
    "opened": "432",
    "expiring": "123",
    "expired": "86",
    "bags": [
        {
            "bagCode": "AC5490",
            "status": "opened",
            "dispenser": "Alby09",
            "room": "Room 4",
            "openingDate": "07/20",
            "expiration": "09/20",
            "wineLeft": "3",
            "lastConsumption": "8/20 4:02",
            "alert": ""
        }
    ]
},
{
    "id": 1113,
    "wine": "Montefalco",
    "vintage": "2014",
    "img": "https://www.stoneycreekwinepress.com/assets/images/labels/categories/classic-vineyard-wine-label1.jpg",
    "winery": "Cantina Albi",
    "purchasePrice": "65.00",
    "available": "324",
    "opened": "57",
    "expiring": "15",
    "expired": "21",
    "bags": [
        {
            "bagCode": "AC5490",
            "status": "opened",
            "dispenser": "Alby09",
            "room": "Room 4",
            "openingDate": "07/20",
            "expiration": "09/20",
            "wineLeft": "3",
            "lastConsumption": "8/20 4:02",
            "alert": ""
        }
    ]
}]

const modalStyle = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        border: '0',
        borderRadius: '15px',
        boxShadow: '0px 0px 20px 0px #c5c5c5',
        marginRight: '-50%',
        padding: '50px',
        transform: 'translate(-50%, -50%)',
        maxWidth: '750px'
    }
}

class Stock extends Component {
    // TODO: stock headers must be included outside of state in order to be translated
    state = {
        isLoading: false,
        wineSearch: '',
        isModalOpen: false,
        isUploadModal: false,
        showArchive: false,
        selectedRoom: '',
        isDeletingAlert: '',
        isRemovingAlerts: '',
        rooms: [
            {
                id: "00001",
                name: "Reception"
            },
            {
                id: "00002",
                name: "Sala pranzo"
            },
            {
                id: "00003",
                name: "Sala degustazioni"
            }
        ],
        itemsToUpload: [{
            "id": 4531,
            "wine": "Sangiovese",
            "vintage": "2015",
            "img": "https://www.stoneycreekwinepress.com/assets/images/labels/categories/classic-vineyard-wine-label1.jpg",
            "winery": "Cantina Albi",
            "purchasePrice": "29.97",
            "available": "921",
            "opened": "237",
            "expiring": "45",
            "selected": true,
            "expired": "21",
            "bags": [
                {
                    "bagCode": "AC5490",
                    "status": "opened",
                    "dispenser": "Alby09",
                    "room": "Room 4",
                    "openingDate": "07/20",
                    "expiration": "09/20",
                    "wineLeft": "3",
                    "lastConsumption": "8/20 4:02",
                    "alert": ""
                }
            ]
        }, {
            "id": 4231,
            "wine": "Bordeaux",
            "vintage": "2018",
            "img": "https://www.stoneycreekwinepress.com/assets/images/labels/categories/classic-vineyard-wine-label1.jpg",
            "winery": "Cantina Albi",
            "purchasePrice": "29.97",
            "available": "921",
            "opened": "237",
            "expiring": "45",
            "selected": true,
            "expired": "21",
            "bags": [
                {
                    "bagCode": "AC5230",
                    "status": "opened",
                    "dispenser": "Alby09",
                    "room": "Room 4",
                    "openingDate": "07/20",
                    "expiration": "09/20",
                    "wineLeft": "3",
                    "lastConsumption": "8/20 4:02",
                    "alert": ""
                },
                {
                    "bagCode": "AC5450",
                    "status": "opened",
                    "dispenser": "Alby09",
                    "room": "Room 4",
                    "openingDate": "07/20",
                    "expiration": "09/20",
                    "wineLeft": "3",
                    "lastConsumption": "8/20 4:02",
                    "alert": ""
                }
            ]
        }],
        filteredStock: [...stockData],
        alert: {
            data: [{
                "id": "1",
                "alertLevel": "Medium",
                "date": "03/12/2020 09:34 AM",
                "description": "Vino in scadenza",
                "tagID": "Alby003",
                "wine": "Sangiovese",
                "winery": "Cantina Baffi",
                "room": "Sala Rossa"
            },
            {
                "id": "2",
                "alertLevel": "Medium",
                "date": "03/12/2020 09:12 AM",
                "description": "Vino in scadenza",
                "tagID": "Alby003",
                "wine": "Sangiovese",
                "winery": "Cantina Russi",
                "room": "Cantina"
            },
            {
                "id": "3",
                "alertLevel": "High",
                "date": "28/11/2020 08:20 PM",
                "description": "Vino in scadenza",
                "tagID": "Alby003",
                "wine": "Sangiovese",
                "winery": "Cantina Leonardo",
                "room": "Bar"
            }]
        },
        stock: {
            headers: [
                { title: "", field: "img" },
                { title: "Vino", field: "wine" },
                { title: "Prezzo medio acquisto", field: "purchasePrice" },
                { title: "Rimasto", field: "available" },
                { title: "Aperto", field: "opened" },
                { title: "In Scadenza", field: "expiring" },
                { title: "Scadute", field: "expired" },
            ],
            bagHeaders: [
                { title: "Codice Bag", field: "bagCode" },
                { title: "Stato", field: "status" },
                { title: "Room", field: "room" },
                { title: "Apertura", field: "openingDate" },
                { title: "Scadenza", field: "expiration" },
                { title: "Bicchieri Rimasti", field: "wineLeft" },
                { title: "Ultime Consumazioni", field: "lastConsumption" },
                { title: "Alert", field: "alert" },
            ],
            data: [...stockData]
        }
    }

    componentDidMount() {

        // // fetching Alert data
        // fetch('https://api.github.com/gists/11381d302b504135f5f81460f12fb701')
        //     .then(res => (
        //         res.json()
        //     ))
        //     .then(data => {
        //         this.setState({ ...this.state, alert: { ...this.state.alert, data: JSON.parse(data.files["AlertWineDispenser.json"].content).dataWine } })
        //         // this.setState({ isLoading: false })
        //         console.log(this.state);
        //     })
        //     .catch(error => console.log("There was a problem with the receiving data, please contact the customer care."));

        // // fetching Stock data
        // fetch('https://api.github.com/gists/fd0f573b584dda95ef5a5373e7c40b8f')
        //     .then(res => (
        //         res.json()
        //     ))
        //     .then(data => {
        //         this.setState({ ...this.state, stock: { ...this.state.stock, data: JSON.parse(data.files["Stock.json"].content).data } })
        //         this.setState({ isLoading: false })
        //         console.log(this.state);
        //         // this.setState(prev => ({
        //         //   stock: { 
        //         //     ...prev.stock,
        //         //     data: JSON.parse(data.files["Stock.json"].content).data
        //         //   }
        //         // }))
        //     })
        //     .catch(error => console.log("There was a problem with the receiving data, please contact the customer care."));
    }

    addSearchedElement = () => {
        // let newElement = {
        //     "id": 1,
        //     "wine": "Sangiovese",
        //     "img": "https://www.stoneycreekwinepress.com/assets/images/labels/categories/classic-vineyard-wine-label1.jpg",
        //     "winery": "Cantina Albi",
        //     "purchasePrice": "32$",
        //     "available": "921",
        //     "opened": "237",
        //     "expiring": "45",
        //     "expired": "21"
        // }
        // this.setState((prev) => ({
        //     stock: {
        //         ...prev.stock,
        //         data: [...this.state.stock.data, newElement]
        //     }
        // }))
    }

    uploadModal = (wineSearch, onCancel) => {
        return (
            <Translation>
                {(t) =>
                    <>
                        <h3>{t("stock.add wines.title").toUpperCase()}</h3>
                        <div className="h5 d-flex justify-content-center">
                            <div className="m-1">
                                <span>{t("settings.inserted code")}</span>
                                <div className="d-flex align-items-center">
                                    <i className="las la-box la-2x"></i>
                                    <span className="font-medium-4">{wineSearch}</span>
                                </div>
                            </div>
                            <div className="m-1">
                                <span>{t('stock.destination room')}</span>
                                <select name="room" class="form-control" onChange={(e) => this.setState({ selectedRoom: e.target.value })}>
                                    {this.state.rooms.map(room =>
                                        <option key={room.id} value={room.id}>{room.name}</option>
                                    )}
                                </select>
                            </div>
                        </div>
                        <form className="">
                            <div className="my-2">{t("stock.add wines.subtitle").toUpperCase()}</div>
                            <div className="my-2 border border-light round">
                                {this.state.itemsToUpload.map(item =>
                                    <div key={item.id} className="col-12 d-flex justify-content-around align-items-center my-1 border-light border-bottom-1">
                                        <fieldset className="position-relative has-icon-left">
                                            <input
                                                type="checkbox"
                                                checked={item.selected}
                                                onChange={(e) => this.setState({
                                                    ...this.state,
                                                    itemsToUpload: this.state.itemsToUpload.map(el => ({
                                                        ...el,
                                                        selected: el.id === item.id ? e.target.checked : el.selected
                                                    }))
                                                })}
                                            />
                                        </fieldset>
                                        <div className="">
                                            <div className="font-medium-2">{item.bags.length}x</div>
                                            <div>
                                                <img src={SmartBag} width="40px" height="40px" style={{ "objectFit": "contain" }} alt="smart bag icon" />
                                                <div>{t("wine.bag")}</div>
                                            </div>
                                        </div>
                                        <div className="font-medium-2 font-weight-bold p-1">{item.vintage}</div>
                                        <div className="font-medium-3">
                                            <div>{item.wine}</div>
                                            <div>{item.winery}</div>
                                        </div>
                                        <div className="">
                                            <strong>{t('stock.purchase price')}</strong>
                                            <fieldset className="position-relative has-icon-left">
                                                <input
                                                    type="number"
                                                    step="0.01"
                                                    className="form-control round"
                                                    placeholder="--.--"
                                                    style={{ "width": "125px" }}
                                                    value={item.purchasePrice}
                                                    onChange={(e) => this.setState({
                                                        ...this.state,
                                                        itemsToUpload: this.state.itemsToUpload.map(el => ({
                                                            ...el,
                                                            price: el.id === item.id ? e.target.value : el.purchasePrice
                                                        }))
                                                    })}
                                                />
                                                <div className="form-control-position">
                                                    <i className="la la-dollar-sign"></i>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <div className="">
                                            <strong>{t('stock.destination room')}</strong>
                                            <select name="room" class="form-control" value={this.state.selectedRoom}>
                                                {this.state.rooms.map(room =>
                                                    <option key={room.id} value={room.id}>{room.name}</option>
                                                )}
                                            </select>
                                        </div>
                                    </div>
                                )}
                            </div>
                            <div className="d-flex justify-content-between">
                                <button type="button" className="btn btn-outline-info round" onClick={onCancel}>
                                    <span className="mx-1">{t("settings.cancel").toUpperCase()}</span>
                                    <i className="las la-times"></i>
                                </button>
                                <button type="button" className="btn btn-outline-danger round" onClick={() => this.onUpload()}>
                                    <span className="mx-1">{t("settings.upload").toUpperCase()}</span>
                                    <i className="las la-arrow-right"></i>
                                </button>
                            </div>
                        </form>
                    </>
                }
            </Translation>
        )
    }

    wineListModal = (onCancel) => {
        return (
            <Translation>
                {(t) =>
                    <>
                        <h3>{t("stock.wine list.title").toUpperCase()}</h3>
                        <form className="text-center">
                            <div className="my-1">{t("stock.wine list.subtitle").toUpperCase()}</div>
                            <div className="mt-3 mb-4">
                                {this.state.stock.data.map(item =>
                                    <div key={item.id} className="d-flex justify-content-between align-items-center my-1 px-3">
                                        <div className="d-flex mr-3">
                                            <div>
                                                <img className="img-fluid rounded" src={Label} width={"35px"} height={"55px"} alt={`${item.wine}'s label`} />
                                            </div>
                                            <div className="font-weight-bold p-1">{item.vintage}</div>
                                            <div className="text-left px-1">
                                                <div className="font-weight-bold">{item.wine}</div>
                                                <div>{item.winery}</div>
                                            </div>
                                        </div>
                                        <fieldset className="position-relative has-icon-left">
                                            <input
                                                type="number"
                                                step="0.01"
                                                className="form-control round"
                                                placeholder="--.--"
                                                style={{ "width": "125px" }}
                                                value={item.purchasePrice}
                                                onChange={(e) => this.setState({
                                                    ...this.state,
                                                    stock: {
                                                        ...this.state.stock,
                                                        data: [...this.state.stock.data.map(el => ({
                                                            ...el,
                                                            purchasePrice: el.id === item.id ? e.target.value : el.purchasePrice
                                                        }))]
                                                    }
                                                })}
                                            />
                                            <div className="form-control-position">
                                                <i className="la la-dollar-sign"></i>
                                            </div>
                                        </fieldset>
                                    </div>
                                )}
                            </div>
                            <div className="d-flex justify-content-between">
                                <button type="button" className="btn btn-outline-info round" onClick={onCancel}>
                                    <span className="mx-1">{t("settings.cancel").toUpperCase()}</span>
                                    <i className="las la-times"></i>
                                </button>
                                <button type="button" className="btn btn-outline-danger round" onClick={onCancel}>
                                    <span className="mx-1">{t("settings.save").toUpperCase()}</span>
                                    <i className="las la-arrow-right"></i>
                                </button>
                            </div>
                        </form>
                    </>
                }
            </Translation>
        )
    }

    closeModal = () => this.setState({ isModalOpen: false })
    // TODO: If wines already exist only add bags, if bags with same code exist don't add anything
    onUpload = () => {
        this.setState({ ...this.state, stock: { ...this.state.stock, data: [...this.state.stock.data, ...this.state.itemsToUpload.filter(item => item.selected === true)] } })
        this.closeModal()
    }

    setIsDeletingAlert = (value) => {
        this.setState({ isDeletingAlert: value })
    }

    deleteAlert = () => {
        this.setState({
            ...this.state,
            alert: {
                ...this.state.alert,
                data: this.state.alert.data.filter(alert => alert.id !== this.state.isDeletingAlert)
            }
        })
    }

    setIsRemovingAlerts = (value) => {
        this.state.alert.data.length > 0 &&
            this.setState({ isRemovingAlerts: value })
    }

    deleteAllAlerts = () => {
        this.setState({
            ...this.state,
            alert: {
                ...this.state.alert,
                data: []
            }
        })
    }

    render() {
        let { isLoading, wineSearch, showArchive, alert, stock, isRemovingAlerts } = this.state;
        console.log(alert);

        // const filterSelection = ["All Wines"].concat(stock.data.map(item => item.wine))
        //     .filter((elem, index, self) => index === self.indexOf(elem))

        // // You should be able to pass titles and rows for this table
        // const filterRows = (value) => {
        //     value.toLowerCase() !== "all wines"
        //         ? this.setState({ stock: { ...stock }, data: stockData.filter(item => item.wine === value) })
        //         : this.setState({ stock: { ...stock }, data: stockData })
        // }

        return (
            <Translation>
                {(t) =>
                    isLoading
                        ? <h3>{t("settings.loading")}...</h3>
                        : <>
                            <div className="row">
                                {/* Stock Alerts */}
                                <div className="col-md-8 col-12">
                                    <ResponsiveAlertTable title="Alert" link="/homepro"
                                        headers={[
                                            { title: t('table headers.status'), field: "alertLevel" },
                                            { title: t('table headers.tag id'), field: "tagID" },
                                            { title: t('table headers.wine name'), field: "wine" },
                                            { title: t('table headers.description'), field: "description" },
                                            { title: t('table headers.date'), field: "date" },
                                            { title: t('table headers.room'), field: "room" }
                                        ]}
                                        data={alert.data}
                                        onDelete={(id) => this.setIsDeletingAlert(id)}
                                        onDeleteAll={() => this.setIsRemovingAlerts(true)} />
                                </div>

                                <div className="col-md-4 col-12">
                                    {/* Upload stock */}
                                    <div className="card">
                                        <div className="input-group text-center mt-2 px-3">
                                            <input className="form-control text-center border-bottom-1 border-top-0 border-left-0 border-right-0" placeholder={t("stock.add wines.placeholder")} onChange={(e) => this.setState({ wineSearch: e.target.value.toUpperCase() })}></input>
                                            <button className="btn btn-success round border-0 font-medium-1 px-1" onClick={() => wineSearch && this.setState({ isModalOpen: true, isUploadModal: true })}>{t("stock.add wines.button").toUpperCase()}<i className="la la-plus text-white ml-1"></i></button>
                                        </div>
                                        <span className="text-center m-1 mx-5 mb-2">{t("stock.add wines.description")}</span>
                                    </div>
                                    {/* Wine list */}
                                    <div className="card">
                                        <div className="text-center mt-2 px-1">
                                            <button className="btn btn-info round border-0 font-medium-1 px-1" onClick={() => this.setState({ isModalOpen: true, isUploadModal: false })}>{t("stock.wine list.button").toUpperCase()} <i className="la la-arrow-right text-white ml-1"></i></button>
                                        </div>
                                        <span className="text-center m-1 mx-5 mb-2">{t("stock.wine list.description")}</span>
                                    </div>
                                </div>
                            </div>

                            {/* Stock wines */}
                            <div className="card">
                                {/* Card Header */}
                                <div className="card-header d-flex justify-content-between">
                                    <h4 className="card-title">
                                        <strong>{t("stock.title")}</strong>
                                    </h4>

                                    {/* Heading Elements */}
                                    <div className="d-flex align-items-center">
                                        {/* Inserisci qui gli elementi a destra del titolo */}
                                        {this.props.withSearch &&
                                            <fieldset className="position-relative has-icon-left">
                                                <input
                                                    type="text"
                                                    className="form-control round"
                                                    placeholder="Search..."
                                                    value={this.state.searchQuery}
                                                    onChange={(e) => this.updateSearchQuery(e.target.value)}
                                                />
                                                <div className="form-control-position">
                                                    <i className="la la-search"></i>
                                                </div>
                                            </fieldset>
                                        }
                                        {/* <button className="ml-1" onClick={this.toggleFilter}>
                                    <i className="las la-filter la-2x"></i>
                                    <div>Filter</div>
                                </button> */}
                                        <button type="button" onClick={() => this.setState({ showArchive: !showArchive })}>
                                            <div>{showArchive ? t("stock.hide archive") : t("stock.show archive")}</div>
                                        </button>
                                        {/* <Filter
                                    onSelect={(wine) => filterRows(wine)}
                                    selection={filterSelection}
                                /> */}
                                    </div>
                                </div>
                                <div className="card-content">
                                    <StockTable stock={stock} />
                                </div>
                            </div>

                            {/* Stock Archive */}
                            {showArchive &&
                                <div className="card">
                                    {/* Card Header */}
                                    <div className="card-header d-flex justify-content-between">
                                        <h4 className="card-title">
                                            <strong>{t("stock.stock archive")}</strong>
                                        </h4>

                                        {/* Heading Elements */}
                                        <div className="d-flex align-items-center">
                                            {/* Inserisci qui gli elementi a destra del titolo */}
                                            {this.props.withSearch &&
                                                <fieldset className="position-relative has-icon-left">
                                                    <input
                                                        type="text"
                                                        className="form-control round"
                                                        placeholder="Search..."
                                                        value={this.state.searchQuery}
                                                        onChange={(e) => this.updateSearchQuery(e.target.value)}
                                                    />
                                                    <div className="form-control-position">
                                                        <i className="la la-search"></i>
                                                    </div>
                                                </fieldset>
                                            }
                                            {/* <button className="ml-1" onClick={this.toggleFilter}>
                                    <i className="las la-filter la-2x"></i>
                                    <div>Filter</div>
                                </button> */}
                                        </div>
                                    </div>
                                    <div className="card-content">
                                        <StockTable stock={stock} />
                                    </div>
                                </div>
                            }

                            {/* TODO: Delete ResponsiveTable if not needed */}
                            {/* <ResponsiveTable title="Wines in stock"
                        link="/block"
                        headers={stock.headers}
                    data={stock.data} /> */}

                            <Modal
                                isOpen={this.state.isModalOpen}
                                onRequestClose={() => this.closeModal()}
                                style={modalStyle}
                                contentLabel=""
                            >
                                <div className="text-center">
                                    <button
                                        type="button"
                                        onClick={() => this.closeModal()}
                                        className="close"
                                        aria-label="Close"
                                        style={{
                                            position: 'absolute',
                                            top: '20px',
                                            right: '25px',
                                        }}>
                                        <i className="la la-times"></i>
                                    </button>
                                    {this.state.isUploadModal ? this.uploadModal(wineSearch, () => this.closeModal()) : this.wineListModal(() => this.closeModal())}
                                </div>
                            </Modal>
                            <ConfirmModal
                                isOpen={this.state.isDeletingAlert.length > 0}
                                onRequestClose={() => this.setIsDeletingAlert('')}
                                onSave={() => this.deleteAlert()}
                                message={t('settings.confirm delete alert')}
                            />
                            <ConfirmModal
                                isOpen={isRemovingAlerts}
                                onRequestClose={() => this.setIsRemovingAlerts(false)}
                                onSave={() => this.deleteAllAlerts()}
                                message={t('settings.confirm delete alerts')}
                            />
                        </>
                }
            </Translation>
        )
    }
}

export default Stock